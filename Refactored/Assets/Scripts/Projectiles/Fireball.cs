﻿using UnityEngine;
using System.Collections;

public class Fireball : AugmentedRealityObject {

	private GameObject explosionEffect;
	private Vector3 shotDirection;
	private bool setted = false;
	private float velocity = 5f;

	public void setTarget( Vector3 targetPosition ){
		shotDirection = (targetPosition - transform.position).normalized;
		setted = true;
	}
	
	void Awake(){
		explosionEffect = Util.LoadPrefabResource ("FireballExplosion") as GameObject;
	}
	
	void Update () {
		if (setted) {
			transform.position += shotDirection*Time.deltaTime*velocity;
		}
	}

	void OnTriggerEnter(Collider col){
		Instantiate ( explosionEffect, transform.position, transform.rotation );

		if ( collider != null && col.gameObject.tag == "ApplicableItemArea") {
			Hero hero = col.gameObject.transform.parent.gameObject.GetComponent<Hero>();
			if( hero != null ) hero.sufferDamage(3);
		}

		Destroy ( gameObject );
	}

}
