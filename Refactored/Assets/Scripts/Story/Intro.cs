﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour {

		
	int currentSubtitle;
	const int maxStrings = 16;
	float[] maxTimes;
	float startTimeCurrentSubtitle = 0;

	GUIStyle style = new GUIStyle();


	void Update (){

	}
	
	void Start (){

		currentSubtitle = 0;
		startTimeCurrentSubtitle = Time.time;
		maxTimes = new float[maxStrings] {1.5f,1.5f,1.5f,1.5f,1.5f,1.5f,1.5f,1.5f,1.5f,1.5f,1.5f,1.5f,1.5f,1.5f,1.5f,1.5f};

	}
	
	void OnGUI(){

		if(currentSubtitle >= maxStrings){
			// TODO: LOAD MAIN SCREEN
		}
		else{
			if(Time.time - startTimeCurrentSubtitle > maxTimes[currentSubtitle]){
				currentSubtitle++;
				startTimeCurrentSubtitle = Time.time;
			}

			loadSubtitles ();
		}

	}

	int findNearestSpace(string text){

		int lenght = Mathf.FloorToInt( text.Length * 3.5f / 6);

		while (text[lenght] != ' ') {
			lenght --;
		}

		return lenght;

	}

	void loadSubtitles(){

		style.fontSize = (int)(0.05f*Screen.height);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
		style.alignment = TextAnchor.MiddleCenter;

		string text = STR.get("Cap0."+currentSubtitle);

		if (text.Length >= 40) {

			Rect SubtitleRect1 = new Rect(Screen.width*0.05f,  Screen.height*0.80f, 0.90f* Screen.width , 0.10f*Screen.height);
			Rect SubtitleRect2 = new Rect(Screen.width*0.05f,  Screen.height*0.87f, 0.90f* Screen.width , 0.10f*Screen.height);

			int nearestSpace = findNearestSpace(text);

			GUI.Label(SubtitleRect1, text.Substring(0,nearestSpace), style);
			GUI.Label(SubtitleRect2, text.Substring(nearestSpace), style);
		
		}
		else{

			Rect SubtitleRect = new Rect(Screen.width*0.05f,  Screen.height*0.84f, 0.90f* Screen.width , 0.10f*Screen.height);

			GUI.Label(SubtitleRect, text, style);

		}

	}

}