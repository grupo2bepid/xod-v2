﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemiesAIPath : AIPath {

	private Transform[] intermediateTargestsPerLevel;
	private IntermediateTargetsCentral targetsCentral;
	private int currentLevel = 0;

	new void Awake(){
		base.Awake ();
		targetsCentral = FindObjectOfType<IntermediateTargetsCentral> ();
	}

	new void Start () {
		base.Start ();
		target = targetsCentral.getRandomTargetOfLevel (currentLevel);
		StartCoroutine ("getNextIntermediateTargetRoutine");
	}

	private float sqr2DDistance( Vector3 a, Vector3 b ){
		return (  (a.x-b.x)*(a.x-b.x) + (a.z-b.z)*(a.z-b.z) );
	}

	IEnumerator getNextIntermediateTargetRoutine(){
		while (true) {
			if( target != null ){
				if( sqr2DDistance(target.position, transform.position) < 5f ){
					++currentLevel;
					if( currentLevel > targetsCentral.phaseMaxTargetLevel ) currentLevel = targetsCentral.phaseMaxTargetLevel;
					target = targetsCentral.getRandomTargetOfLevel( currentLevel );
				}
			}
			yield return new WaitForSeconds(0.3f);
		}
	}

	public void startWalk(){
		canSearch = true;
		canMove = true;
	}

	public void stopWalk(){
		canMove = false;
		canSearch = false;
	}

}
