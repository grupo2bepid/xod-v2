﻿using UnityEngine;
using System.Collections;

public class Util : Object {

	public static Object LoadImageResource(string imageName){
		return Resources.Load ("Images/" + imageName);
	}

	public static Object LoadImageResource(STR.Resources imageName){
		return LoadImageResource(STR.get(imageName));
	}

	public static Object LoadImageResource(STR.TranslatedResources imageName){
		return LoadImageResource(STR.get(imageName));
	}

	public static Object LoadPrefabResource(string prefabName){
		return Resources.Load ("Prefabs/" + prefabName);
	}

//	public static Object LoadPrefabOfHeroClass(Barracks.HeroClass heroClass){
//		return LoadPrefabResource (Barracks.getNameOfClass (heroClass) + "Lvl" + Barracks.getLevelOfClass (heroClass));
//	}
//
	public static Object LoadPrefabForMenuOfHeroClass(Barracks.HeroClass heroClass){
		return LoadPrefabResource ("HerosMenu/" + Barracks.getNameOfClass (heroClass) + "Menu");
	}

	public static Object LoadGIFResource(string gifName, int i){
		return Resources.Load ("GIF/" + gifName + i.ToString());
	}


}
