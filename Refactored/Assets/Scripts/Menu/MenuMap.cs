﻿using UnityEngine;
using System.Collections;

public class MenuMap : MonoBehaviour {
	/*Variaveis para cuidar da sonoplastia*/
	public GameObject load;

	//sounds
	public GameObject musicBox;
	public AudioSource effects, music;

	/*Variaveis para menu de settings*/
	Texture musicOnIcon;
	Texture musicOffIcon;
	Texture musicIcon;

	Texture effectOnIcon;
	Texture effectOffIcon;
	Texture effectIcon;

	Texture homeIcon;
	Texture settingIcon;
	Rect	musicIconPos;
	Rect	effectIconPos;
	Rect	homeIconPos;
	Rect 	settingIconPos;
	bool 	isSettingMenuOn = false;
	bool	clickedSetting  = false;
	bool	clickedHome		= false;



	/*Variaveis para controlar posicao do menu principal*/
	bool 	isPlayerMenuOn = false; 
	bool	clickedQuartel = false;
	bool	clickedStore   = false;
	Texture kingdomButton;
	Texture storeButton;
	Texture	quartelButton;
	Rect 	kingdomButtonPos;
	Rect 	storeButtonPos;
	Rect	quartelButtonPos;

	
	/*Variaveis para o Mapa de background*/
	Texture map;
	Rect mapPos;
	
	/*Variaveis para as fases*/
	Texture[]	phasesButton; //Imagens de cada fase
	Rect[] 		phasesButtonPos;
	bool[]		clickedPhase;
		
	/**Variavel para realizar efeito de pressionar botao**/
	float timePassed = 0; //Controla o tempo pasado de ate um dado frame
	const float threshold = 0.05F;//Limitante de tempo para efeito de botao pressionar

	/*Variáveis do capítulo*/
	float currentChapter;
	int chapterStep;
	Texture chapterBG;
	Rect chapterBGRect;
	bool nextStep = false;

	PointerArrow pointerArrow;
	Rect lastArrowPosition;

	GUIStyle style = new GUIStyle();

	/*Variaveis para fazer animacao de um loading*/
	Texture loadImage;
	Rect	loadImagePos;
	bool 	loading = false;

	//Salva status no dicionario, caso termine aplicacao nessa tela
	void OnApplicationQuit(){
		Database.writeDictionaryOnDisk ();
	}

	void OnApplicationPause (){
		Database.writeDictionaryOnDisk ();
	}


	void Awake(){
		//Inicializa sem loading
		loadImage = (Texture)Util.LoadImageResource (STR.TranslatedResources.LOADING_TRES);
		loadImagePos = new Rect (Screen.width/2 - (loadImage.width * Screen.width/2048)/2, Screen.height/2 - (loadImage.height * Screen.height/1536)/2, loadImage.width * Screen.width/2048, loadImage.height * Screen.height/1536);


		//sounds migue
		if (AudioListener.volume != 1.0F)
			AudioListener.volume = 1.0F;

		/*Mapa usado em background*/
		map		= (Texture)Util.LoadImageResource (STR.TranslatedResources.MAP);
		mapPos	= new Rect (0, 0, Screen.width, Screen.height);

		/*** Inicializa menu principal do reino no centro e seu menu circular ***/
		/*Reino menu central*/  //Coloca extamente no centro da imagem 
		kingdomButton		= (Texture)Util.LoadImageResource (STR.Resources.CAPITAL_BUTTON);
		kingdomButtonPos = new Rect (0.65f*Screen.width - (kingdomButton.width * Screen.width / 2048), 0.42f*Screen.height - (kingdomButton.height * Screen.height / 1536), kingdomButton.width * Screen.width / 2048, kingdomButton.height * Screen.height / 1536);

		//Variaveis para desnehar os botoes circulares em volta do reino
		float diag		= Mathf.Sqrt (Mathf.Pow (kingdomButtonPos.width, 2) + Mathf.Pow (kingdomButtonPos.height, 2));
		float ray		= 1*diag;
		float centeX	= kingdomButtonPos.x + (kingdomButtonPos.width / 2);
		float centerY	= kingdomButtonPos.y + (kingdomButtonPos.height / 2);
		float x;
		float y;

		/*Posicao no menu circular do botao de shop*/
		storeButton		= (Texture) Util.LoadImageResource(STR.TranslatedResources.STORE_BUTTON);
		x = centeX  + (ray * Mathf.Cos (Mathf.PI / 6)); //Fica a 30gr
		y = centerY - (ray * Mathf.Sin (Mathf.PI / 6)); 
		storeButtonPos = new Rect (x - ((storeButton.width * Screen.width / 2048) / 2), y - (storeButton.height * Screen.height / 1536) / 2, storeButton.width * Screen.width / 2048, storeButton.height * Screen.height / 1536);

		/*Posicao no menu circular do botao de quartel*/
		quartelButton	= (Texture) Util.LoadImageResource(STR.TranslatedResources.BARRACKS_BUTTON);
		x = centeX  + (ray * Mathf.Cos (Mathf.PI / 6 + 2 * Mathf.PI / 3)); //Fica a 150gr
		y = centerY - (ray * Mathf.Sin (Mathf.PI / 6 + 2 * Mathf.PI / 3)); 
		quartelButtonPos = new Rect (x - ((quartelButton.width * Screen.width / 2048) / 2), y - (quartelButton.height * Screen.height / 1536) / 2, quartelButton.width * Screen.width / 2048, quartelButton.height * Screen.height / 1536);

		/*Posicao no menu circular do botao de setting*/
		settingIcon = (Texture) Util.LoadImageResource(STR.TranslatedResources.SETTINGS_BUTTON);
		x = centeX  + (ray * Mathf.Cos (-Mathf.PI / 2)); //Fica a -90gr ou 270gr
		y = centerY - (ray * Mathf.Sin (-Mathf.PI / 2)); 
		settingIconPos = new Rect (x - ((settingIcon.width * Screen.width / 2048) / 2), y - (settingIcon.height * Screen.height / 1536) / 2, settingIcon.width * Screen.width / 2048, settingIcon.height * Screen.height / 1536);



		/*** Inicializar menu circular do botao de settings ***/
		diag	= Mathf.Sqrt (Mathf.Pow (settingIconPos.width, 2) + Mathf.Pow (settingIconPos.height,2));
		ray		= 0.75f*diag;
		centeX	= settingIconPos.x + (settingIconPos.width / 2);
		centerY	= settingIconPos.y + (settingIconPos.height / 2);

		//Calcula posicao botao de sound a 30gr
		musicOnIcon  = (Texture) Util.LoadImageResource(STR.Resources.MUSIC_BUTTON);
		musicOffIcon = (Texture) Util.LoadImageResource(STR.Resources.MUSIC_BUTTON_PRESSED);
		x = centeX  + (ray * Mathf.Cos (Mathf.PI / 6)); //Fica a 30gr
		y = centerY - (ray * Mathf.Sin (Mathf.PI / 6)); 
		musicIconPos = new Rect (x - ((musicOffIcon.width * Screen.width / 2048) / 2), y - (musicOffIcon.height * Screen.height / 1536) / 2, musicOffIcon.width * Screen.width / 2048, musicOffIcon.height * Screen.height / 1536);

		//Calcula posicao botao de efeitos a 150gr
		effectOnIcon  = (Texture) Util.LoadImageResource(STR.Resources.EFFECTS);
		effectOffIcon = (Texture) Util.LoadImageResource(STR.Resources.EFFECTS_PRESSED);
		x = centeX  + (ray * Mathf.Cos (Mathf.PI / 6 + 2 * Mathf.PI / 3)); //Fica a 150gr
		y = centerY - (ray * Mathf.Sin (Mathf.PI / 6 + 2 * Mathf.PI / 3)); 
		effectIconPos = new Rect (x - ((effectOffIcon.width * Screen.width / 2048) / 2), y - (effectOffIcon.height * Screen.height / 1536) / 2, effectOffIcon.width * Screen.width / 2048, effectOffIcon.height * Screen.height / 1536);

		//Calcula posicao botao de home -90gr ou 270gr
		homeIcon  = (Texture) Util.LoadImageResource(STR.Resources.HOME);
		x = centeX  + (ray * Mathf.Cos (-Mathf.PI / 2)); //Fica a -90gr ou 270gr
		y = centerY - (ray * Mathf.Sin (-Mathf.PI / 2)); 
		homeIconPos = new Rect (x - ((homeIcon.width * Screen.width / 2048) / 2), y - (homeIcon.height * Screen.height / 1536) / 2, homeIcon.width * Screen.width / 2048, homeIcon.height * Screen.height / 1536);



		/*** Inicializa as phases na tela ***/ //PROVISORIO: NAO ESTA GERANDO VARIAS FASES PROGRAMAT.
		phasesButton 	= new Texture[1]; //APENAS TEMOS UMA FASE AINDA
		phasesButtonPos = new Rect[1];
		clickedPhase	= new bool[1];

		//Inicializa necrophase (AJUSTAR POSICAO)
		phasesButton[0]		= (Texture) Util.LoadImageResource(STR.Resources.DESERT_LEVEL);
		Texture necro 		= phasesButton[0]; 
		phasesButtonPos[0] 	= new Rect (Screen.width  - 1.45f*(necro.width * Screen.width / 2048), 0.48f*Screen.height, necro.width * Screen.width / 2048, necro.height * Screen.height / 1536);
		 
	
		
		chapterBG = (Texture) Util.LoadImageResource(STR.Resources.MENU_PAUSED_BACK);
		chapterBGRect = new Rect(-0.10f*Screen.width, 0.80f*Screen.height, 1.2f*Screen.width, 0.2f*Screen.height);


		GameObject pointerArrowPrefab = Util.LoadPrefabResource ("PointerArrow") as GameObject;
		GameObject pointerArrowGameObject = Instantiate (pointerArrowPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		pointerArrow = pointerArrowGameObject.GetComponent (typeof(PointerArrow)) as PointerArrow;
	}



	void Start(){
		/*cria um objeto filho para receber o audiosource de musicas*/
		musicBox = new GameObject ("MusicBox");
		musicBox.transform.position = gameObject.transform.position;

		/*inicializacao da parte sonora*/
		music = gameObject.AddComponent<AudioSource> ();
		effects = musicBox.AddComponent<AudioSource>();

		/*efeito inicial e loop da musica*/
		SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);
		SoundController.playSoundMusic (music, Resources.Load ("Sounds/menuMapMusic")as AudioClip);

		/*Inicializa booleanos para cuidar de efeito de click como falso*/
		clickedPhase[0] = false;
		clickedHome 	= false;
		clickedQuartel 	= false;
		clickedSetting 	= false;
		clickedStore 	= false;

		/*Inicializa textura de sons com estado atual*/
		if(SoundController.isEffectOff())//Verifica estado do som, para inicializar textura
			effectIcon = effectOffIcon;
		else 
			effectIcon = effectOnIcon;

		if(SoundController.isMusicOff())//Verifica estado do som, para inicializar textura
			musicIcon = musicOffIcon;
		else 
			musicIcon = musicOnIcon;

		currentChapter = Database.currentChapter ();
		chapterStep = 0;

	}



	void Update(){

		timePassed += Time.deltaTime;


		/*verifica se a musica nao foi mutada*/
		SoundController.muteSounds (music,effects);
	}
	
	void OnGUI() {  
		GUI.backgroundColor = Color.clear; 


		//Desenhar mapa em backgound
		GUI.DrawTexture(mapPos, map);


		if (loadChapterStory ()) {
			drawPhases ();
			drawPlayerMenu ();
		}/*else{
			drawPhases (false);
			drawPlayerMenu (false);
		}*/

		if(loading) //Caso esteja carregando coloca imagem
			GUI.DrawTexture(loadImagePos, loadImage);

	}

	//Desenha botoes para ir para as fases na tela
	//O metodo deveria ser generico para todas as phases, mas ainda so esta para uma.
	void drawPhases (bool enable = true) {
		//Coloca primeira phase, necrophase
		if(enable&&GUI.RepeatButton (phasesButtonPos[0], "")){
			//Vai para estado pressionado
			phasesButton[0] = (Texture) Util.LoadImageResource(STR.Resources.DESERT_LEVEL_PRESSED);
			
			clickedPhase[0] = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(phasesButtonPos[0], phasesButton[0]);



			
		}
		else if(clickedPhase[0] && timePassed > threshold){
			//Volta para estado despressionado
			phasesButton[0]	= (Texture) Util.LoadImageResource(STR.Resources.DESERT_LEVEL);
			clickedPhase[0]		= false;

			//Persiste possiveis mudancas feitas no store/quartel/settings. 
			Database.writeDictionaryOnDisk ();

			//Coloca informacao de "Loading"
			//Inicializa sem loading
			loading = true;

			//Chama scene da fase
			Application.LoadLevel ("Lv1-1.1");

			GUI.DrawTexture(phasesButtonPos[0], phasesButton[0]);
		}
		else
			GUI.DrawTexture(phasesButtonPos[0], phasesButton[0]);

	}


	//Metodo usado para desenhar o menu principal que se encontra no centro no reino ds dwarfs.
	//ao clicar no reino aparece um menu circular com 3 botoes: quartel, store e setting.
	//Esse ultimo, ao ser clicado tera um submenu circular.
	void drawPlayerMenu (bool enable = true, bool enableSettings = true, bool enableStore = true, bool enableBarracks = true){
		//Coloca botao central do reino. Se clicado deve mostrar um menu circular e manter clicado
		if(enable&&GUI.Button(kingdomButtonPos, "")){
			//Caso estava ligado, entao, coloca para desligado.
			if(isPlayerMenuOn){
				kingdomButton  = (Texture) Util.LoadImageResource(STR.Resources.CAPITAL_BUTTON);
				isPlayerMenuOn = false;
				isSettingMenuOn = false;
			}
			else{ //Caso contrario, liga menu
				kingdomButton  = (Texture) Util.LoadImageResource(STR.Resources.CAPITAL_BUTTON_PRESSED);
				isPlayerMenuOn = true;
			}

			//sounds
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);

		}
		GUI.DrawTexture (kingdomButtonPos, kingdomButton);

		/**Verifica se submenu circular deve ser colocado**/
		if(isPlayerMenuOn){
			drawSettingMenu(enableSettings);
			drawStoreButton(enableStore);
			drawQuartelButton(enableBarracks);
		}
	}

	//Desenha e cuida do evento do botao de ir para o quartel
	void drawQuartelButton(bool enable = true) {
		if(enable&&GUI.RepeatButton (quartelButtonPos, "")){
			//Vai para estado pressionado
			quartelButton	= (Texture) Util.LoadImageResource(STR.TranslatedResources.BARRACKS_BUTTON_PRESSED);
			clickedQuartel = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(quartelButtonPos, quartelButton);

			
		}
		else if(clickedQuartel && timePassed > threshold){
			//Volta para estado despressionado
			quartelButton	= (Texture) Util.LoadImageResource(STR.TranslatedResources.BARRACKS_BUTTON);
			clickedQuartel	= false;

			//sounds
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);

			//Coloca informacao de "Loading"
			//Inicializa sem loading
			loading = true;

			//Chama scene do quartel
			Application.LoadLevel ("MenuBarracks");
			
			GUI.DrawTexture(quartelButtonPos, quartelButton);
		}
		else
			GUI.DrawTexture(quartelButtonPos, quartelButton);

	}

	//Desenha e cuida do evento do botao de ir para a store
	void drawStoreButton(bool enable = true) {
		if(enable&&GUI.RepeatButton (storeButtonPos, "")){
			//Vai para estado pressionado
			storeButton	= (Texture) Util.LoadImageResource(STR.TranslatedResources.STORE_BUTTON_PRESSED);
			clickedStore = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(storeButtonPos, storeButton);


		}
		else if(clickedStore && timePassed > threshold){
			//Volta para estado despressionado
			storeButton		= (Texture) Util.LoadImageResource(STR.TranslatedResources.STORE_BUTTON);
			clickedStore	= false;

			//sounds
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);
			
			//Coloca informacao de "Loading"
			loading = true;

			//Chama scene do quartel
			Application.LoadLevel ("MenuStore");

			GUI.DrawTexture(storeButtonPos, storeButton);
		}
		else
			GUI.DrawTexture(storeButtonPos, storeButton);
	}


	
	//Desenha e cuida dos eventos do submenu circular de settings.
	void drawSettingMenu (bool enable = true) {
		//Coloca o botao central do menu de setting
		if(enable&&GUI.RepeatButton (settingIconPos, "")){
			//Vai para estado pressionado
			settingIcon = (Texture) Util.LoadImageResource(STR.TranslatedResources.SETTINGS_BUTTON_PRESSED);
			clickedSetting = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(settingIconPos, settingIcon);

			
		}
		else if(clickedSetting && timePassed > threshold){
			//Volta para estado despressionado
			settingIcon = (Texture) Util.LoadImageResource(STR.TranslatedResources.SETTINGS_BUTTON);
			clickedSetting	= false;

			//sounds
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);
			
			//Inverte estado de mostrar menu circular de setting
			if(isSettingMenuOn)
				isSettingMenuOn = false;
			else
				isSettingMenuOn = true;
			
			GUI.DrawTexture(settingIconPos, settingIcon);
		}
		else
			GUI.DrawTexture(settingIconPos, settingIcon);


		if(isSettingMenuOn){
			drawHomeButton();
			drawMusicButton();
			drawEffectBUtton();

		}
	}

	void drawHomeButton(bool enable = true) {
		if(enable&&GUI.RepeatButton (homeIconPos, "")){
			//Vai para estado pressionado
			homeIcon  = (Texture) Util.LoadImageResource(STR.Resources.HOME_PRESSED);
			clickedHome = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(homeIconPos, homeIcon);
			
		}
		else if(clickedHome && timePassed > threshold){
			//Volta para estado despressionado
			homeIcon  = (Texture) Util.LoadImageResource(STR.Resources.HOME);
			clickedHome	= false;

			//Salva possiveis mudancas no banco
			Database.writeDictionaryOnDisk ();

			
			//Coloca informacao de "Loading"
			loading = true;

			//Chama scene de inicio
			Application.LoadLevel ("BeginMenu");
			
			GUI.DrawTexture(homeIconPos, homeIcon);
		}
		else
			GUI.DrawTexture(homeIconPos, homeIcon);
	}

	void drawMusicButton(bool enable = true){
		if(enable&&GUI.Button(musicIconPos, "")){
			if(SoundController.isMusicOff()){//Caso esteja desligado som, entao liga o mesmo.
				SoundController.musicIsOff(false);
				musicIcon = musicOnIcon;
			}
			else{ //Contrario, estava ligado, entao desliga.
				SoundController.musicIsOff(true);
				musicIcon = musicOffIcon;
			}
		}

		//Desenha botao com estado atual do som
		GUI.DrawTexture (musicIconPos, musicIcon);
	}

	void drawEffectBUtton(){
		if(GUI.Button(effectIconPos, "")){
			if(SoundController.isEffectOff()){//Caso esteja desligado som, entao liga o mesmo.
				SoundController.effectIsOff(false);
				effectIcon = effectOnIcon;
			}
			else{ //Contrario, estava ligado, entao desliga.
				SoundController.effectIsOff(true);
				effectIcon = effectOffIcon;
			}
		}
		
		//Desenha botao com estado atual do som
		GUI.DrawTexture (effectIconPos, effectIcon);
	}

	bool loadChapterStory(){

		if (chapterStep <= 9 && currentChapter == 0) {

			loadBackground();
			loadText();
			loadCharacter();
			loadNextButton();
			modifyMap();

			return false;
		}
		else{

			return true;
		}

	}

	void modifyMap(){

		if (chapterStep == 1) {
			
			kingdomButton  = (Texture) Util.LoadImageResource(STR.Resources.CAPITAL_BUTTON);
			isPlayerMenuOn = false;
			
			if(lastArrowPosition != phasesButtonPos[(int)currentChapter]){
				pointerArrow.pointTo(phasesButtonPos[(int)currentChapter]);
				lastArrowPosition = phasesButtonPos[(int)currentChapter];
			}
			
		}
		else if (chapterStep == 6 || chapterStep == 7) {

			kingdomButton  = (Texture) Util.LoadImageResource(STR.Resources.CAPITAL_BUTTON_PRESSED);
			isPlayerMenuOn = true;

			if(lastArrowPosition != quartelButtonPos){
				pointerArrow.pointTo(quartelButtonPos);
				lastArrowPosition = quartelButtonPos;
			}

		}

		else if (chapterStep == 8) {
			
			kingdomButton  = (Texture) Util.LoadImageResource(STR.Resources.CAPITAL_BUTTON_PRESSED);
			isPlayerMenuOn = true;

			if(lastArrowPosition != storeButtonPos){
				pointerArrow.pointTo(storeButtonPos);
				lastArrowPosition = storeButtonPos;
			}
		}

		else{
			kingdomButton  = (Texture) Util.LoadImageResource(STR.Resources.CAPITAL_BUTTON);
			isPlayerMenuOn = false;
			pointerArrow.pointToNothing();
		}

		drawPlayerMenu(false,false,false,false);
		drawPhases (false);
	}

	void loadBackground(){
		GUI.DrawTexture (chapterBGRect, chapterBG);
	}

	int findNearestSpace(string text){
		
		int lenght = Mathf.FloorToInt( text.Length * 3.5f / 6);
		
		while (text[lenght] != ' ') {
			lenght --;
		}
		
		return lenght;
		
	}
	
	void loadText(){
		
		style.fontSize = (int)(0.045f*Screen.height);
		style.fontStyle = FontStyle.Normal;
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		style.normal.textColor = Color.white;
		style.alignment = TextAnchor.MiddleCenter;

		string text = STR.get("Ch"+currentChapter+"."+chapterStep);
		
		if (text.Length >= 35) {
			
			Rect SubtitleRect1 = new Rect(Screen.width*0.25f,  Screen.height*0.80f, 0.60f* Screen.width , 0.10f*Screen.height);
			Rect SubtitleRect2 = new Rect(Screen.width*0.25f,  Screen.height*0.87f, 0.60f* Screen.width , 0.10f*Screen.height);
			
			int nearestSpace = findNearestSpace(text);
			
			GUI.Label(SubtitleRect1, text.Substring(0,nearestSpace), style);
			GUI.Label(SubtitleRect2, text.Substring(nearestSpace), style);
			
		}
		else{
			
			Rect SubtitleRect = new Rect(Screen.width*0.25f,  Screen.height*0.84f, 0.60f* Screen.width , 0.10f*Screen.height);
			
			GUI.Label(SubtitleRect, text, style);
			
		}
		
	}

	void loadCharacter(){
		Texture character = (Texture) Util.LoadImageResource(STR.Resources.KING);
		Rect characterRect = new Rect(-0.15f*Screen.width, 0.7f*Screen.height, 0.4f*Screen.width ,0.4f * Screen.height);
		GUI.DrawTexture (characterRect, character);
	}

	void loadNextButton(){
	
		Texture nextButton = (Texture) Util.LoadImageResource(STR.Resources.BACK_MENU_ITEM);
		Rect nextButtonRect = new Rect(0.90f*Screen.width, 0.85f*Screen.height, 0.07f*Screen.width ,0.09f * Screen.height);

		if(GUI.RepeatButton (nextButtonRect, "")){
			//Vai para estado pressionado
			nextButton  = (Texture) Util.LoadImageResource(STR.Resources.BACK_MENU_ITEM_PRESSED);

			nextStep = true;

			timePassed = 0;
			
			GUI.DrawTexture(nextButtonRect, nextButton);
			
		}
		else if(nextStep && timePassed > threshold){
			//Volta para estado despressionado
			nextButton  = (Texture) Util.LoadImageResource(STR.Resources.BACK_MENU_ITEM);
			nextStep	= false;
			
			chapterStep++;

			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);

			if(chapterStep > 9){
				Database.nextChapter();
			}
			
			GUI.DrawTexture(nextButtonRect, nextButton);
		}
		else
			GUI.DrawTexture(nextButtonRect, nextButton);
	}

}

