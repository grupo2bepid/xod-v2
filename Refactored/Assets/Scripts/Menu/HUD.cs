﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
//public class HUD : MonoBehaviour {
//	
//	public GUISkin skin = null;
//
//	private Cristal myCrystal;
//
//
//	float size = Screen.height/28;  //Tamanho dos dados na tela
//	float raio  = Screen.height/6; //Raio definido do botao para seus sub-botoes
//
//	//Fire attack
//	public RaycastHit _hit;
//	public static bool fireAttackNow = false;
//	public bool delayFireAttack = false;
//	public static RaycastHit fireTarget;
//
//
//	//Lista com informacoes do menu esquerdo: EXERCITO
//	public static string[] listMenuLeft = {"army","range", "sword"};
//
//	//Lista com informacoes do menu direito
//	public static string[] listMenuRight = {"item", "upgrade", "attack", "cure"};
//	
//	//Nome do objeto selecionado para adicionar no jogo
//	HeroNode selectedObject = null;
//
//	public GameObject RangePrefab = null;
//	
//	//Booleano para controlar o que deve ser mostrado
//	//Menu esquerdo
//	bool rangeAttack 	 = false;
//	bool swordAttack 	 = false;
//	bool leftMenuOn		 = false;
//	//Menu direito
//	bool rightMenuOn	 = false;
//	bool cureItem		 = false;
//	bool attackItem		 = false;
//	bool upgradeItem	 = false;
//
//	//Controle de menus
//	bool paused 	 	 = true;
//	bool confirmSelect 	 = false;
//
//	//Start
//	void Start(){
//		myCrystal   = (Cristal)FindObjectOfType (typeof(Cristal));
//	}
//
//	//Mensagem de pause
//	void Pause(){
//		paused = true;
//	}
//	void Resume(){
//		paused = false;
//	}
//
//	void OnEnable(){
//		GameController.onPause  += Pause;
//		GameController.onResume += Resume;
//	}
//
//	void OnDisable(){
//		GameController.onPause  -= Pause;
//		GameController.onResume -= Resume;
//	}
//
//
//	//Na GUI
//	void OnGUI() {  
//
//		//GUI.skin = skin;
//		GUIStyle style = new GUIStyle();
//		style.fontSize = (int)(35* (size/10.0F) );
//		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
//		style.fontStyle = FontStyle.Bold;
//		style.normal.textColor = Color.white;
//
//
//
//
//		GUI.backgroundColor = Color.clear;
//
//		if (paused) {
//				style.fontSize = 100;
//				GUI.Label (new Rect ((Screen.width - 400) / 2, Screen.height / 2, 400, 200), "PAUSED", style);
//			
//		}
//		else{
//			GUI.DrawTexture(new Rect(2*size, size, 5 * size, 2.5F * size), (Texture) Util.LoadImageResource("wall"), ScaleMode.StretchToFill, true, size);
//			GUI.Label(new Rect(7 * size, 0, 25 * size, 2.5F * size), "" +myCrystal.Life , style);
//
//			//Verifica se estamos habilitado para confirmar algo
//			if(confirmSelect == true)
//				confirmSelection();
//
//			//Caso sem nennhum menu sendo usado
//			else if(leftMenuOn == false && rightMenuOn == false){ 
//				if(//Botao do menu direito
//				   GUI.Button(new Rect(Screen.width - 6.0F* size, Screen.height/2, 5.0F* size, 5.0F* size), (Texture) Util.LoadImageResource("item"), "")
//					){
//						rightMenuOn = true;
//						leftMenuOn  = false;
//					}
//
//				else if(//Botao do menu esquerdo
//				        GUI.Button(new Rect(0,Screen.height/2, 5 * size,5 * size), (Texture) Util.LoadImageResource("army"), "")
//					){
//						leftMenuOn = true;
//						rightMenuOn = false;
//				}
//			}
//			//Usando menu da esquerda para colocar exercito
//			else if(leftMenuOn == true && rightMenuOn == false) 
//				LoadDefensesMenu();
//
//			//Usando menu da direita para colocar itens
//			else if(rightMenuOn == true && leftMenuOn == false)
//				LoadItemMenu();
//
//			//Caso de erro, volta pro estado desativado dos menus
//			else{
//				leftMenuOn  = false;
//				rightMenuOn = false;
//			}
//
//			
//		}
//		
//
//	}
//
//
//	IEnumerator Timer() {
//		yield return new WaitForSeconds(10); //wait 10 seconds
//		delayFireAttack = false;
//	}
//
//	/* Metodo responsavel pelo menu da esquerda que é usado pelo usuário para selecionar algum personagem do seu
//	 * exercíto. 
//	 * Somente é chamado quando o menu está ativo.
//	 */
//	void LoadDefensesMenu() {
//		if(//Botao do menu direito
//		   GUI.Button(new Rect(Screen.width - 6.0F* size, Screen.height/2, 5.0F* size, 5.0F* size), (Texture) Util.LoadImageResource("item"), "")
//		   ){
//			//Volta este menu para estado inicial e ativa o menu de itens(direita)
//			rightMenuOn = true;
//			leftMenuOn  = false;
//
//			//Retira qualquer submenu antes selecionado do menu esquerdo
//			swordAttack = false;
//			rangeAttack = false;
//			cureItem	 = false;
//			attackItem	 = false;
//			upgradeItem	 = false;
//		}
//
//		else if( //Botao de voltar desse menu em questao
//		        GUI.Button(new Rect(0,Screen.height/2, 5 * size,5 * size), (Texture) Util.LoadImageResource("back"), "")
//			) { 
//			//Volta menu para estado inicial
//			leftMenuOn  = false;
//
//			//Retira qualquer submenu antes selecionado do menu esquerdo
//			swordAttack = false;
//			rangeAttack = false;
//			cureItem	 = false;
//			attackItem	 = false;
//			upgradeItem	 = false;
//		}
//		//Mostrar menu de defesa ativo.
//		else {
//			for(int index = 1; index < listMenuLeft.Length; index++) {
//				int signal;
//				if(index == 1)
//					signal = -1;
//				else 
//					signal = 1;
//
//				if(GUI.Button(new Rect(0,  (Screen.height/2) + (signal * (5*size)), 5 * size, 5 * size), "")) {
//
//					
//					if(index == 1){ //Selecionou ataque a distancia
//						swordAttack = false;
//						rangeAttack = true;
//					}
//					else if(index == 2){ //Selecionou ataque corpo-corpo
//						swordAttack = true;
//						rangeAttack = false;
//					}
//				}
//				
//				GUI.DrawTexture(new Rect(0, (Screen.height/2) + (signal * (5*size)), 5 * size, 5 * size), (Texture) Util.LoadImageResource(listMenuLeft[index]), ScaleMode.StretchToFill, true, size);
//			}
//			
//			//Caso em que submenu de ataque a distancia está ativo
//			if(rangeAttack){
//				//Lista informacoes de tipos de herois de range que o jogador possui.
//				List<HeroNode> rangeExisting = Player.getHeroesRange();
//
//				//Divide os 120 graus entre os botoes existente
//				float graus =(2 * Mathf.PI) / (3 * rangeExisting.Count); 
//
//				int  i = 0;
//				//Percorre a lista para colocar na tela os botoes desses herois.
//				foreach(HeroNode node in rangeExisting){
//					//Posicao para ficar o botao desse caracter
//					Rect position = new Rect(raio * Mathf.Cos( Mathf.PI/2 - Mathf.PI/(6) - (graus * i) ) , (Screen.height/2 - 5*size) - raio * Mathf.Sin(  Mathf.PI/2 - Mathf.PI/(6) - (graus * i) ), 5 * size, 5 * size);
//					
//					
//					if(GUI.Button(position, "")) {
//						//Caso escolheu um range attack para colocar no jogo
//						rangeAttack = false;
//						
//						//Marca nome do mesmo selecionado para ser usado posteriormente para adicionar na fase
//						selectedObject = node;
//						
//						//Muda estado para confirmar a selecao e posicionar o mesmo
//						confirmSelect = true;
//					}
//					//Desenha botao
//					GUI.DrawTexture(position, (Texture) Util.LoadImageResource(node.name), ScaleMode.StretchToFill, true, size);
//					
//					//Desenha label com quantidade disponivel
//					GUIStyle style = new GUIStyle();
//					style.fontSize = (int)(size);
//					style.fontStyle = FontStyle.Bold;
//					style.normal.textColor = Color.white;
//					GUI.DrawTexture(new Rect(position.x+3.5F*size, position.y+3.5F*size, 1.4F*size, 1.4F*size),   (Texture) Util.LoadImageResource("quantityBack"), ScaleMode.StretchToFill, true, size);
//					GUI.Box(new Rect(position.x+3.7F*size, position.y+3.6F*size, size, size), node.quantity.ToString(),  style); 
//					
//					i++;
//
//				}
//			}
//			//Caso submenu de ataque corpo-corpo está ativo
//			else if(swordAttack){
//				//Lista informacoes de tipos de herois de corpo-corpo que o jogador possui.
//				List<HeroNode> swordExisting = Player.getHeroesSword();
//				
//				//Divide os 120 graus entre os botoes existente
//				float graus =(2 * Mathf.PI) / (3 * swordExisting.Count); 
//				
//				int  i = 0;
//				//Percorre a lista para colocar na tela os botoes desses herois.
//				foreach(HeroNode node in swordExisting){
//					//Posicao para ficar o botao desse caracter
//					Rect position = new Rect(raio * Mathf.Cos( Mathf.PI/2 - Mathf.PI/(6) - (graus * i) ) , (Screen.height/2 + 5*size) + raio * Mathf.Sin(  Mathf.PI/2 - Mathf.PI/(6) - (graus * i) ), 5 * size, 5 * size);
//					
//					
//					if(GUI.Button(position, "")) {
//						//Caso escolheu um sword attack para colocar no jogo
//						swordAttack = false;
//						
//						//Marca nome do mesmo selecionado para ser usado posteriormente para adicionar na fase
//						selectedObject = node;
//						
//						//Muda estado para confirmar a selecao e posicionar o mesmo
//						confirmSelect = true;
//					}
//					
//					//Desenha botao
//					GUI.DrawTexture(position, (Texture) Util.LoadImageResource(node.name), ScaleMode.StretchToFill, true, size);
//					
//					//Desenha label com quantidade disponivel
//					GUIStyle style = new GUIStyle();
//					style.fontSize = (int)(size);
//					style.fontStyle = FontStyle.Bold;
//					style.normal.textColor = Color.white;
//					GUI.DrawTexture(new Rect(position.x+3.5F*size, position.y+3.5F*size, 1.4F*size, 1.4F*size),   (Texture) Util.LoadImageResource("quantityBack"), ScaleMode.StretchToFill, true, size);
//					GUI.Box(new Rect(position.x+3.7F*size, position.y+3.6F*size, size, size), node.quantity.ToString(),  style); 
//
//					i++;
//					
//				}
//				
//			}
//		}
//	}
//
//	/* Metodo responsavel pelo menu da direita que é usado pelo usuário para selecionar algum item seu.
//	 * Somente é chamado quando o menu está ativo.
//	 */
//	void LoadItemMenu (){
//		if(//Botao do menu esquerdo
//		   GUI.Button(new Rect(0,Screen.height/2, 5 * size,5 * size), (Texture) Util.LoadImageResource("army"), "")
//		   ){
//			//Volta este menu para estado inicial e ativa o menu de exercito(esquerda)
//			rightMenuOn = false;
//			leftMenuOn  = true;
//
//			//Retira qualquer submenu antes selecionado do menu esquerdo
//			swordAttack = false;
//			rangeAttack = false;
//			cureItem	 = false;
//			attackItem	 = false;
//			upgradeItem	 = false;
//		}
//
//		else if( //Botao de voltar desse menu em questao
//		        GUI.Button(new Rect(Screen.width - 6.0F* size, Screen.height/2, 5.0F* size, 5.0F* size), (Texture) Util.LoadImageResource("backRight"), "")
//			) { 
//			//Volta menu para estado inicial
//			rightMenuOn  = false;
//
//			//Retira qualquer submenu antes selecionado
//			swordAttack = false;
//			rangeAttack = false;
//			cureItem	 = false;
//			attackItem	 = false;
//			upgradeItem	 = false;
//		}
//		//Mostrar menu de item ativo.
//		else {
//			for(int index = 1; index < listMenuRight.Length; index++) {
//				int signal = 0;
//				int left   = 0;
//				if(index == 1)
//					signal = -1;
//				else if(index == 2)
//					signal = 1;
//				else
//					left = 1;
//				
//				if(GUI.Button(new Rect( (Screen.width - 6.0F* size) - (left * (5*size)),  (Screen.height/2) + (signal * (5*size)), 5 * size, 5 * size), "")) {
//					
//					
//					if(index == 1){ //Selecionou upgrade
//						attackItem  = false;
//						upgradeItem = true;
//						cureItem    = false;
//					}
//					else if(index == 2){ //Selecionou attack direto
//						attackItem  = true;
//						upgradeItem = false;
//						cureItem    = false;
//					}
//					else if(index == 3){ //Selecionou curar
//						attackItem  = false;
//						upgradeItem = false;
//						cureItem    = true;
//					}
//				}
//				
//				GUI.DrawTexture(new Rect( (Screen.width - 6.0F* size) - (left * (5*size)),  (Screen.height/2) + (signal * (5*size)), 5 * size, 5 * size), (Texture) Util.LoadImageResource(listMenuRight[index]), ScaleMode.StretchToFill, true, size);
//			}
//			if(attackItem){
//				//Lista informacoes de tipos de itens de ataque direto que o jogador possui.
//				List<HeroNode> itensExisting = Player.getItensAttack();
//
//				Debug.Log(itensExisting.Count);
//
//				//Divide os 150 graus entre os botoes existente
//				float graus =(5 * Mathf.PI) / (6 * itensExisting.Count); 
//				
//				int  i = 0;
//				//Percorre a lista para colocar na tela os botoes desses itens.
//				foreach(HeroNode node in itensExisting){
//					//Posicao para ficar o botao desse caracter
//					Rect position = new Rect((Screen.width - 6.0F* size) - raio * Mathf.Cos( Mathf.PI/2 - (graus * i) ) , (Screen.height/2 + 5*size) + raio * Mathf.Sin(  Mathf.PI/2  - (graus * i) ), 5 * size, 5 * size);
//
//					
//					if(GUI.Button(position, "")) {
//						//Caso escolheu um item de ataque para colocar no jogo
//						attackItem = false;
//						
//						//Marca nome do mesmo selecionado para ser usado posteriormente para adicionar na fase
//						selectedObject = node;
//						
//						//Muda estado para confirmar a selecao e posicionar o mesmo
//						confirmSelect = true;
//					}
//					//Desenha botao
//					Debug.Log(node.name);
//					GUI.DrawTexture(position, (Texture) Util.LoadImageResource(node.name), ScaleMode.StretchToFill, true, size);
//					
//					//Desenha label com quantidade disponivel
//					GUIStyle style = new GUIStyle();
//					style.fontSize = (int)(size);
//					style.fontStyle = FontStyle.Bold;
//					style.normal.textColor = Color.white;
//					GUI.DrawTexture(new Rect(position.x+3.5F*size, position.y+3.5F*size, 1.4F*size, 1.4F*size),   (Texture) Util.LoadImageResource("quantityBack"), ScaleMode.StretchToFill, true, size);
//					GUI.Box(new Rect(position.x+3.7F*size, position.y+3.6F*size, size, size), node.quantity.ToString(),  style); 
//					
//					i++;
//					
//				}
//
//			}
//			else if(upgradeItem){
//				//Lista informacoes de tipos de itens de upgrades  que o jogador possui.
//				List<HeroNode> itensExisting = Player.getItensUpgrade();
//				
//				
//				//Divide os 150 graus entre os botoes existente
//				float graus =(5 * Mathf.PI) / (6 * itensExisting.Count); 
//				
//				int  i = 0;
//				//Percorre a lista para colocar na tela os botoes desses itens.
//				foreach(HeroNode node in itensExisting){
//					//Posicao para ficar o botao desse item
//					Rect position = new Rect((Screen.width - 6.0F* size) - raio * Mathf.Cos( Mathf.PI/2 - (graus * i) ) , (Screen.height/2 - 5*size) - raio * Mathf.Sin(  Mathf.PI/2  - (graus * i) ), 5 * size, 5 * size);
//					
//					
//					if(GUI.Button(position, "")) {
//						//Caso escolheu um item de upgrade para colocar no jogo
//						upgradeItem = false;
//						
//						//Marca nome do mesmo selecionado para ser usado posteriormente para adicionar na fase
//						selectedObject = node;
//						
//						//Muda estado para confirmar a selecao e posicionar o mesmo
//						confirmSelect = true;
//					}
//					//Desenha botao
//					GUI.DrawTexture(position, (Texture) Util.LoadImageResource(node.name), ScaleMode.StretchToFill, true, size);
//					
//					//Desenha label com quantidade disponivel
//					GUIStyle style = new GUIStyle();
//					style.fontSize = (int)(size);
//					style.fontStyle = FontStyle.Bold;
//					style.normal.textColor = Color.white;
//					GUI.DrawTexture(new Rect(position.x+3.5F*size, position.y+3.5F*size, 1.4F*size, 1.4F*size),   (Texture) Util.LoadImageResource("quantityBack"), ScaleMode.StretchToFill, true, size);
//					GUI.Box(new Rect(position.x+3.7F*size, position.y+3.6F*size, size, size), node.quantity.ToString(),  style); 
//					
//					i++;
//					
//				}
//
//			}
//			else if(cureItem){
//				//Lista informacoes de tipos de itens de curar  que o jogador possui.
//				List<HeroNode> itensExisting = Player.getItensCure();
//				
//				
//				//Divide os 180 graus entre os botoes existente
//				float graus =( Mathf.PI) / (itensExisting.Count); 
//				
//				int  i = 0;
//				//Percorre a lista para colocar na tela os botoes desses itens.
//				foreach(HeroNode node in itensExisting){
//					//Posicao para ficar o botao desse item
//					Rect position = new Rect((Screen.width - 6.0F* size - 5.0F* size) - raio * Mathf.Cos( Mathf.PI/3 - Mathf.PI/6 - (graus * i) ) , (Screen.height/2 ) - raio * Mathf.Sin(  Mathf.PI/3 - Mathf.PI/6  - (graus * i) ), 5 * size, 5 * size);
//					
//					
//					if(GUI.Button(position, "")) {
//						//Caso escolheu um item de upgrade para colocar no jogo
//						cureItem = false;
//						
//						//Marca nome do mesmo selecionado para ser usado posteriormente para adicionar na fase
//						selectedObject = node;
//						
//						//Muda estado para confirmar a selecao e posicionar o mesmo
//						confirmSelect = true;
//					}
//					//Desenha botao
//					GUI.DrawTexture(position, (Texture) Util.LoadImageResource(node.name), ScaleMode.StretchToFill, true, size);
//					
//					//Desenha label com quantidade disponivel
//					GUIStyle style = new GUIStyle();
//					style.fontSize = (int)(size);
//					style.fontStyle = FontStyle.Bold;
//					style.normal.textColor = Color.white;
//					GUI.DrawTexture(new Rect(position.x+3.5F*size, position.y+3.5F*size, 1.4F*size, 1.4F*size),   (Texture) Util.LoadImageResource("quantityBack"), ScaleMode.StretchToFill, true, size);
//					GUI.Box(new Rect(position.x+3.7F*size, position.y+3.6F*size, size, size), node.quantity.ToString(),  style); 
//					
//					i++;
//					
//				}
//
//			}
//		}
//	}
//
//	//Objeto que cuida das posicoes possiveis de instanciar algo no jogo
//	private InstantiationSystem instSystem=null;
//
//	/* Metodo responsavel pelo menu por confirmar uma selecao, instanciando o objeto no centro da tela.
//	 * Desativa os menus, colocando botao de confirmar ou negar.
//	 */
//	void confirmSelection(){
//		//Cria feedback de onde pode instanciar se nao for pra curar wall E nao tiver.
//		if (instSystem == null && !selectedObject.name.Equals("CureWall")) { 
//
//
//			//Instancia o prefab que realiza a funcao de permitir instanciacao e efeitos
//			GameObject obj = Instantiate((GameObject)Util.LoadPrefabResource("InstantiationSystem"), Vector3.zero, Quaternion.identity) as GameObject; 
//
//			//Pega o objeto do prefab que implementa as funcoes.
//			instSystem = obj.GetComponent(typeof(InstantiationSystem)) as InstantiationSystem ;
//
//			bool isTelep = false;
//			string objName;
//			if(selectedObject.name.Equals("UpgradeSpeed") &&  (leftMenuOn == true && rightMenuOn == false) ){
//				objName = UpgradeSpeed.heroName();
//				isTelep = true;
//			}
//			else
//				objName = selectedObject.name;
//
//			instSystem.startInstantiationSystem(objName, isTelep);
//
//		}
//
//		//Coloca mira no centro do device, caso nao seja curar muralha.
//		if(!selectedObject.name.Equals("CureWall")){
//			Texture aimTex = (Texture) Util.LoadImageResource("aim");
//			GUI.DrawTexture(new Rect(Screen.width/2.0f - 1.5f*size , Screen.height/2.0f - 1.5f*size , 3.0F* size, 3.0F* size), aimTex ,ScaleMode.StretchToFill, true, size);
//		}
//
//		Rect confirmPosition;
//		Rect cancelPosition;
//
//		//Primeiramente, verificaremos qual menu foi selecionado para posicionar botoes de confirmar
//		if(leftMenuOn == true && rightMenuOn == false){ //Caso menu da esquerda(exercito) teve algum objeto selecionado
//			confirmPosition = new Rect(Screen.width - 6.0F* size, Screen.height/2, 5.0F* size, 5.0F* size);
//			cancelPosition  = new Rect(0,Screen.height/2, 5 * size,5 * size);
//		}
//		else if(leftMenuOn == false && rightMenuOn == true){ //Caso menu da direita(item) teve algum objeto selecionado
//			cancelPosition 	= new Rect(Screen.width - 6.0F* size, Screen.height/2, 5.0F* size, 5.0F* size);
//			confirmPosition = new Rect(0,Screen.height/2, 5 * size,5 * size);
//
//
//		}
//		else{ //Caso de erro, entrou no confirmar sem ter nenhum selecionado
//			//Sai do estado de confirmar, voltando tudo para estado incial.
//			confirmSelect = false;
//			leftMenuOn 	  = false;
//			rightMenuOn   = false;
//
//			selectedObject = null;
//
//			return;
//		}
//
//		//Botao de confirmar
//		if(
//			GUI.Button(confirmPosition, (Texture) Util.LoadImageResource("confirm"), "")
//		   ){
//			Debug.Log("Cofirmou");
//
//			//Caso deu um erro, pois nao temos objeto nenhum para instanciar.
//			if(selectedObject == null){ 
//				//Sai do estado de confirmar, voltando tudo para estado incial.
//				confirmSelect = false;
//				leftMenuOn 	  = false;
//				rightMenuOn   = false;
//
//				selectedObject = null;
//				return;
//			}
//			//Quando selecionado um item
//			if(rightMenuOn){ 
//				Debug.Log("right");
//				switch (selectedObject.name){
//				case "AttackFire": //Ataque direto de fogo em um ponto
//
//					break;
//				case "AttackThunder": //attack direto de trovao em um ponto
//					if( instSystem != null && instSystem.instantiate() == true ){
//						Player.removeItemOfClass(Barracks.ItemClass.AttackThunder); //Remove o item adicionado na tela.
//						confirmSelect = false;
//						leftMenuOn 	  = false;
//						rightMenuOn   = false;
//						
//						selectedObject = null; 
//						
//						Destroy(instSystem);
//						instSystem = null;
//					}
//					break;
//				case "AttackBlackHole": //ataque matar todos em campo
//					if( instSystem != null && instSystem.instantiate() == true ){
//						Player.removeItemOfClass(Barracks.ItemClass.AttackBlackHole); //Remove o item adicionado na tela.
//						confirmSelect = false;
//						leftMenuOn 	  = false;
//						rightMenuOn   = false;
//						
//						selectedObject = null; 
//						
//						Destroy(instSystem);
//						instSystem = null;
//					}
//					break;
//
//				case "UpgradeSpeed": //Heroi se TELETRANSPORTA
//					if( instSystem != null && instSystem.instantiate() == true){
//						//Remove o item adicionado na tela.
//						Player.removeItemOfClass(Barracks.ItemClass.UpgradeSpeed); 
//
//						//Habilita para adicionar um heroi na tela
//						confirmSelect = true;
//						leftMenuOn 	  = true;
//						rightMenuOn   = false;
//
//						Destroy(instSystem);
//						instSystem = null;
//					}
//
//					break;
//
//				case "UpgradeDefense": //Defesa maxima para um heroi
//
//					break;
//
//				case "UpgradeForce": //Forca absoluta para todos herois
//
//					break;
//
//				case "CureWall": //Curar sua muralha 
//					Instantiate( (GameObject)Util.LoadPrefabResource("CureWall"),transform.position, Quaternion.identity );
//					Player.removeItemOfClass(Barracks.ItemClass.CureWall); //Remove o item adicionado na tela.
//					confirmSelect = false;
//					leftMenuOn 	  = false;
//					rightMenuOn   = false;
//					
//					selectedObject = null; 
//
//
//					break;
//				
//				case "CureCharacter": //Curar um heroi
//					if( instSystem != null && instSystem.instantiate() == true ){
//						Player.removeItemOfClass(Barracks.ItemClass.CureCharacter); //Remove o item adicionado na tela.
//						confirmSelect = false;
//						leftMenuOn 	  = false;
//						rightMenuOn   = false;
//						
//						selectedObject = null; 
//						
//						Destroy(instSystem);
//						instSystem = null;
//					}
//					break;
//
//				default: //Erro, nenhum item existente ou algo do tipo. 
//					break;
//				}
//
//			}
//			//Quando selecionado um heroi
//			else{ 					
//
//				if( instSystem != null && instSystem.instantiate() == true ){
//					//Caso em que nao é teletransporte, logo, instanciou um novo heroi.
//					if(!instSystem.isTeleportation){
//						//Remove heroi do exercito guardado no player
//						Player.removeHeroOfClass(selectedObject.name); 
//					}
//
//					//Retorna todas flags para estado incial
//					confirmSelect = false;
//					leftMenuOn 	  = false;
//					rightMenuOn   = false;
//					
//					selectedObject = null; 
//					
//					Destroy(instSystem);
//					instSystem = null;
//				}
//			}
//
//		}
//		//Apenas mostra o botao de negar quando nao for a escolha da nova posicao
//		//do heroi. (Apenas pode confirmar, visto que ja gastou o item)
//		else if(selectedObject.name != "UpgradeSpeed" || (leftMenuOn == false && rightMenuOn == true)){ //Note que esta em leftMenu ligado e um objeto de item, diz ser a confirmacao da nova posicao do heroi.
//			//Botao de negar
//			if(GUI.Button(cancelPosition, (Texture) Util.LoadImageResource("cancel"), "")){
//				//Sai do estado de confirmar, voltando tudo para estado incial.
//				confirmSelect = false;
//				leftMenuOn 	  = false;
//				rightMenuOn   = false;
//
//				selectedObject = null; 
//				Destroy(instSystem);
//				instSystem = null;
//
//			}
//		}
//	}
//	
//
//}
