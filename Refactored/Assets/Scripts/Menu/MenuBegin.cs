﻿using UnityEngine;
using System.Collections;

public class MenuBegin : MonoBehaviour {
	/*Imagem de background*/
	Texture firstScreenBack;
	Rect	firstScreenBackPos;

	/*Variaveis para informacao de confirmacao de novo jogo que anulara antigo salvo*/
	Texture confirmPerg;
	Rect	confirmPergPos;
	Texture confirmButton;
	Rect	confirmButtonPos;
	bool	clickedConfirm = false;
	Texture denyButton;
	Rect	denyButtonPos;
	bool	clickedDeny	= false;
	string 	confirHeader;
	Rect	confirmHeaderPos;
	string	infoToConfirm;
	Rect	infoToConfirmPos;


	/*Variaveis para controle do botao de novo jogo*/
	Texture newGameButton;
	Rect	newGameButtonPos;
	bool	clickedNewGame = false;
	bool	confirmNewGame = false; //Botao que indica estado para confirmacao de new game, visto que ja possui um jogo salvo.

	/*Variaveis para controle do botao de continuar*/
	bool isThereSavedGame; 
	Texture continueButton;
	Rect	continueButtonPos;
	bool	clickedContinue = false;


	/*Variaveis para controle de botao de como jogar*/
	Texture howToPlayButton;
	Rect	howToPlayButtonPos;
	bool	clickedHowToPlay = false;

	/*Variaveis para controle de botao de creditos*/
	Texture creditsButton;
	Rect	creditsButtonPos;
	bool	clickedCredits = false;

	/**Variavel para realizar efeito de pressionar botao**/
	float timePassed = 0; //Controla o tempo pasado de ate um dado frame
	const float threshold = 0.05F;//Limitante de tempo para efeito de botao pressionar

	/*Estilo de texto */
	GUIStyle style = new GUIStyle();

	void Awake(){
		//Inicializa informacoes uteis para o jogo 
		HashIDs.fillHashIDs (); 
		STR.initiate ();
		AppropriateDistancesForFight.fillDictionary ();

		isThereSavedGame = Database.loadInitialValuesAndCheckDatabaseExistence ();

		//Inicializa imagem de fundo
		firstScreenBack 	= (Texture)Util.LoadImageResource (STR.Resources.FIRST_SCREEN_BACKGROUND);
		firstScreenBackPos = new Rect (0, 0, Screen.width, Screen.height);

		//Inicializa botao de new Game
		newGameButton		= (Texture)Util.LoadImageResource (STR.TranslatedResources.NEW_GAME);
		newGameButtonPos 	= new Rect (Screen.width / 2 - (newGameButton.width * Screen.width / 2048) / 2, 0.56f*Screen.height - (newGameButton.height * Screen.height / 1536), newGameButton.width * Screen.width / 2048, newGameButton.height * Screen.height / 1536);

		//Cria espacoes fixos entre os botoes
		float space = newGameButtonPos.height / 3;

		//Inicializa botao de continue (inicializamos o continue na mesma posicao do newgame, pois se nao existi-lo, o botao abaixo fica certo)
		continueButton 		= (Texture)Util.LoadImageResource (STR.TranslatedResources.CONTINUE);
		continueButtonPos	= new Rect (newGameButtonPos.x, newGameButtonPos.y, newGameButtonPos.width, newGameButtonPos.height);
		//Caso possuo o botao de continue, seta posicao abaixo do new game
		if(isThereSavedGame)
			continueButtonPos.y += continueButtonPos.height + space;

		//Inicializa botao de como jogar
		howToPlayButton		= (Texture)Util.LoadImageResource (STR.TranslatedResources.HOW_TO_PLAY);
		howToPlayButtonPos  = new Rect (continueButtonPos.x, continueButtonPos.y + continueButtonPos.height + space, continueButtonPos.width, continueButtonPos.height);

		//Inicializa botao de creditos
		creditsButton		= (Texture)Util.LoadImageResource (STR.TranslatedResources.CREDITS);
		creditsButtonPos 	= new Rect (howToPlayButtonPos.x, howToPlayButtonPos.y + howToPlayButtonPos.height + space, howToPlayButtonPos.width, howToPlayButtonPos.height);


		/*Inicializa informacao de confirmacao de new game*/
		confirmPerg 		= (Texture)Util.LoadImageResource (STR.Resources.CONFIRM_POPUP_BACKGROUND);
		confirmPergPos 		= new Rect (Screen.width / 2 - (confirmPerg.width * Screen.width / 2048) / 2, Screen.height / 2 - (confirmPerg.height * Screen.height / 1536) / 2, confirmPerg.width * Screen.width / 2048, confirmPerg.height * Screen.height / 1536);

		float spaceBetInfo = confirmPergPos.height / 6;
		//Inicializa header
		confirHeader 		= STR.get ("DeleteSavedGame");
		confirmHeaderPos 	= new Rect (confirmPergPos.x, confirmPergPos.y, confirmPergPos.width,confirmPergPos.height/6);

		//Inicializar informacao
		infoToConfirm		= STR.get ("NewAndDeleteSavedGame");
		infoToConfirmPos 	= new Rect (confirmHeaderPos.x + confirmHeaderPos.width/14, confirmHeaderPos.y + confirmHeaderPos.height + spaceBetInfo, confirmPergPos.width - confirmHeaderPos.width/8, confirmPergPos.height / 6);

		//Botao de confirmar
		confirmButton		= (Texture)Util.LoadImageResource (STR.Resources.CONFIRM_BUTTON);
		float x = confirmHeaderPos.x + confirmHeaderPos.width/2 + confirmHeaderPos.width/8;
		confirmButtonPos	= new Rect (x - (140 * Screen.width/2048)/2, infoToConfirmPos.y + infoToConfirmPos.height + 0.7f*spaceBetInfo,140 * Screen.width/2048, 140 * Screen.height / 1536);

		//Botao de negar
		denyButton		= (Texture)Util.LoadImageResource (STR.Resources.DENY_BUTTON);
		x = confirmHeaderPos.x + confirmHeaderPos.width/2 - confirmHeaderPos.width/8;
		denyButtonPos	= new Rect (x - (140 * Screen.width/2048)/2, infoToConfirmPos.y + infoToConfirmPos.height + 0.7f*spaceBetInfo,140 * Screen.width/2048, 140 * Screen.height / 1536);

//		//sounds
//		/*inicializacao da parte sonora*/
//		if (SoundController.isMusicOff() == true) {
//			AudioListener listener = FindObjectOfType(typeof(AudioListener)) as AudioListener;
//			listener.enabled = false;
//		}

	}

	void Update (){

		timePassed += Time.deltaTime;
	}

	void OnGUI(){
		//Retira marca de botao
		GUI.backgroundColor = Color.clear; 

		//Coloca imagem de background
		GUI.DrawTexture (firstScreenBackPos, firstScreenBack);

		if(confirmNewGame)
			drawConfirmNewGame();
		else{
			drawNeGameButton ();

			if(isThereSavedGame)
				drawContinueButton ();

			drawHowToPlayButton ();

			drawCreditsButton ();
		}
	}

	void drawNeGameButton(){
		if(GUI.RepeatButton (newGameButtonPos, "")){
			//Vai para estado pressionado
			newGameButton	= (Texture)Util.LoadImageResource (STR.TranslatedResources.NEW_GAME_PRESSED);
			clickedNewGame 	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(newGameButtonPos, newGameButton);
			
		}else if(clickedNewGame && timePassed > threshold){
			//Faz operacao
			//Caso tenha jogo salvo, coloco no estado de confirmacao da acao, pois matara antigo saved
			if(isThereSavedGame)
				confirmNewGame = true;
			//Caso contrario, primeira vez que executa o jogo no device (MOSTRAR TUTORIAL DE TARGET)
			else{

				GameController.firstPlay = true;
				GameController.firstRunDevice = true;

				Database.createNewDatabase();
				
				Application.LoadLevel("MainMenu");

			}

			//Volta para estado despressionado
			newGameButton	= (Texture)Util.LoadImageResource (STR.TranslatedResources.NEW_GAME);
			clickedNewGame 	= false;
			
			GUI.DrawTexture(newGameButtonPos, newGameButton);

		}else
			GUI.DrawTexture(newGameButtonPos, newGameButton);

	}

	void drawContinueButton(){
		if(GUI.RepeatButton (continueButtonPos, "")){
			//Vai para estado pressionado
			continueButton 		= (Texture)Util.LoadImageResource (STR.TranslatedResources.CONTINUE_PRESSED);
			clickedContinue 	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(continueButtonPos, continueButton);
			
		}else if(clickedContinue && timePassed > threshold){
			//Faz operacao
			Database.loadExistentDatabase();

			GameController.firstPlay = false;
			GameController.firstRunDevice = false;

			Application.LoadLevel("MainMenu");

			//Volta para estado despressionado
			continueButton 		= (Texture)Util.LoadImageResource (STR.TranslatedResources.CONTINUE);
			clickedContinue 	= false;
			
			GUI.DrawTexture(continueButtonPos, continueButton);

		}else
			GUI.DrawTexture(continueButtonPos, continueButton);

	}

	void drawHowToPlayButton(){
		if(GUI.RepeatButton (howToPlayButtonPos, "")){
			//Vai para estado pressionado
			howToPlayButton		= (Texture)Util.LoadImageResource (STR.TranslatedResources.HOW_TO_PLAY_PRESSED);
			clickedHowToPlay 	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(howToPlayButtonPos, howToPlayButton);
			
		}else if(clickedHowToPlay && timePassed > threshold){
			//Faz operacao

			Application.LoadLevel("HowToPlay");

			//Volta para estado despressionado
			howToPlayButton		= (Texture)Util.LoadImageResource (STR.TranslatedResources.HOW_TO_PLAY);
			clickedHowToPlay 	= false;
			
			GUI.DrawTexture(howToPlayButtonPos, howToPlayButton);

		}else
			GUI.DrawTexture(howToPlayButtonPos, howToPlayButton);

	}

	void drawCreditsButton(){
		if(GUI.RepeatButton (creditsButtonPos, "")){
			//Vai para estado pressionado
			creditsButton	= (Texture)Util.LoadImageResource (STR.TranslatedResources.CREDITS_PRESSED);
			clickedCredits 	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(creditsButtonPos, creditsButton);
			
		}else if(clickedCredits && timePassed > threshold){
			//Faz operacao

			Application.LoadLevel("CreditScene");

			//Volta para estado despressionado
			creditsButton	= (Texture)Util.LoadImageResource (STR.TranslatedResources.CREDITS);
			clickedCredits 	= false;
			
			GUI.DrawTexture(creditsButtonPos, creditsButton);

		}else
			GUI.DrawTexture(creditsButtonPos, creditsButton);

	}

	void drawConfirmNewGame(){
		//Coloca apenas imagem dos botoes, sem terem acoes ao clicar.
		GUI.DrawTexture (newGameButtonPos, newGameButton);
		GUI.DrawTexture (continueButtonPos, continueButton);
		GUI.DrawTexture (howToPlayButtonPos, howToPlayButton);
		GUI.DrawTexture (creditsButtonPos, creditsButton);

		//Coloca pergaminho na frente que devera ter a info e opcao de confirmar
		GUI.DrawTexture (confirmPergPos, confirmPerg);

		//Fonte usada
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);

		//Header
		style.fontSize = (int)(0.3f*Screen.height/6);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
		style.alignment = TextAnchor.UpperCenter;

		GUI.Label (confirmHeaderPos, confirHeader, style);
		
		//Informacao para saber se deseja fazer delecao
		style.fontStyle = FontStyle.Normal;
		style.fontSize = (int)(Screen.height / 22);
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleCenter;
		style.wordWrap = true;
			
		GUI.TextField (infoToConfirmPos, infoToConfirm, style);

		//Coloca botao de confirmar acao
		drawConfirmButton ();

		//Coloca botao de negar acao
		drawDenyButton ();

	}

	void drawConfirmButton(){
		if(GUI.RepeatButton (confirmButtonPos, "")){
			//Vai para estado pressionado
			confirmButton 	= (Texture)Util.LoadImageResource (STR.Resources.CONFIRM_BUTTON_PRESSED);
			clickedConfirm 	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(confirmButtonPos, confirmButton);
			
		}else if(clickedConfirm && timePassed > threshold){
			//Faz operacao para carregar novo jogo matando antigo
			GameController.firstPlay = true;
			GameController.firstRunDevice = false;
			Database.createNewDatabase();

			Application.LoadLevel("MainMenu");
			
			//Volta para estado despressionado
			confirmButton 	= (Texture)Util.LoadImageResource (STR.Resources.CONFIRM_BUTTON);
			clickedConfirm 	= false;
			
			GUI.DrawTexture(confirmButtonPos, confirmButton);

		}else
			GUI.DrawTexture(confirmButtonPos, confirmButton);
	}

	void drawDenyButton(){
		if(GUI.RepeatButton (denyButtonPos, "")){
			//Vai para estado pressionado
			denyButton 	= (Texture)Util.LoadImageResource (STR.Resources.DENY_BUTTON_PRESSED);
			clickedDeny	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(denyButtonPos, denyButton);
			
		}else if(clickedDeny && timePassed > threshold){
			//Faz operacao para remover estado de confirmacao
			confirmNewGame = false;

			//Volta para estado despressionado
			denyButton 	= (Texture)Util.LoadImageResource (STR.Resources.DENY_BUTTON);
			clickedDeny 	= false;
			
			GUI.DrawTexture(denyButtonPos, denyButton);

		}else
			GUI.DrawTexture(denyButtonPos, denyButton);

	}
}
