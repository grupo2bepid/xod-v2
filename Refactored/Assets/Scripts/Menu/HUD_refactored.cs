using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class buttonMenu{
	public Texture	icon;
	public int		count;
	public bool		clicked;
	public buttonMenu[] subButton;
};

public class HUD_refactored : MonoBehaviour {
	public bool callEndScene = false; //MIGUE PARA SEGURAR O JOGO APOS ACABAR


	public delegate void OnMouseAction(Vector2 mpos);
	public event OnMouseAction onMouseAction;
	public string pointExclusivelyTo = "";
	public bool runningTutorial = false; //migue
	
	private Vector2 lastMousePosition;
	
	
	/*Variavel especial para cuidar do caso do item de Teletransporte*/
	bool isThereDenyButton = true; //caso geral q temos o botao de confirmar
	
	/**Variaveis do menu esquerdo (exercito)**/
	buttonMenu armyMenu  = new buttonMenu();//menu esquerdo, exercito
	bool leftMenuOn		 = false;
	Texture backMenuHero;
	bool backMenuClicked = false;
	Rect armyPos;
	
	/**Variaveis do menu direito (item)**/
	public buttonMenu itemMenu = new buttonMenu();//menu direito, item
	bool rightMenuOn	= false;
	Texture backMenuItem;
	Rect itemPos;
	
	/**Variaveis de controle de boss na tela***/
	private Boss[] bosses;	//Objeto de controle do boss, pode ter mais de um	
	public static bool isBossActived;	//Booleano para avisar de boss esta ativo	
	Rect lifeBossIconPos;//Posicao do primeiro boss na tela
	Rect lifeBossBarPos;//Posicao do primeiro boss na tela
	Texture lifeBossIcon;	//Imagens do boss 
	Texture lifeBossBar;	// obs: (ISSO PRECISARIA SER UM ARRAY PARA TER VARIOS BOSS e MANTER UM PADRAO DE NOME PARA CARRGARMOS IMAGENS)
	
	/**Variaveis de vida do cristal na tela**/
	Cristal myCrystal; //variavel que guarda o objeto do cristal na tela.
	Rect lifeCristalIconPos;
	Rect lifeCristalBarPos;
	Texture lifeCristalIcon;//Imagens de icones
	Texture lifeCristalBar; //obs: nao estou usando o bar
	
	/**Variaves para quando o jogo fica parado (perdeu target)*/
	string labelPaused ;
	Rect labelPausedPos;
	Rect backMainButtonPos;
	Rect soundButtonPos;
	Rect effectButtonPos;
	Rect menuPausedPos;
	Texture menuPaused;
	Texture backMainButton;
	Texture soundOnButton;
	Texture soundOffButton;
	Texture soundButton;
	Texture effectOnButton;
	Texture effectOffButton;
	Texture effectButton;
	//bool displayLabel 			= false; //NAO UTILIZADO
	//bool backMainButtonClicked	= false;
	bool paused 				= true; //booleano que diz se esta jogo esta parado(perdeu target)
	
	/*Variaveis para confirmacao de acao de abandonar batalha*/
	bool confirmQuit = false;
	Texture confirmPerg;
	Rect	confirmPergPos;
	//Texture confirmButton;
	Rect	confirmButtonPos;
	//bool	clickedConfirm = false;
	//Texture denyButton;
	Rect	denyButtonPos;
	//bool	clickedDeny	= false;
	string 	confirHeader;
	Rect	confirmHeaderPos;
	string	infoToConfirm;
	Rect	infoToConfirmPos;
	
	/**Variaveis usadas para estado de confirmacao**/
	Barracks.HeroClass heroSelected;//Heroi selecionado
	Barracks.ItemClass itemSelected;//Item selecionado
	bool confirmSelectHero 	 = false;
	bool confirmSelectItem	 = false;
	InstantiationSystem instSystem = null;	//Instation objeto
	GameObject instantiationSystemPrefab;
	Texture confirmButton;
	Texture denyButton;
	bool confirmButtonClicked		= false;
	bool denyButtonClicked			= false;
	
	/**Variaveis para fazer alvo na tela**/
	Rect aimPos;
	Texture aim;
	
	/**Variavel para realizar efeito de pressionar botao**/
	float timePassed = 0; //Controla o tempo pasado de ate um dado frame
	const float threshold = 0.1F;//Limitante de tempo para efeito de botao pressionar
	const float piscarTempo = 2.0F;
	
	//sounds
	AudioSource effects;
	
	
	//Variavel para desenhar estilo
	GUIStyle style = new GUIStyle();
	
	//Imagem de qauntidade de um dado item/heroi
	Texture quantityItemIcon;
	
	void Update (){
		timePassed += Time.deltaTime;
		
		if(callEndScene)
			Invoke("loadEndScene", 3.0f);
	
	}
	
	void loadEndScene(){
		Application.LoadLevel ("MenuEndGame");
			
	}
	
	void Start (){
		//Inicializa posicao dos botoes de menu
		armyPos = new Rect(0, Screen.height/2 - (armyMenu.icon.height * Screen.height/1536)/2, armyMenu.icon.width * Screen.width/2048, armyMenu.icon.height * Screen.height/1536);
		itemPos = new Rect(Screen.width - (armyMenu.icon.width * Screen.width/2048), Screen.height/2 - (armyMenu.icon.height * Screen.height/1536)/2, armyMenu.icon.width * Screen.width/2048, armyMenu.icon.height * Screen.height/1536);
		
		//Pega o cristal
		myCrystal   = (Cristal)FindObjectOfType (typeof(Cristal));
		
		
		/*Comeca todos estados de botoes clicados de heroi e itens como falso*/
		armyMenu.clicked = false;
		for (int i =0; i < armyMenu.subButton.Length; i++)
			armyMenu.subButton [i].clicked = false;
		
		itemMenu.clicked = false;
		for(int i=0; i < itemMenu.subButton.Length; i++)
			itemMenu.subButton[i].clicked = false;
		
		
		//Inicializa phase sem o boss
		isBossActived = false;
		
		//NAO UTILIZADO
		//displayLabel = true;
		
		
	}
	
	void Awake(){
		//Migue para pegar itens na persistencia
		GameController.hudCurrente = this;
		
		labelPaused = STR.get ("NoTarget");
		
		/*** Inicializa Menu heroi ***/
		//Inicializa texture do heroi menu
		armyMenu.icon = (Texture)Util.LoadImageResource (STR.Resources.ARMY_MENU);
		
		//Inicializa botoes do submenu de heroi
		armyMenu.subButton = new buttonMenu[4]; //Temos 4 tipos de heroi.
		
		//Inicializa quantidade e texture de cada botao de heroi
		armyMenu.count = 0;
		
		
		for(int i =0; i < armyMenu.subButton.Length; i++){
			//Pega heroi equivalente
			Barracks.HeroClass cls = (Barracks.HeroClass)((int)Barracks.HeroClass.Fighter + i);
			
			//Coloca texture
			armyMenu.subButton[i] = new buttonMenu();
			
			armyMenu.subButton[i].icon  =  Util.LoadImageResource(Barracks.getNameOfClass(cls) +  STR.get(STR.Resources.BUTTON_SUFFIX)) as Texture;
			
			//Coloca quantidade
			armyMenu.subButton[i].count = Barracks.getHeroCount(cls);
			
			
			//Incrementa numero totais de heroi
			armyMenu.count += armyMenu.subButton[i].count;
			
			//sounds
			effects = gameObject.AddComponent<AudioSource> ();
		}
		
		
		/*** Inicializa Menu Item ***/
		//Inicializa primeiro menu
		itemMenu.icon = (Texture)Util.LoadImageResource (STR.Resources.ITEM_MENU);
		itemMenu.subButton = new buttonMenu[8]; //Temos 8 tipos de item.
		itemMenu.count = 0;
		
		for(int i =0; i < itemMenu.subButton.Length; i++){
			//Pega heroi equivalente
			Barracks.ItemClass cls = (Barracks.ItemClass)((int)Barracks.ItemClass.UpgradeDefense + i);
			
			//Coloca texture
			itemMenu.subButton[i] = new buttonMenu();
			itemMenu.subButton[i].icon  =  Util.LoadImageResource(Barracks.getNameOfClassItem(cls) +  STR.get(STR.Resources.BUTTON_SUFFIX)) as Texture;
			
			//Coloca quantidade
			itemMenu.subButton[i].count = Barracks.getItemCount(cls);
			
			
			//Incrementa numero totais de heroi
			itemMenu.count += itemMenu.subButton[i].count;
		}
		
		
		instantiationSystemPrefab = Util.LoadPrefabResource ("InstantiationSystem") as GameObject;
		
		
		
		//Inicia imagem icone
		lifeCristalIcon = (Texture)Util.LoadImageResource (STR.Resources.LIFE_CRYSTAL_ICON);
		lifeCristalBar 	= (Texture)Util.LoadImageResource (STR.Resources.LIFE_CRYSTAL_BAR);
		lifeBossIcon	= (Texture)Util.LoadImageResource (STR.Resources.LIFE_BOSS_ICON);
		lifeBossBar 	= (Texture)Util.LoadImageResource (STR.Resources.LIFE_BOSS_BAR);
		quantityItemIcon	= (Texture) Util.LoadImageResource(STR.Resources.QUANTITY_BACKGROUND);
		
		//Inicia imagens de botao que nao eh item/heroi
		confirmButton	= (Texture)Util.LoadImageResource (STR.Resources.CONFIRM_BUTTON);
		denyButton		= (Texture)Util.LoadImageResource (STR.Resources.DENY_BUTTON);
		backMainButton = (Texture)Util.LoadImageResource (STR.TranslatedResources.QUIT_BATTLE);
		soundOnButton		= (Texture)Util.LoadImageResource (STR.Resources.SOUND_BUTTON);
		soundOffButton		= (Texture)Util.LoadImageResource (STR.Resources.SOUND_BUTTON_PRESSED);
		effectOnButton	= (Texture)Util.LoadImageResource (STR.Resources.EFFECTS_BUTTON);
		effectOffButton	= (Texture)Util.LoadImageResource (STR.Resources.EFFECTS_BUTTON_PRESSED);
		backMenuHero	= (Texture)Util.LoadImageResource (STR.Resources.BACK_MENU_HERO);
		backMenuItem	= (Texture)Util.LoadImageResource (STR.Resources.BACK_MENU_ITEM);
		aim				= (Texture)Util.LoadImageResource (STR.Resources.AIM);
		
		
		//Seta posicao de menu de quando perde o target e para o jogo
		labelPausedPos		=  new Rect (Screen.width/6, Screen.height/4, 0.7f*Screen.width,  Screen.height/4);
		backMainButtonPos	=  new Rect (Screen.width/2 - (backMainButton.width*Screen.width/2048)/2, labelPausedPos.y + labelPausedPos.height, backMainButton.width * Screen.width / 2048, backMainButton.height * Screen.height / 1536);
		soundButtonPos		=  new Rect (backMainButtonPos.x, backMainButtonPos.y + backMainButtonPos.height + labelPausedPos.height/10, soundOffButton.width * Screen.width / 2048, soundOffButton.height * Screen.height / 1536);
		effectButtonPos		=  new Rect(soundButtonPos.x, soundButtonPos.y + soundButtonPos.height + labelPausedPos.height/10, effectOffButton.width * Screen.width / 2048, effectOffButton.height * Screen.height / 1536);
		
		/*Inicializa textura de sons com estado atual*/
		if(SoundController.isEffectOff())//Verifica estado do som, para inicializar textura
			effectButton = effectOffButton;
		else 
			effectButton = effectOnButton;
		
		if(SoundController.isMusicOff())//Verifica estado do som, para inicializar textura
			soundButton = soundOffButton;
		else 
			soundButton = soundOnButton;
		
		
		
		/*Inicializa informacao de confirmacao de Abandono de jogo*/
		confirmPerg 		= (Texture)Util.LoadImageResource (STR.Resources.CONFIRM_POPUP_BACKGROUND);
		confirmPergPos 		= new Rect (Screen.width / 2 - (confirmPerg.width * Screen.width / 2048) / 2, Screen.height / 2 - (confirmPerg.height * Screen.height / 1536) / 2, confirmPerg.width * Screen.width / 2048, confirmPerg.height * Screen.height / 1536);
		
		float spaceBetInfo = confirmPergPos.height / 6;
		//Inicializa header
		confirHeader 		= STR.get ("AbandonBattle");
		confirmHeaderPos 	= new Rect (confirmPergPos.x, confirmPergPos.y, confirmPergPos.width,confirmPergPos.height/6);
		
		//Inicializar informacao
		infoToConfirm		= STR.get ("ConfirmQuitBattle");;
		infoToConfirmPos 	= new Rect (confirmHeaderPos.x + confirmHeaderPos.width/14, confirmHeaderPos.y + confirmHeaderPos.height + spaceBetInfo, confirmPergPos.width - confirmHeaderPos.width/8, confirmPergPos.height / 6);
		
		//Botao de confirmar
		float x = confirmHeaderPos.x + confirmHeaderPos.width/2 + confirmHeaderPos.width/8;
		confirmButtonPos	= new Rect (x - (140 * Screen.width/2048)/2, infoToConfirmPos.y + infoToConfirmPos.height + 0.7f*spaceBetInfo,140 * Screen.width/2048, 140 * Screen.height / 1536);
		
		//Botao de negar
		x = confirmHeaderPos.x + confirmHeaderPos.width/2 - confirmHeaderPos.width/8;
		denyButtonPos	= new Rect (x - (140 * Screen.width/2048)/2, infoToConfirmPos.y + infoToConfirmPos.height + 0.7f*spaceBetInfo,140 * Screen.width/2048, 140 * Screen.height / 1536);
		
		
		//Seta posicao do alvo no centro da tela
		aimPos = new Rect (Screen.width / 2 - (aim.width * Screen.width / 2048) / 2, Screen.height / 2 - (aim.height * Screen.height / 1536) / 2, aim.width * Screen.width / 2048, aim.height * Screen.height / 1536);
		
		
		/*Seta posicao da vida do primeiro boss  na tela*/
		//icone
		float xLifeBossIcon = Screen.width - (lifeBossIcon.width * Screen.width / 2048);
		lifeBossIconPos = new Rect (xLifeBossIcon, 0, lifeBossIcon.width * Screen.width/2048, lifeBossIcon.height * Screen.height/1536);
		lifeBossBarPos = new Rect (Screen.width/2 + 0.04f*lifeBossIconPos.width, lifeBossIconPos.y + lifeBossIconPos.height/2 - (lifeBossIconPos.height/5)/2, Screen.width / 2 - lifeBossIconPos.width, lifeBossIconPos.height / 5);
		
		
		/*Seta posicao da vida do cristal*/
		lifeCristalIconPos = new Rect (0, 0, lifeCristalIcon.width * Screen.width/2048, lifeCristalIcon.height * Screen.height/1536);
		float widthLifeCristal = Screen.width/2 - 2*lifeCristalIconPos.width;
		lifeCristalBarPos = new Rect (lifeCristalIconPos.x + 0.97f*lifeCristalIconPos.width, lifeCristalIconPos.y + lifeCristalIconPos.height/2 - (lifeCristalIconPos.height/5)/2, widthLifeCristal, lifeCristalIconPos.height / 5);
		
		//Fonte padrao
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
	}
	
	
	
	void Pause(){
		
		paused = true;
		confirmQuit = false; //Tira possivel info de confirm abandonar
		
	}
	
	void Resume(){
		
		paused = false;
		
	}
	
	void OnEnable(){
		
		GameController.onPause  += Pause;
		GameController.onResume += Resume;
	}
	
	void OnDisable(){
		
		GameController.onPause  -= Pause;
		GameController.onResume -= Resume;
	}
	
	
	void OnGUI(){
		
		if (Event.current.isMouse){
			lastMousePosition = Event.current.mousePosition;
		}
		
		//Caso perdeu target, entao, jogo fica parado e desenha menu de opcoes
		if (paused){
			GUI.backgroundColor = Color.clear; 
			
			drawMenuPaused();
		}
		else{
			
			/** Coloca a vida do cristal na tela ***/
			drawCrystalStatus();
			
			/** Caso boss(es) esta(ao) na tela, coloca a vida do(s) mesmo(s) na tela. **/
			if(isBossActived)
				drawBossStatus();
			
			GUI.backgroundColor = Color.clear; 
			
			/**Verificaremos booleanos para saber qual estado do menu in game se encontra **/
			drawSideMenus();
		}
		
	}
	
	/* Metodo usado para ver estado dos booleanos e chamar as devidas funcoes para
	 * desenhar estado atual dos menus laterais, tanto intem quanto exercito.*/
	void drawSideMenus(){
		
		/*Seta tamanho da fonte da quantitade de item */
		style.fontSize = (int)(Screen.height/23);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleCenter;
		
		//Verifica se estamos no estado para confirmar heroi
		if(confirmSelectHero == true){
			//Coloca mira na tela
			GUI.DrawTexture(aimPos, aim);
			
			//Pega confirmacao
			confirmSelectionHero();
		}
		
		//Verifica se estamos no estado de confirmar item
		else if(confirmSelectItem == true){
			if(itemSelected != Barracks.ItemClass.AttackBlackHole && itemSelected != Barracks.ItemClass.CureCristal)
				//Coloca mira na tela
				GUI.DrawTexture(aimPos, aim);
			
			//Pega confirmacao
			confirmSelectionItem();
		}			
		
		//Caso sem nenhum menu sendo usando, apenas coloca os botoes
		else if(leftMenuOn == false && rightMenuOn == false){ 
			
			/*** Botao do menu direito de item ***/
			if(GUI.RepeatButton (itemPos, "")  && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(itemMenu.icon.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
				//Vai para estado pressionado
				itemMenu.icon = (Texture) Util.LoadImageResource(STR.Resources.ITEM_MENU_PRESSED);
				
				itemMenu.clicked = true;
				
				timePassed = 0;
				
				GUI.DrawTexture(itemPos, itemMenu.icon);
				
			}
			else if(itemMenu.clicked && timePassed > threshold){
				//Faz operacao
				//Seleciona o menu de item, entao, fecha menu de heroi e liga o de item
				rightMenuOn = true;
				leftMenuOn  = false;
				
				//Avisa tutorial que clicou nesse botao
				if(runningTutorial) onMouseAction(lastMousePosition);  
				
				//Volta para estado despressionado
				itemMenu.icon = (Texture) Util.LoadImageResource(STR.Resources.ITEM_MENU);
				itemMenu.clicked = false;
				
				GUI.DrawTexture(itemPos, itemMenu.icon);
			}
			else
				GUI.DrawTexture(itemPos, itemMenu.icon);
			
			
			/*** Botao do menu esquerdo de heroi ***/
			if(GUI.RepeatButton (armyPos, "")  && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(armyMenu.icon.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
				
				//Vai para estado pressionado
				armyMenu.icon = (Texture) Util.LoadImageResource(STR.Resources.ARMY_MENU_PRESSED);
				
				armyMenu.clicked = true;
				
				timePassed = 0;
				
				GUI.DrawTexture(armyPos, armyMenu.icon);
			}
			else if(armyMenu.clicked && timePassed > threshold){
				
				//Faz operacao
				//Seleciona o menu de item, entao, fecha menu de heroi e liga o de item
				rightMenuOn = false;
				leftMenuOn  = true;
				
				//Avisa tutorial que clicou nesse botao
				if(runningTutorial) onMouseAction(lastMousePosition);  
				
				
				//Volta para estado despressionado
				armyMenu.icon = (Texture) Util.LoadImageResource(STR.Resources.ARMY_MENU);
				armyMenu.clicked = false;
				
				GUI.DrawTexture(armyPos, armyMenu.icon);
			}
			else
				GUI.DrawTexture(armyPos, armyMenu.icon);
			
		}
		
		//Usando menu da esquerda para colocar exercito
		else if(leftMenuOn == true && rightMenuOn == false) 
			drawArmyMenu();
		
		//Usando menu da direita para colocar itens
		else if(rightMenuOn == true && leftMenuOn == false)
			drawItemMenu();
		
		//Caso de erro, volta pro estado desativado dos menus
		else{
			leftMenuOn  = false;
			rightMenuOn = false;
		}
		
	}
	
	
	/* Metodo para carregar na tela os botoes de itens disponiveis para selecao.
	 * Ele carrega em menu circular. */
	void drawItemMenu (){
		
		/*** Botao do menu esquerdo de exercito ***/
		if(GUI.RepeatButton (armyPos, "") && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(armyMenu.icon.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
			//Vai para estado pressionado
			armyMenu.icon = (Texture) Util.LoadImageResource(STR.Resources.ARMY_MENU_PRESSED);
			
			armyMenu.clicked = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(armyPos, armyMenu.icon);
			
		}else if(armyMenu.clicked && timePassed > threshold){
			//Faz operacao
			//Seleciona o menu de heroi, entao, fecha menu de item e liga o de heroi
			rightMenuOn = false;
			leftMenuOn  = true;
			
			//Avisa tutorial que clicou nesse botao
			if(runningTutorial) onMouseAction(lastMousePosition);  
			
			
			//Volta para estado despressionado
			armyMenu.icon = (Texture) Util.LoadImageResource(STR.Resources.ARMY_MENU);
			armyMenu.clicked = false;
			
			GUI.DrawTexture(armyPos, armyMenu.icon);
			
		}else
			GUI.DrawTexture(armyPos, armyMenu.icon);
		
		
		/*** Botao de voltar desse menu ***/
		if(GUI.RepeatButton(itemPos, "") && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(backMenuItem.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
			//Vai para estado pressionado
			backMenuItem = (Texture) Util.LoadImageResource(STR.Resources.BACK_MENU_ITEM_PRESSED);
			
			backMenuClicked = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(itemPos, backMenuItem);
			
		}else if(backMenuClicked && timePassed > threshold){
			//Faz operacao
			//Seleciona o voltar do item, entao, fecha menu de item.
			rightMenuOn  = false;
			
			//Avisa tutorial que clicou nesse botao
			if(runningTutorial) onMouseAction(lastMousePosition);  
			
			
			//Volta para estado despressionado
			backMenuItem = (Texture) Util.LoadImageResource(STR.Resources.BACK_MENU_ITEM);
			backMenuClicked = false;
			
			GUI.DrawTexture(itemPos, backMenuItem);
		}else
			GUI.DrawTexture(itemPos, backMenuItem);
		
		
		/*** Coloca botoes circular dos itens disponiveis ***/
		
		//Divide os 180 graus entre os botoes existente, mantendo espacos igualmente
		buttonMenu[] existingItensButton = getItensClassesNotEmpty();
		float graus;
		if (existingItensButton.Length == 1)
			graus = 0;
		
		else 
			graus =( Mathf.PI) / (existingItensButton.Length -1);
		
		//Pega centro da circunferencia
		float centerX = itemPos.x + (itemPos.width/ 2);
		float centerY = itemPos.y + (itemPos.height/ 2);
		
		//Calcula o raio entre os centros da imagem central com seus botoes circulares
		//Note que devemos levar em conta o tamanho da imagem q eh o mesmo para o centro e botoes.
		float diag =  Mathf.Sqrt( Mathf.Pow(itemPos.width,2) + Mathf.Pow(itemPos.height, 2)); //Pitagoras
		
		float angular 	= (float)existingItensButton.Length / 6.0f;
		float aux 		= angular + 0.67f;
		float ray 		= ( aux * diag ); 
		
		//Comecar com o angulo em baixo (270 ou -90)
		float beginAngle = - Mathf.PI/2 ;
		
		
		int i = 0; //Contador
		foreach (buttonMenu buttonItem in existingItensButton){
			//Calcular cordenada circular
			float x = centerX - (ray * Mathf.Cos(beginAngle + (graus * i)));
			float y = centerY - (ray * Mathf.Sin(beginAngle + (graus * i)));
			
			//Rect do desenho do item
			Rect buttonItemIconPos = new Rect(x - (0.7f*buttonItem.icon.width * Screen.width/2048)/2, y - (0.7f*buttonItem.icon.height * Screen.height/1536)/2, 0.7f*buttonItem.icon.width * Screen.width/2048, 0.7f*buttonItem.icon.height * Screen.height/1536);
			
			//Rect do botao clicavel do item (fizamos tamnho 200x200 para ipada grande)
			Rect  buttonItemPos= new Rect(x - (200 * Screen.width/2048)/2, y - (200 * Screen.height/1536)/2, 200 * Screen.width/2048, 200 * Screen.height/1536);
			
			//note que o botao e a imagem sao rects diferente devido ao fato da imagem ter um over grande.
			
			//Coloca botao do item em questao
			if(GUI.RepeatButton(buttonItemPos, "") && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(buttonItem.icon.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
				//Carrega imagem de clicado ("b" no final se nao tem o mesmo)
				if(!buttonItem.icon.name.EndsWith("b"))
					buttonItem.icon = (Texture) Util.LoadImageResource(buttonItem.icon.name + "b");
				
				buttonItem.clicked = true; //ESPERO QUE SEJA PASSADO POR REF. ???
				
				timePassed = 0;
				
				GUI.DrawTexture(buttonItemIconPos, buttonItem.icon);
			}
			else if(buttonItem.clicked && timePassed > threshold){
				//Desliga flag de clicado
				buttonItem.clicked = false;
				
				//Pega objeto de item
				string itemName = buttonItem.icon.name.Replace( STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX), "");//Remove "Buttb" para pegar nome
				itemSelected = Barracks.getItemClass(itemName);
				
				
				//Volta menu
				rightMenuOn = false;
				leftMenuOn  = false;
				
				//Coloca estado de confirmar posicao de item
				confirmSelectHero = false;
				confirmSelectItem = true;
				
				//Avisa tutorial que clicou nesse botao
				if(runningTutorial) onMouseAction(lastMousePosition);  
				
				
				//Volta para estado despressionado
				string notPress = buttonItem.icon.name.Replace( STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX),  STR.get(STR.Resources.BUTTON_SUFFIX)); //Pega string sem o b
				buttonItem.icon = (Texture) Util.LoadImageResource(notPress); //Carrega texture
				
				GUI.DrawTexture(buttonItemIconPos, buttonItem.icon);
			}
			else
				GUI.DrawTexture(buttonItemIconPos, buttonItem.icon);
			
			/*Adiciona label com quantidade do dado item*/
			Rect iconeQuantPos = new Rect (buttonItemPos.x ,buttonItemPos.y + buttonItemPos.height - (quantityItemIcon.height*Screen.height/1536)/2, quantityItemIcon.width*Screen.width/2048, quantityItemIcon.height*Screen.height/1536);
			if(buttonItem.icon.name.Contains("Cristal")) //Tratar caso da imagem do cristal q possui muito over
				iconeQuantPos.y -=  buttonItemPos.height/4;
			
			GUI.DrawTexture(iconeQuantPos, quantityItemIcon);
			Rect valueQuantPos = new Rect (iconeQuantPos.x - iconeQuantPos.width/11, iconeQuantPos.y + iconeQuantPos.height/25, iconeQuantPos.width, iconeQuantPos.height);
			GUI.Label(valueQuantPos, buttonItem.count.ToString(),  style);
			
			i++;
		}
		
	}
	
	/* Metodo para carregar na tela botoes dos herois disponiveis para selecao.
	 * Ela carrega em menu circular.
	 * Apenas é chamado quando este clicou no menu esquerdo (exercito).
	 * */
	void drawArmyMenu (){
		/*** Botao do menu direito de item ***/
		if(GUI.RepeatButton (itemPos, "") && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(itemMenu.icon.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
			//Vai para estado pressionado
			itemMenu.icon = (Texture) Util.LoadImageResource(STR.Resources.ITEM_MENU_PRESSED);
			
			itemMenu.clicked = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(itemPos, itemMenu.icon);
			
		}
		else if(itemMenu.clicked && timePassed > threshold){
			//Faz operacao
			//Seleciona o menu de item, entao, fecha menu de heroi e liga o de item
			rightMenuOn = true;
			leftMenuOn  = false;
			
			//Avisa tutorial que clicou nesse botao
			if(runningTutorial) onMouseAction(lastMousePosition);  
			
			//Volta para estado despressionado
			itemMenu.icon = (Texture) Util.LoadImageResource(STR.Resources.ITEM_MENU);
			itemMenu.clicked = false;
			
			GUI.DrawTexture(itemPos, itemMenu.icon);
			
		}
		else
			GUI.DrawTexture(itemPos, itemMenu.icon);
		
		
		/*** Botao de voltar desse menu ***/
		if(GUI.RepeatButton(armyPos, "") && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(backMenuHero.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
			//Vai para estado pressionado
			backMenuHero = (Texture) Util.LoadImageResource(STR.Resources.BACK_MENU_HERO_PRESSED);
			
			backMenuClicked = true;
			
			timePassed = 0;
		}
		if(backMenuClicked && timePassed > threshold){
			//Faz operacao
			//Seleciona o voltar do army, entao, fecha menu de heroi.
			leftMenuOn  = false;
			
			//Avisa tutorial que clicou nesse botao
			if(runningTutorial) onMouseAction(lastMousePosition);  
			
			//Volta para estado despressionado
			backMenuHero = (Texture) Util.LoadImageResource(STR.Resources.BACK_MENU_HERO);
			backMenuClicked = false;
		}
		GUI.DrawTexture(armyPos, backMenuHero);
		
		
		/*** Coloca botoes circular dos herois disponiveis ***/
		
		//Divide os 180 graus entre os botoes existente, mantendo espacos igualmente
		buttonMenu[] existingHeroButton = getHeroClassesNotEmpty();
		float graus;
		if (existingHeroButton.Length == 1)
			graus = 0;
		
		else 
			graus =( Mathf.PI) / (existingHeroButton.Length -1);
		
		
		
		//Pega centro da circunferencia
		float centerX = armyPos.x + (armyPos.width/ 2);
		float centerY = armyPos.y + (armyPos.height/ 2);
		
		//Calcula o raio entre os centros da imagem central com seus botoes circulares
		//Note que devemos levar em conta o tamanho da imagem q eh o mesmo para o centro e botoes.
		float diag =  Mathf.Sqrt( Mathf.Pow(armyPos.width,2) + Mathf.Pow(armyPos.height, 2)); //Pitagoras
		
		
		
		int aux = (existingHeroButton.Length/6) + (1-1/3);
		float ray  = ( aux * diag ); 
		
		//Comecar com o angulo em baixo (270 ou -90)
		float beginAngle = - Mathf.PI/2 ;
		
		
		int i = 0; //Contador
		foreach (buttonMenu buttonHero in existingHeroButton){
			//Calcular cordenada circular
			float x = centerX + (ray * Mathf.Cos(beginAngle + (graus * i)));
			float y = centerY - (ray * Mathf.Sin(beginAngle + (graus * i)));
			
			Rect buttonHeroPos = new Rect(x - (buttonHero.icon.width * Screen.width/2048)/2, y - (buttonHero.icon.height * Screen.height/1536)/2, buttonHero.icon.width * Screen.width/2048, buttonHero.icon.height * Screen.height/1536);
			
			//Coloca botao do heroi em questao
			if(GUI.RepeatButton(buttonHeroPos, "") && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(buttonHero.icon.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
				//Carrega imagem de clicado ("b" no final se nao tem o mesmo)
				if(!buttonHero.icon.name.EndsWith("b"))
					buttonHero.icon = (Texture) Util.LoadImageResource(buttonHero.icon.name + "b");
				
				buttonHero.clicked = true; //ESPERO QUE SEJA PASSADO POR REF. ???
				
				timePassed = 0;
				
				GUI.DrawTexture(buttonHeroPos, buttonHero.icon);
			}
			else if(buttonHero.clicked && timePassed > threshold){
				//Desliga flag de clicado
				buttonHero.clicked = false;
				
				//Pega objeto de heroi
				string heroName = buttonHero.icon.name.Replace( STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX), "");//Remove "Buttb" para pegar nome
				heroSelected = Barracks.getHeroClass(heroName);
				
				//Volta menu
				rightMenuOn = false;
				leftMenuOn  = false;
				
				//Coloca estado de confirmar posicao de heroi
				confirmSelectHero = true;
				confirmSelectItem = false;
				
				//Avisa tutorial que clicou nesse botao
				if(runningTutorial) onMouseAction(lastMousePosition);  
				
				//Volta para estado despressionado
				string notPress = buttonHero.icon.name.Replace( STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX),  STR.get(STR.Resources.BUTTON_SUFFIX)); //Pega string sem o b
				buttonHero.icon = (Texture) Util.LoadImageResource(notPress); //Carrega texture
				
				GUI.DrawTexture(buttonHeroPos, buttonHero.icon);
			}
			else
				GUI.DrawTexture(buttonHeroPos, buttonHero.icon);
			
			/*Adiciona label com quantidade do dado heroi*/
			Rect iconeQuantPos = new Rect (buttonHeroPos.x + buttonHeroPos.width -  (quantityItemIcon.width*Screen.width/2048)/2,buttonHeroPos.y + buttonHeroPos.height - (quantityItemIcon.height*Screen.height/1536)/3, quantityItemIcon.width*Screen.width/2048, quantityItemIcon.height*Screen.height/1536);
			GUI.DrawTexture(iconeQuantPos, quantityItemIcon);
			Rect valueQuantPos = new Rect (iconeQuantPos.x - iconeQuantPos.width/11, iconeQuantPos.y + iconeQuantPos.height/25, iconeQuantPos.width, iconeQuantPos.height);
			GUI.Label(valueQuantPos, buttonHero.count.ToString(),  style);
			
			i++;
		}
		
	}
	
	//Metodo chamado quando selecionou um heroi e está aguardando
	//confirmacao e posicao para instanciar o mesmo.
	void confirmSelectionHero(){
		if(instSystem == null){
			GameObject instGameObj = Instantiate (instantiationSystemPrefab, Vector3.zero, Quaternion.identity) as GameObject;
			
			instSystem = instGameObj.GetComponent (typeof(InstantiationSystem)) as InstantiationSystem;
			instSystem.startInstantiationSystem (Barracks.getNameOfClass(heroSelected));
		}
		
		/*** Coloca botao de confirmar ***/
		if(GUI.RepeatButton (itemPos, "") && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(confirmButton.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
			//Vai para estado pressionado
			confirmButton = (Texture) Util.LoadImageResource(STR.Resources.CONFIRM_BUTTON_PRESSED);
			
			confirmButtonClicked = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(itemPos, confirmButton);
			
		}
		else if(confirmButtonClicked && timePassed > threshold){
			//Soltou o clique em uma area valida.
			if(instSystem != null && instSystem.instantiate() == true){
				//Desativa qualquer botao selecionado
				rightMenuOn = false;
				leftMenuOn  = false;
				
				//Desativa estado de confirmacao
				confirmSelectItem = false;
				confirmSelectHero = false;
				
				if(isThereDenyButton) //Caso nao ta usando teletransporte e de fato é um novo cara do exercito
					removeHero(heroSelected);//remove dado heroi do banco local
				
				//Remove sempre para estado de existir, apos colocar heroi
				isThereDenyButton = true;
				
				//Avisa tutorial que clicou nesse botao
				if(runningTutorial) onMouseAction(lastMousePosition);  
				
				//Destroi objeto de instanciacao
				Destroy(instSystem);
				instSystem = null;
				
			}
			//Volta para estado despressionado, caso nao entrou no if acima, continua no estado de confirmacao.
			confirmButton = (Texture) Util.LoadImageResource(STR.Resources.CONFIRM_BUTTON);
			confirmButtonClicked = false;
			
			GUI.DrawTexture(itemPos, confirmButton);
			
		}
		else
			GUI.DrawTexture(itemPos, confirmButton);
		
		if(isThereDenyButton){
			
			/*** Coloca botao de negar ***/
			if(GUI.RepeatButton (armyPos, "") && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(denyButton.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
				//Vai para estado pressionado
				denyButton = (Texture) Util.LoadImageResource(STR.Resources.DENY_BUTTON_PRESSED);
				
				denyButtonClicked = true;
				
				timePassed = 0;
				
				GUI.DrawTexture(armyPos, denyButton);
				
			}
			else if(denyButtonClicked && timePassed > threshold){
				
				//Volta para estado despressionado, negando colocacao do heroi
				denyButton = (Texture) Util.LoadImageResource(STR.Resources.DENY_BUTTON);
				denyButtonClicked = false;
				
				/*Volta estado inicial*/
				//Desativa qualquer botao selecionado
				rightMenuOn = false;
				leftMenuOn  = false;
				
				//Desativa estado de confirmacao
				confirmSelectItem = false;
				confirmSelectHero = false;
				
				//Avisa tutorial que clicou nesse botao
				if(runningTutorial) onMouseAction(lastMousePosition);  
				
				//Destroi objeto de instanciacao
				Destroy(instSystem);
				instSystem = null;
				
				GUI.DrawTexture(armyPos, denyButton);
				
			}
			else
				GUI.DrawTexture(armyPos, denyButton);
		}
	}
	
	
	
	//Metodo chamado quando seleciou um item e esta aguardando 
	//confirmacao e posicao para instanciar o mesmo.
	void confirmSelectionItem(){
		//Caso primeira vez da confirmacao, cria instation system.
		if(instSystem == null){
			GameObject instGameObj = Instantiate (instantiationSystemPrefab, Vector3.zero, Quaternion.identity) as GameObject;
			
			instSystem = instGameObj.GetComponent (typeof(InstantiationSystem)) as InstantiationSystem;
			instSystem.startInstantiationSystem (Barracks.getNameOfClassItem(itemSelected));
		}
		
		/*** Coloca botao de confirmar ***/
		if(GUI.RepeatButton (itemPos, "") && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(confirmButton.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
			//Vai para estado pressionado
			confirmButton = (Texture) Util.LoadImageResource(STR.Resources.CONFIRM_BUTTON_PRESSED);
			
			confirmButtonClicked = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(itemPos, confirmButton);
			
		}
		else if(confirmButtonClicked && timePassed > threshold){
			//Soltou o clique em uma area valida.
			if(instSystem != null && instSystem.instantiate() == true){
				
				//Desativa qualquer botao selecionado
				rightMenuOn = false;
				leftMenuOn  = false;
				
				//Desativa estado de confirmacao
				confirmSelectItem = false;
				confirmSelectHero = false;
				
				//remove dado item do banco local
				removeItem(itemSelected);
				
				//Avisa tutorial que clicou nesse botao
				if(runningTutorial) onMouseAction(lastMousePosition);  
				
				//Verifica se é o caso especial do teletransporte para avisar o confirmation que nao deve possuir botao de negar
				if(itemSelected == Barracks.ItemClass.Teletransport){
					
					/*Vai para estado de colocar um heroi, sendo q nesse caso instation system ja sabe qual é*/
					//Volta menu
					rightMenuOn = false;
					leftMenuOn  = false;
					
					confirmSelectHero = true;
					confirmSelectItem = false;
					
					isThereDenyButton = false;
				}
				else{ //Caso geral
					//Destroi objeto de instanciacao
					Destroy(instSystem.gameObject);
					instSystem = null;
					
					isThereDenyButton = true;
				}
				
			}
			//Volta para estado despressionado, caso nao entrou no if acima, continua no estado de confirmacao.
			confirmButton = (Texture) Util.LoadImageResource(STR.Resources.CONFIRM_BUTTON);
			confirmButtonClicked = false;
			
			GUI.DrawTexture(itemPos, confirmButton);
			
		}
		else
			GUI.DrawTexture(itemPos, confirmButton);
		
		/*** Coloca botao de negar ***/
		if(GUI.RepeatButton (armyPos, "") && ((pointExclusivelyTo == "") || (pointExclusivelyTo.Contains(denyButton.name)))){ //Apenas executa botao se nao tiver no tutorial(nao aponta pra nenhuma string) OU aponta para string desse botao(nome da textura sem pressionar)
			//Vai para estado pressionado
			denyButton = (Texture) Util.LoadImageResource(STR.Resources.DENY_BUTTON_PRESSED);
			
			denyButtonClicked = true;
			
			timePassed = 0;
			
			GUI.DrawTexture(armyPos, denyButton);
			
		}
		else if(denyButtonClicked && timePassed > threshold){
			
			//Volta para estado despressionado, negando colocacao do heroi
			denyButton = (Texture) Util.LoadImageResource(STR.Resources.DENY_BUTTON);
			denyButtonClicked = false;
			
			/*Volta estado inicial*/
			//Desativa qualquer botao selecionado
			rightMenuOn = false;
			leftMenuOn  = false;
			
			//Desativa estado de confirmacao
			confirmSelectItem = false;
			confirmSelectHero = false;
			
			//Avisa tutorial que clicou nesse botao
			if(runningTutorial) onMouseAction(lastMousePosition);  
			
			//Destroi objeto de instanciacao
			Destroy(instSystem);
			instSystem = null;
			
			GUI.DrawTexture(armyPos, denyButton);
			
		}
		else
			GUI.DrawTexture(armyPos, denyButton);
		
		
	}
	
	
	/**  Metodo para desenhar o status do boss(es) na tela, apenas é chamada
	 *   quando o boss(es) entrarao em cena. */
	void drawBossStatus(){
		//Caso primeira vez que apareceu os chefoes, inicializa variavel que guarda objeto deles
		if(bosses == null){
			//Pegar todos os boss da fase
			GameObject[] gameBosses = GameObject.FindGameObjectsWithTag("Boss"); 
			
			//Inicia o array dos objetos dos chefoes
			bosses = new Boss[gameBosses.Length];
			
			//Coloca os objetos dos gameobject de chefao
			for(int bossCounter = 0; bossCounter < gameBosses.Length; bossCounter++)
				bosses[bossCounter] =  gameBosses[bossCounter].GetComponent<Boss>();
		}
		
		//Coloca imagens do bosses
		float spaceBetBoss = lifeBossIcon.height/5; 
		Rect  currentIcon = new Rect(lifeBossIconPos.x, lifeBossIconPos.y, lifeBossIconPos.width, lifeBossIconPos.height);
		Rect  currentBar  = new Rect(lifeBossBarPos.x, lifeBossBarPos.y, lifeBossBarPos.width, lifeBossBarPos.height);
		foreach(Boss boss in bosses){

			
			//Calcula porcentagem da vida atual
			float porce = (float)boss.Life / (float)boss.initialLife;
			if(porce > 0.0){//Apenas coloca barra da vida se for maior q zero.
				Rect barLifeActual = new Rect(currentBar.x + (currentBar.width * (1-porce)) + currentBar.width/100, currentBar.y + currentBar.height/10, currentBar.width * porce - currentBar.width/100, currentBar.height - 0.13f*currentBar.height);
				
				//Coloca cor que varia de verde ate vermelho
				Color life = new Color (1 - porce, porce, 0);
				drawRect (barLifeActual, life);
			}
			
			//Coloca borda da barra
			GUI.DrawTexture(currentBar, lifeBossBar);

			//Coloca icone
			GUI.DrawTexture(currentIcon, lifeBossIcon);

			currentBar.y += spaceBetBoss;
			currentIcon.y += spaceBetBoss;
		}
	}
	
	
	/**  Metodo para desenhar o status do CRISTAL na tela */
	void drawCrystalStatus(){
		
		//Calcula porcentagem da vida atual
		float porce = (float)myCrystal.Life / (float)myCrystal.initialLife;

		if(porce > 0.0f){ //Apenas preenche com cor a barra se tem maior q zero.
			//Coloca barra de vida equivalente ao estado atual
			Rect barLifeActual = new Rect(lifeCristalBarPos.x + lifeCristalBarPos.width/100, lifeCristalBarPos.y + lifeCristalBarPos.height/10, lifeCristalBarPos.width * porce - lifeCristalBarPos.width/50, lifeCristalBarPos.height - 0.13f*lifeBossBarPos.height);

			Color life = new Color (1 - porce, porce, 0);
			
			
			drawRect (barLifeActual, life);
		}
		
		//Desenha borda da barra da vida
		GUI.DrawTexture(lifeCristalBarPos, lifeCristalBar);

		GUI.DrawTexture(lifeCristalIconPos, lifeCristalIcon);
	}
	
	void drawMenuPaused(){
		/*Seta tamanho da fonte do texto de parado */
		style.fontSize = (int)(Screen.height/10);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleLeft;
		
		//Texto para avisar parado
		string text = STR.get ("PAUSED") + " - ";
		
		text = text + labelPaused; 
		
		//Coloca a infromacao na tela
		GUI.Label (labelPausedPos, text, style);
		GUI.Button (labelPausedPos, "");
		
		if(confirmQuit)
			drawConfirmQuit();
		else{
			//Coloca botao de voltar menu principal
			if(GUI.Button(backMainButtonPos, "")){
				//sounds
				SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);
				
				//Entra em estado de confirmacao da acao
				confirmQuit = true;
			}
			GUI.DrawTexture(backMainButtonPos, backMainButton);
			
			//Coloca botao para desligar som
			drawSoundButton ();
			
			//Coloca botao para desligar efeito
			//drawEffectBUtton (); (COMENTADO, MIGUE DO GUSTAVO PRO SOM)
			
		}
	}
	
	void drawSoundButton(){
		if(GUI.Button(soundButtonPos, "")){
			if(SoundController.isMusicOff()){//Caso esteja desligado som, entao liga o mesmo.
				SoundController.musicIsOff(false);
				soundButton = soundOnButton;
			}
			else{ //Contrario, estava ligado, entao desliga.
				SoundController.musicIsOff(true);
				soundButton = soundOffButton;
			}
		}
		
		//Desenha botao com estado atual do som
		GUI.DrawTexture (soundButtonPos, soundButton);
	}
	
	void drawEffectBUtton(){
		if(GUI.Button(effectButtonPos, "")){
			if(SoundController.isEffectOff()){//Caso esteja desligado som, entao liga o mesmo.
				SoundController.effectIsOff(false);
				effectButton = effectOnButton;
			}
			else{ //Contrario, estava ligado, entao desliga.
				SoundController.effectIsOff(true);
				effectButton = effectOffButton;
			}
		}
		
		//Desenha botao com estado atual do som
		GUI.DrawTexture (effectButtonPos, effectButton);
	}
	
	
	/*Desenhar mensagem de confirmacao para abandonar a partida em questao
	 * Importante para nao haver abandono indesejado, visto que apenas salvamos
	 * os dados apos acabar a batalha*/
	
	void drawConfirmQuit(){
		//Coloca apenas imagem dos botoes, sem terem feedback ao clicar.
		GUI.DrawTexture (backMainButtonPos, backMainButton);
		GUI.DrawTexture (soundButtonPos, soundButton);
		//GUI.DrawTexture (effectButtonPos, effectButton);
		
		//Coloca pergaminho na frente que devera ter a info e opcao de confirmar
		GUI.DrawTexture (confirmPergPos, confirmPerg);
		
		//Fonte usada
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		
		//Header
		style.fontSize = (int)(0.4f*Screen.height/6);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.red;
		style.alignment = TextAnchor.UpperCenter;
		
		GUI.Label (confirmHeaderPos, confirHeader, style);
		
		//Informacao para saber se deseja fazer delecao
		style.fontStyle = FontStyle.Normal;
		style.fontSize = (int)(Screen.height / 22);
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleCenter;
		style.wordWrap = true;
		
		GUI.TextField (infoToConfirmPos, infoToConfirm, style);
		
		//Coloca botao de confirmar acao
		drawConfirmButtonQuit ();
		
		//Coloca botao de negar acao
		drawDenyButtonQuit ();
		
	}
	
	void drawConfirmButtonQuit(){
		//Coloca botao de confirmar abandono sem efeito
		if(GUI.Button(confirmButtonPos, "")){
			//sounds
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/crystalExplosionEffect")as AudioClip);
			
			//Faz acao para abandonar a batalha
			GameController.State = GameController.GameStateEnum.Menu;//Volta estado do jogo para menu (tirar timescale do zero)
			Application.LoadLevel("MainMenu");
			
		}
		
		GUI.DrawTexture(confirmButtonPos, confirmButton);
		
		
	}
	
	void drawDenyButtonQuit(){
		//Coloca botao de negar abandono sem efeito
		if(GUI.Button(denyButtonPos, "")){
			//sounds
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);
			
			//Faz operacao para remover estado de confirmacao
			confirmQuit = false;
		}
		
		GUI.DrawTexture(denyButtonPos, denyButton);
		
		
	}
	
	
	/******************************************************************
	 *** Metodos para cuidar da quantidade de itens e herois na fase **
     ******************************************************************/
	
	//Pegar numero de um item (precisamos passar qual grupo pertence)
	int currentNumberOfItensInGroup(Barracks.ItemClass item){
		return itemMenu.subButton [(int)item].count;
	}
	
	
	int currentNumberOfHeroes(Barracks.HeroClass hero){
		return armyMenu.subButton[(int)hero].count;
	}
	
	//Metodo que devolve um array com os botoes de heroi que há algum disponivel.
	buttonMenu[] getHeroClassesNotEmpty(){
		List<buttonMenu> classesNotEmpty = new List<buttonMenu> ();
		
		for(int i =0; i < armyMenu.subButton.Length; i++){
			if(armyMenu.subButton[i].count > 0)
				classesNotEmpty.Add(armyMenu.subButton[i]);
		}
		
		return classesNotEmpty.ToArray();
	}
	
	//Metodo que devolve um array com os botoes de itens que há algum disponivel.
	buttonMenu[] getItensClassesNotEmpty(){
		List<buttonMenu> classesNotEmpty = new List<buttonMenu> ();
		
		for(int i =0; i < itemMenu.subButton.Length; i++){
			if(itemMenu.subButton[i].count > 0)
				classesNotEmpty.Add(itemMenu.subButton[i]);
		}
		
		return classesNotEmpty.ToArray();
	}
	
	void removeItem(Barracks.ItemClass item){
		//remove um elemento do dado item
		itemMenu.subButton [(int)item].count--;
	}
	
	void removeHero(Barracks.HeroClass hero){
		//remove um elemento do dado heroi
		armyMenu.subButton [(int)hero].count--;
		
	}
	
	private void itemsRectangles(Dictionary<string, Rect> ans){
		buttonMenu[] existingItensButton = getItensClassesNotEmpty();
		float graus;
		if (existingItensButton.Length == 1)
			graus = 0;
		
		else 
			graus =( Mathf.PI) / (existingItensButton.Length -1);
		
		//Pega centro da circunferencia
		float centerX = itemPos.x + (itemPos.width/ 2);
		float centerY = itemPos.y + (itemPos.height/ 2);
		
		//Calcula o raio entre os centros da imagem central com seus botoes circulares
		//Note que devemos levar em conta o tamanho da imagem q eh o mesmo para o centro e botoes.
		float diag =  Mathf.Sqrt( Mathf.Pow(itemPos.width,2) + Mathf.Pow(itemPos.height, 2)); //Pitagoras
		
		float angular 	= (float)existingItensButton.Length / 6.0f;
		float aux 		= angular + 0.67f;
		float ray 		= ( aux * diag ); 
		
		//Comecar com o angulo em baixo (270 ou -90)
		float beginAngle = - Mathf.PI/2 ;
		
		int i = 0; //Contador
		foreach (buttonMenu buttonItem in existingItensButton) {
			//Calcular cordenada circular
			float x = centerX - (ray * Mathf.Cos (beginAngle + (graus * i)));
			float y = centerY - (ray * Mathf.Sin (beginAngle + (graus * i)));
			
			
			//Rect do botao clicavel do item (fizamos tamnho 200x200 para ipada grande)
			Rect  buttonItemPos= new Rect(x - (200 * Screen.width/2048)/2, y - (200 * Screen.height/1536)/2, 200 * Screen.width/2048, 200 * Screen.height/1536);
			
			//note que o tamanho do botao é fixo com 200x200
			
			
			string name = buttonItem.icon.name;
			name = name.Remove(  name.Length - 4 );
			ans[ name ] = buttonItemPos;
			++i;
			
		}
	}
	
	private void heroesRectangles(Dictionary<string, Rect> ans){
		buttonMenu[] existingHeroButton = getHeroClassesNotEmpty();
		float graus;
		if (existingHeroButton.Length == 1)
			graus = 0;
		
		else 
			graus =( Mathf.PI) / (existingHeroButton.Length -1);
		
		//Pega centro da circunferencia
		float centerX = armyPos.x + (armyPos.width/ 2);
		float centerY = armyPos.y + (armyPos.height/ 2);
		
		//Calcula o raio entre os centros da imagem central com seus botoes circulares
		//Note que devemos levar em conta o tamanho da imagem q eh o mesmo para o centro e botoes.
		float diag =  Mathf.Sqrt( Mathf.Pow(armyPos.width,2) + Mathf.Pow(armyPos.height, 2)); //Pitagoras
		
		int aux = (existingHeroButton.Length/6) + (1-1/3);
		float ray  = ( aux * diag ); 
		
		//Comecar com o angulo em baixo (270 ou -90)
		float beginAngle = - Mathf.PI/2 ;
		
		int i = 0; //Contador
		foreach (buttonMenu buttonHero in existingHeroButton) {
			//Calcular cordenada circular
			float x = centerX + (ray * Mathf.Cos (beginAngle + (graus * i)));
			float y = centerY - (ray * Mathf.Sin (beginAngle + (graus * i)));
			
			Rect buttonHeroPos = new Rect (x - (buttonHero.icon.width * Screen.width / 2048) / 2, y - (buttonHero.icon.height * Screen.height / 1536) / 2, buttonHero.icon.width * Screen.width / 2048, buttonHero.icon.height * Screen.height / 1536);
			
			string name = buttonHero.icon.name;
			name = name.Remove(  name.Length - 4 );
			ans[ name ] = buttonHeroPos;
			
			++i;
		}
	}
	
	public Dictionary<string, Rect> getRetangles(){
		Dictionary<string, Rect> ans = new Dictionary<string, Rect>();
		ans["armyPos"] = new Rect(0, Screen.height/2 - (armyMenu.icon.height * Screen.height/1536)/2, armyMenu.icon.width * Screen.width/2048, armyMenu.icon.height * Screen.height/1536);
		ans["itemPos"] = new Rect(Screen.width - (armyMenu.icon.width * Screen.width/2048), Screen.height/2 - (armyMenu.icon.height * Screen.height/1536)/2, armyMenu.icon.width * Screen.width/2048, armyMenu.icon.height * Screen.height/1536);
		heroesRectangles (ans);
		itemsRectangles (ans);
		
		
		return ans;
	}
	
	//AUXILIAR PARA DESENHAR O BOX DA VIDA
	Texture2D texture;
	void drawRect(Rect position, Color color){
		//Corrigir bug de quando nao tem width e desenha pra tras.
		if(position.width < 1.0f)
			return;

		if (texture != null) {
			
			Texture2D.DestroyImmediate (texture, true);
		}
		
		texture = new Texture2D(1, 1);
		texture.SetPixel(0,0,color);
		texture.Apply();
		GUI.skin.box.normal.background = texture;
		GUI.Box(position, GUIContent.none);
	}
	
	
	
}