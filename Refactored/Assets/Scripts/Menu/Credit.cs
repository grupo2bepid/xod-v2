using UnityEngine;
using System.Collections;

public class Credit : MonoBehaviour {
	
	/*background image*/
	Texture creditBackgroudTexture;
	Rect creditBackgroudTexturePos;
	
	/*back button*/
	Texture backButtonTexture;
	Rect backButtonPos; 
	
	/*text style */
	GUIStyle style = new GUIStyle();
	
	void Awake(){
		//Inicializa informacoes uteis para o jogo 
		HashIDs.fillHashIDs (); 
		STR.initiate ();
		AppropriateDistancesForFight.fillDictionary ();
		
		//Backgroud init
		creditBackgroudTexture = (Texture)Util.LoadImageResource (STR.get("creditBackgroundTexture"));
		creditBackgroudTexturePos = new Rect (0, 0, Screen.width, Screen.height);
		
		//backbutton init
		backButtonTexture = (Texture) Util.LoadImageResource(STR.Resources.EXIT_BUTTON);
		backButtonPos = new Rect (Screen.width / 35, 0.2f * Screen.height / 50, backButtonTexture.width * Screen.width / 2048, backButtonTexture.height * Screen.height / 1536);
		
	}
	void OnGUI(){
		//clear button backgroud 
		GUI.backgroundColor = Color.clear; 
		
		//draw backgroud
		GUI.DrawTexture (creditBackgroudTexturePos, creditBackgroudTexture);
		
		//draw backButton
		GUI.DrawTexture(backButtonPos, backButtonTexture);
		
		//backbutton
		if (GUI.RepeatButton (backButtonPos, ""))
			Application.LoadLevel("BeginMenu");
		
		
//		//TEXT
//		//Developers
//		creatHeader (STR.get("developers"),);//COMPLETAR
//		creatDescription (STR.get("developersDis1"),);//COMPLETAR
//		creatDescription (STR.get("developersDis2"),);//COMPLETAR
//		creatDescription (STR.get("developersDis3"),);//COMPLETAR
//		creatDescription (STR.get("developersDis4"),);//COMPLETAR
//		//Sounds
//		creatHeader (STR.get("developers"),);//COMPLETAR
//		creatDescription (STR.get("developersDis1"),);//COMPLETAR
//		creatDescription (STR.get("developersDis2"),);//COMPLETAR
//		//Target Texture
//		creatHeader (STR.get("target"),);//COMPLETAR
//		creatDescription (STR.get("targetDis1"),);//COMPLETAR
//		//Art
//		creatHeader (STR.get("art"),);//COMPLETAR
//		creatDescription (STR.get(""),);//COMPLETAR
		
		
		
	}
	void creatHeader(string headerText, Rect headerPos){
		style.fontSize = (int)(Screen.height/18);
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleCenter;
		style.fontStyle = FontStyle.Bold;
		
		GUI.Label (headerPos, headerText, style);
		
	}
	void creatDescription(string text, Rect textPos){
		style.fontStyle = FontStyle.Normal;
		style.fontSize = (int)(Screen.height / 22);
		style.wordWrap = true;
		
		
		GUI.TextField (textPos, text, style);
		
	}
}