﻿using UnityEngine;
using System.Collections;

public class ShowQuantityHero : MonoBehaviour {
	
	GameObject quantityHeroText; //Text da quantidade
	int quantityHero = 0; //Numeros de herois q possui
	public Barracks.HeroClass typeHero; //Tipo de heroi que este objeto referencia
	ObjectLabel follower; //Script pego da net para um texto seguir um gameobject
	//MenuBarracks menu; //Pegar instancia do menuBarracks
	Vector3 anterior; 

	//Funcao de size (funcao linear)
	int sizeText; //y da funcoa
	float a; 
	float b;


	void Awake(){
		//Cria text com follower
		quantityHeroText = Instantiate(Resources.Load("Prefabs/HerosMenu/FollowTextMenu"), gameObject.transform.position, Quaternion.identity) as GameObject;

		//Seta follower para esse gameobject
		follower = quantityHeroText.GetComponent<ObjectLabel>();

		//Pega objeto do menu na tela do quartel
		//menu = FindObjectOfType (typeof(MenuBarracks)) as MenuBarracks;

		//Seta para texto seguir gameobject em questao
		follower.target = gameObject.transform;

		//Seta o campo do texto
		quantityHero = Barracks.getHeroCount (typeHero);
		quantityHeroText.guiText.text = quantityHero.ToString();

		anterior = quantityHeroText.transform.position;


		//Calcula valores das constantes da funcao
		a =  -0.175F;
		b = - 430 * a;


	}


	void Update(){

		//Atualiza font apenas quando alterou posicao do heroi
		if(!quantityHeroText.transform.position.Equals(anterior)){
			//Seta tamanho da fonte inicial
			float x = quantityHeroText.transform.position.sqrMagnitude;	

			//Funcao linear do tamanho da fonte
			sizeText = (int)(a * x + b);

			if(sizeText <= 10 && typeHero != Barracks.HeroClass.WarhammerWarrior) //Caso menor q um limiar(10) desaparece text
				quantityHeroText.guiText.enabled = false;

			else
				quantityHeroText.guiText.enabled = true; 

			if(Barracks.HeroClass.WarhammerWarrior == typeHero && sizeText <= 10) //Corrige erro da funcao na mao.
				sizeText = 15;


			quantityHeroText.guiText.fontSize = sizeText; //Seta font do texto

		}


		anterior = quantityHeroText.transform.position;

		//Seta o campo do texto
		quantityHero = Barracks.getHeroCount (typeHero);
		quantityHeroText.guiText.text = quantityHero.ToString();
	}
}
