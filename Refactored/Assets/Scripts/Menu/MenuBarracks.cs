﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuBarracks : MonoBehaviour {

	/* Imagens usados nessa tela */
	Texture bar 		;
	Texture lifeIcon 	;
	Texture rangeIcon 	;
	Texture agilityIcon ;
	Texture attackIcon 	; 
	Texture sideBar		;
	Texture sideBar_fst ;
	Texture sideBar_lst ;
	Texture priceIcon 	;
	Texture coin 		; //MUITO PEQUENO, TEM QUE AUMENTAR!!!
	Texture backButton 	;
	Texture roll		;
	Texture[] golpes	= new Texture[3]; //FALTA BOTOES
	Texture buyButton; //FALTA ICONE
	Texture upgradeButton; //FALTA BOTAO FINAL => USADO PARA DESENHAR
	Texture[] upgradeButtonControl = new Texture[4]; //Usado para controle

	//Auxiliar para construir nome da imagem do botao do golpe
	new string name = STR.get(STR.Resources.EMPTY);

	//Casos limites das propriedades
	float piorDelay; //O limite é o pior pq qnto maior pior pro delay
	float melhorRange;
	float melhorLife = 100;

	//sounds
	public GameObject musicBox;
	public AudioSource effects, music;


	public Barracks.HeroClass classIndex;
	
	private Texture2D texture;
	
	private Dictionary<Barracks.HeroClass, Animator> animatorReferences;
	private Dictionary<Barracks.HeroClass, float[]> strikesAnimationLength;
	private bool animationIsPlaying = false;

	//Salva status no dicionario, caso termine aplicacao nessa tela
	void OnApplicationQuit(){
		Database.writeDictionaryOnDisk ();
	}
	
	void OnApplicationPause (){
		Database.writeDictionaryOnDisk ();
	}


	void fillAnimatorReferences(){
		animatorReferences = new Dictionary<Barracks.HeroClass, Animator>();
		strikesAnimationLength = new Dictionary<Barracks.HeroClass, float[]> ();
		GameObject[] heroesOnScreen = FlowView.Instance.Views;
		foreach( GameObject hero in heroesOnScreen ){
			HeroMenu heroMenuScript = hero.GetComponent(typeof(HeroMenu)) as HeroMenu;
			strikesAnimationLength[ heroMenuScript.heroClass ] = heroMenuScript.attackAnimationDelays;
			animatorReferences[heroMenuScript.heroClass] = heroMenuScript.GetComponent(typeof(Animator)) as Animator;
		}
	}


	int getStrikeHashID(int i){
		if (i == 0)
			return HashIDs.attack1Trigger;
		else if (i == 1)
			return HashIDs.attack2Trigger;
		else
			return HashIDs.attack3Trigger;
	}

	//Booleanos para fazer efeito de click
	bool backButtonClicked		= false; 
	bool buyButtonClicked		= false;
	bool buyButtonClicked_life	= false;
	bool buyButtonClicked_range	= false;
	bool buyButtonClicked_attk	= false;
	bool buyButtonClicked_agil	= false;

	//Variavel que guarda o tempo passado
	float timePassed = -1;

	//Limitante de tempo para efeito de botao pressionar
	const float threshold = 0.1F;

	GUIStyle style = new GUIStyle();

	void Update (){
		timePassed += Time.deltaTime;
		
		if(backButtonClicked && timePassed > threshold)
			Application.LoadLevel("MainMenu");
		
		//sounds
		/*verifica se a musica nao foi mutada*/
		SoundController.muteSounds (music,effects);
		
		
	}
	void inicializaTextureGolpe(){
		//IMAGEM QUEBRA GALHO
		golpes[0] = (Texture)Util.LoadImageResource (STR.Resources.COUP0);
		golpes[1] = (Texture)Util.LoadImageResource (STR.Resources.COUP1);
		golpes[2] = (Texture)Util.LoadImageResource (STR.Resources.COUP2);

		animationIsPlaying = false;
	}

	void Start(){

		fillAnimatorReferences ();

		//Pega pior delay (PEGA DO VALOR INICIAL)
		piorDelay = InitialValues.FIGHTER_ATTACK_DELAY;
		if( InitialValues.AXEWARRIOR_ATTACK_DELAY > piorDelay)
			piorDelay = InitialValues.AXEWARRIOR_ATTACK_DELAY;
		if(InitialValues.MACEWARRIOR_ATTACK_DELAY > piorDelay)
			piorDelay = InitialValues.MACEWARRIOR_ATTACK_DELAY;
		if(InitialValues.WARHAMMERWARRIOR_ATTACK_DELAY > piorDelay)
			piorDelay = InitialValues.WARHAMMERWARRIOR_ATTACK_DELAY;

		piorDelay *= 1.5f; //Seta pior para ter 33% de delay (0.5/1.5), no arrendondamento, 35% = 7quad

		//Pega melhor range (PEGA DO VALOR INICIAL)
		melhorRange = InitialValues.FIGHTER_RANGE;
		if( InitialValues.AXEWARRIOR_RANGE > melhorRange)
			melhorRange = InitialValues.AXEWARRIOR_RANGE;
		if(InitialValues.MACEWARRIOR_RANGE > melhorRange)
			melhorRange = InitialValues.MACEWARRIOR_RANGE;
		if(InitialValues.WARHAMMERWARRIOR_RANGE > melhorRange)
			melhorRange = InitialValues.WARHAMMERWARRIOR_RANGE;

		melhorRange *= 2; //Seta melhor range ser 50% a mais q o maior, ou seja, o melhor comeca com 50% cheio

		//Inicializa botoes de golpe
		inicializaTextureGolpe ();

		//Inicializa imagens de icones com respectivos caminhos
		bar 		= (Texture)Util.LoadImageResource (STR.Resources.EMPTY_BAR);
		lifeIcon 	= (Texture)Util.LoadImageResource (STR.Resources.HP);
		rangeIcon 	= (Texture)Util.LoadImageResource (STR.Resources.RANGE_ICON);
		agilityIcon = (Texture)Util.LoadImageResource (STR.Resources.AGILITY);
		attackIcon 	= (Texture)Util.LoadImageResource (STR.Resources.ATK); 
		sideBar		= (Texture)Util.LoadImageResource (STR.Resources.SIDEBAR);
		sideBar_fst = (Texture)Util.LoadImageResource (STR.Resources.SIDEBAR_FST);
		sideBar_lst = (Texture)Util.LoadImageResource (STR.Resources.SIDEBAR_LST);
		priceIcon 	= (Texture)Util.LoadImageResource (STR.Resources.COIN_OVER);
		coin 		= (Texture)Util.LoadImageResource (STR.Resources.PRICE_OVER); //MUITO PEQUENO, TEM QUE AUMENTAR!!!
		roll		= (Texture)Util.LoadImageResource (STR.Resources.ROLL);


		//Inicializa botoes
		backButton 		= (Texture) Util.LoadImageResource(STR.Resources.EXIT_BUTTON );
		upgradeButton	= (Texture) Util.LoadImageResource(STR.Resources.UP);
		buyButton		= (Texture) Util.LoadImageResource(STR.TranslatedResources.RECRUIT_BUTTON);
		upgradeButtonControl[0]	= (Texture) Util.LoadImageResource(STR.Resources.UP);
		upgradeButtonControl[1]	= (Texture) Util.LoadImageResource(STR.Resources.UP);
		upgradeButtonControl[2]	= (Texture) Util.LoadImageResource(STR.Resources.UP);
		upgradeButtonControl[3]	= (Texture) Util.LoadImageResource(STR.Resources.UP);

		//sounds
		/*cria um objeto filho para receber o audiosource de musicas*/
		musicBox = new GameObject ("MusicBox");
		musicBox.transform.position = gameObject.transform.position;
		/*inicializacao da parte sonora*/
		effects = gameObject.AddComponent<AudioSource> ();
		music = musicBox.AddComponent<AudioSource>();
		/*efeito inicial e loop da musica*/
		SoundController.playSoundEffect (effects, Resources.Load ("Sounds/baracksEffect2")as AudioClip);
		SoundController.playSoundMusic (music, Resources.Load ("Sounds/baracksMusic")as AudioClip);

		
	}

	void OnGUI() {  
		GUI.backgroundColor = Color.clear;

		
		//Class Index
		FlowView fView = FlowView.Instance;
		
		classIndex = (Barracks.HeroClass) fView.GetClosestIndex ();

		//Fonte usada
		style.fontSize = (int)(0.4f*Screen.height/5);
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
		style.alignment = TextAnchor.UpperLeft;


		Rect coinPos = new Rect((Screen.width - Screen.width/35 - (coin.width * Screen.width/2048) - 12* Screen.width /150)*0.9f,Screen.height/ 66, coin.width * Screen.width/2048, coin.height * Screen.height/1536);
		Rect valueCoinPos = new Rect((Screen.width  - Screen.width/35 - 12* Screen.width /150)*0.93f,  0f , 12* Screen.width /150, coin.height);

		GUI.DrawTexture(coinPos, coin, ScaleMode.StretchToFill, true,  0.5f* Screen.width / 6);
		GUI.Label(valueCoinPos, ""+Database.getPlayerMoney(), style);
		
		//Desenhar botao para retornar
		Rect backButtonPos = new Rect (Screen.width / 35, 0.2f * Screen.height / 50, backButton.width * Screen.width / 2048, backButton.height * Screen.height / 1536);
		if(GUI.RepeatButton (backButtonPos,"")){
			backButton = (Texture) Util.LoadImageResource(STR.Resources.EXIT_BUTTON_PRESSED);
			
			backButtonClicked = true;
			
			timePassed = 0;
			
		}

		GUI.DrawTexture(backButtonPos, backButton);

		drawInfo ();

	}


	void drawInfo(){
		//Desenha pergaminho
		Rect pergaminhoPos = new Rect (0, Screen.height - (roll.height * Screen.height/1536), roll.width * Screen.width/2048, roll.height * Screen.height/1536);
		GUI.DrawTexture(pergaminhoPos, roll);

		//Fonte usada
		style.fontSize = (int)(0.4f*Screen.height/5);
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.LowerCenter;

		//Colcoca espaco entre as propriedades
		Rect espace = new Rect (pergaminhoPos.x, pergaminhoPos.y , pergaminhoPos.width, pergaminhoPos.height/19);
		//GUI.Button (espace, "");


		//Coloca o nome
		Rect header = new Rect (pergaminhoPos.x, (espace.y + espace.height)*1.04f, pergaminhoPos.width, (pergaminhoPos.height - pergaminhoPos.height/10)/6);
		string headerText = STR.get (Barracks.getNameOfClass (classIndex));
		GUI.Label (header, headerText, style);
		//GUI.Button (header, "");

		/* PROPRIEDADE DE VIDA */
		Rect life = new Rect (header.x, header.y*0.96f+header.height, pergaminhoPos.width, lifeIcon.height * Screen.height/1536);
		//GUI.Button (life, "");

		//Coloca icone
		float sizeProp = (bar.width * Screen.width/2048) + 3*(lifeIcon.width * Screen.width/2048)/2 + (upgradeButton.width* Screen.width/2048) + 5*(priceIcon.width * Screen.width/2048/1.3f)/2; 
		Rect lifeIconPos = new Rect (Screen.width - sizeProp - Screen.width/20, life.y,  lifeIcon.width * Screen.width/2048, lifeIcon.height * Screen.height/1536);
		GUI.DrawTexture(lifeIconPos, lifeIcon);
		//GUI.Button (new Rect(lifeIconPos.x, life.y, sizeProp, life.height), "");


		//Coloca barra
		Rect barPos = new Rect (lifeIconPos.x + 3*lifeIconPos.width/2, life.y + (lifeIcon.height * Screen.height/1536)/2 - (bar.height * Screen.height/1536)/2, bar.width * Screen.width/2048, bar.height * Screen.height/1536);
		GUI.DrawTexture(barPos, bar);
		//GUI.Button (barPos, "");

		//Preencher a barra

		//Porcentagem da atual range
		int valueLife =  (int)  (Barracks.getValueOfAttribute (Barracks.Attributes.Life, classIndex)  * 100/ melhorLife); //porcentagem de qualidade
		
		//Calcula o numero de quadradinho equivalente
		int restoDiv = valueLife %5;
		//Arrendonda para baixo
		if(restoDiv == 0 || restoDiv == 1 || restoDiv == 2)
			valueLife = (int)((valueLife - restoDiv )/5); //numero de quadrados para pintar
		
		//Arrendonda para cima
		else
			valueLife =  (int)((valueLife + (5-restoDiv ))/5); //numero de quadrados para pintar 

		Rect barSidePos = new Rect(barPos.x + 0.0175f*barPos.width, barPos.y + (0.175f*barPos.height), sideBar_fst.width * Screen.width/2048, sideBar_fst.height * Screen.height/1536);
		GUI.DrawTexture (barSidePos, sideBar_fst);

		barSidePos.x += barSidePos.width + barPos.width * 0.0028f;
		GUI.DrawTexture (barSidePos, sideBar);

		int j;
		for(j=3; j <= (valueLife-1);j++){
			barSidePos.x += barSidePos.width + barPos.width * 0.0040f;
			GUI.DrawTexture (barSidePos, sideBar);
		}
		
		if(valueLife == 20) {//Caso cheio
			barSidePos.x += barSidePos.width + barPos.width * 0.0040f;
			barSidePos.y -= Screen.height * 0.001f;
			GUI.DrawTexture (barSidePos, sideBar_lst);
		}
		else if(j <= valueLife){ //Caso geral apenas coloca o ultimo
			barSidePos.x += barSidePos.width + barPos.width * 0.0040f;
			GUI.DrawTexture (barSidePos, sideBar);
		}

		//Coloca icone de price
		Rect pricePos = new Rect (barPos.x + barPos.width + (priceIcon.width * Screen.width/2048/1.3f)/4, life.y + life.height/2 - (priceIcon.height * Screen.height/1536/1.3f)/2, priceIcon.width * Screen.width/2048/1.3f, priceIcon.height * Screen.height/1536/1.3f);
		GUI.DrawTexture(pricePos, priceIcon);
		//GUI.Button (pricePos, "");

		//Coloca preco do up
		style.fontSize = (int)(0.4f*Screen.height/5);
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.UpperLeft;
		
		int priceUp = (int)Barracks.getValueToUpgradeAttribute (classIndex, Barracks.Attributes.Life);
		priceUp = priceUp + (int)(2 * valueLife); //Faz calculo do preco: BASE + x * NUMER_PROP.

		Rect priceValuePos = new Rect (pricePos.x + pricePos.width, pricePos.y*0.97f, pricePos.width , pricePos.height);
		GUI.Label(priceValuePos, priceUp.ToString(), style);
		//GUI.Button (priceValuePos, "");

		//Coloca botao de upgrade
		Rect upButtonPos = new Rect (priceValuePos.x + priceValuePos.width, life.y + life.height/2 - (upgradeButton.height* Screen.height/1536)/2, upgradeButton.width* Screen.width/2048 ,  upgradeButton.height* Screen.height/1536);
		if(GUI.RepeatButton (upButtonPos, "")){
			//Vai para estado pressionado
			upgradeButtonControl[0] = (Texture) Util.LoadImageResource("upb");

			//Compra upgrade aumentando UM da propriedade
			if(!buyButtonClicked_life){
				if(Database.getPlayerMoney() >= priceUp && valueLife < 20){ //Caso tenha dinheiro suficiente E nao esta no limite dessa propriedade

					//sounds
					SoundController.playSoundEffect (effects, Resources.Load ("Sounds/cashEffect")as AudioClip);

					//Atualiza dinheiro no banco removendo o preco
					Database.updatePlayerMoney((float) Database.getPlayerMoney() - priceUp);
					
					//Atualiza valor da propriedade na base
					//DEVEMOS "AUMENTAR"(melhorar) EM 5% do valor atual
					float newValueUp = Barracks.getValueOfAttribute (Barracks.Attributes.Life, classIndex) +  (0.05F * melhorLife);
					Barracks.updateValueOfAttribute(Barracks.Attributes.Life, classIndex, newValueUp);
				}
					else if(valueLife < 20){ //Caso nao tem dinheiro
						//sounds
						SoundController.playSoundEffect (effects, Resources.Load ("Sounds/noCashEffect")as AudioClip);

						errorNoMoney();//Falta implementar
					}
					else{ //Caso esta no maximo da propriedade
						errorFullAttribute(); //Falta implementar
					}
			}

			buyButtonClicked_life = true;
			
			timePassed = 0;
		}
		else if(buyButtonClicked_life && timePassed > threshold){
			//Volta para estado despressionado
			upgradeButtonControl[0] = (Texture) Util.LoadImageResource(STR.Resources.UP);
			buyButtonClicked_life = false;
		}

		GUI.DrawTexture(upButtonPos, upgradeButtonControl[0]);




		/* PROPRIEDADE DE RANGE */
		Rect range = new Rect (header.x, life.y+life.height + espace.height, pergaminhoPos.width, rangeIcon.height * Screen.height/1536);
		//GUI.Button (life, "");
		
		//Coloca icone
		sizeProp = (bar.width * Screen.width/2048) + 3*(rangeIcon.width * Screen.width/2048)/2 + (upgradeButton.width* Screen.width/2048) + 5*(priceIcon.width * Screen.width/2048/1.3f)/2; 
		Rect rangeIconPos = new Rect (Screen.width - sizeProp - Screen.width/20, range.y,  rangeIcon.width * Screen.width/2048, rangeIcon.height * Screen.height/1536);
		GUI.DrawTexture(rangeIconPos, rangeIcon);
		//GUI.Button (new Rect(lifeIconPos.x, life.y, sizeProp, life.height), "");
		
		
		//Coloca barra
		barPos = new Rect (rangeIconPos.x + 3*rangeIconPos.width/2, range.y + (rangeIcon.height * Screen.height/1536)/2 - (bar.height * Screen.height/1536)/2, bar.width * Screen.width/2048, bar.height * Screen.height/1536);
		GUI.DrawTexture(barPos, bar);
		//GUI.Button (barPos, "");
		
		//Preencher a barra

		//Porcentagem da atual range
		int valueRange =  (int) (Barracks.getValueOfAttribute (Barracks.Attributes.Range, classIndex)  * 100/ melhorRange); //porcentagem de qualidade
		
		//Calcula o numero de quadradinho equivalente
		restoDiv = valueRange %5;
		//Arrendonda para baixo
		if(restoDiv == 0 || restoDiv == 1 || restoDiv == 2)
			valueRange = (int)((valueRange - restoDiv )/5); //numero de quadrados para pintar
		
		//Arrendonda para cima
		else
			valueRange =  (int)((valueRange + (5-restoDiv ))/5); //numero de quadrados para pintar 
		


		barSidePos = new Rect(barPos.x + 0.0175f*barPos.width, barPos.y + (0.175f*barPos.height), sideBar_fst.width * Screen.width/2048, sideBar_fst.height * Screen.height/1536);
		GUI.DrawTexture (barSidePos, sideBar_fst);
		
		barSidePos.x += barSidePos.width + barPos.width * 0.0028f;
		GUI.DrawTexture (barSidePos, sideBar);

		for(j=3; j <= (valueRange-1);j++){
			barSidePos.x += barSidePos.width + barPos.width * 0.0040f;
			GUI.DrawTexture (barSidePos, sideBar);
		}
		
		if(valueRange == 20) {//Caso cheio
			barSidePos.x += barSidePos.width + barPos.width * 0.0040f;
			barSidePos.y -= Screen.height * 0.001f;
			GUI.DrawTexture (barSidePos, sideBar_lst);
		}
		else if(j <= valueRange){ //Caso geral apenas coloca o ultimo
			barSidePos.x += barSidePos.width + barPos.width * 0.0040f;
			GUI.DrawTexture (barSidePos, sideBar);
		}
		
		//Coloca icone de price
		pricePos = new Rect (barPos.x + barPos.width + (priceIcon.width * Screen.width/2048/1.3f)/4, range.y + range.height/2 - (priceIcon.height * Screen.height/1536/1.3f)/2, priceIcon.width * Screen.width/2048/1.3f, priceIcon.height * Screen.height/1536/1.3f);
		GUI.DrawTexture(pricePos, priceIcon);
		//GUI.Button (pricePos, "");
		
		//Coloca preco do up
		style.fontSize = (int)(0.4f*Screen.height/5);
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.UpperLeft;
		
		priceUp = (int)Barracks.getValueToUpgradeAttribute (classIndex, Barracks.Attributes.Range);
		priceUp = priceUp + (int)(2 * valueRange); //Faz calculo do preco: BASE + x * NUMER_PROP.
		
		priceValuePos = new Rect (pricePos.x + pricePos.width, pricePos.y*0.97f, pricePos.width , pricePos.height);
		GUI.Label(priceValuePos, priceUp.ToString(), style);
		//GUI.Button (priceValuePos, "");
		
		//Coloca botao de upgrade
		upButtonPos = new Rect (priceValuePos.x + priceValuePos.width, range.y + range.height/2 - (upgradeButton.height* Screen.height/1536)/2, upgradeButton.width* Screen.width/2048 ,  upgradeButton.height* Screen.height/1536);
		
		if(GUI.RepeatButton (upButtonPos, "")){
			//Vai para estado pressionado
			upgradeButtonControl[1] = (Texture) Util.LoadImageResource(STR.Resources.UP_PRESSED);
			
			//Compra upgrade aumentando UM da propriedade
			if(!buyButtonClicked_range){
				if(Database.getPlayerMoney() >= priceUp && valueRange < 20){ //Caso tenha dinheiro suficiente E nao esta no limite dessa propriedade
					//Atualiza dinheiro no banco removendo o preco
					Database.updatePlayerMoney((float) Database.getPlayerMoney() - priceUp);

					//sounds
					SoundController.playSoundEffect (effects, Resources.Load ("Sounds/cashEffect")as AudioClip);
					
					//Atualiza valor da propriedade na base
					//DEVEMOS "AUMENTAR"(melhorar) EM 5% do valor atual
					float newValueUp = Barracks.getValueOfAttribute (Barracks.Attributes.Range, classIndex) + (0.05F * melhorRange);
					Barracks.updateValueOfAttribute(Barracks.Attributes.Range, classIndex, newValueUp);
				}
				else if(valueRange < 20){ //Caso nao tem dinheiro
					errorNoMoney();//Falta implementar
		
				}
				else{ //Caso esta no maximo da propriedade
					errorFullAttribute(); //Falta implementar
				}
			}
			
			buyButtonClicked_range = true;
			
			timePassed = 0;
		}
		else if(buyButtonClicked_range && timePassed > threshold){
			//Volta para estado despressionado
			upgradeButtonControl[1] = (Texture) Util.LoadImageResource(STR.Resources.UP);
			buyButtonClicked_range = false;
		}
		
		GUI.DrawTexture(upButtonPos, upgradeButtonControl[1]);
		



		/* PROPRIEDADE DE AGILIDADE */
		Rect agility = new Rect (range.x, range.y+range.height + espace.height, sizeProp, agilityIcon.height * Screen.height/1536);

		//Coloca icone
		sizeProp = (bar.width * Screen.width/2048) + 3*(agilityIcon.width * Screen.width/2048)/2 + (upgradeButton.width* Screen.width/2048) + 5*(priceIcon.width * Screen.width/2048/1.3f)/2; 
		Rect agilityIconPos = new Rect (Screen.width - sizeProp - Screen.width/20, agility.y,  agilityIcon.width * Screen.width/2048, agilityIcon.height * Screen.height/1536);
		GUI.DrawTexture(agilityIconPos, agilityIcon);
		//GUI.Button (new Rect(agilityIconPos.x, agility.y, sizeProp, range.height), "");
		
		
		//Coloca barra
		barPos = new Rect (agilityIconPos.x + 3*agilityIconPos.width/2, agility.y + (agilityIcon.height * Screen.height/1536)/2 - (bar.height * Screen.height/1536)/2, bar.width * Screen.width/2048, bar.height * Screen.height/1536);
		GUI.DrawTexture(barPos, bar);
		//GUI.Button (barPos, "");


		//Preencher a barra

		//Porcentagem da atual agilidade
		int valueAgility =  (int) ( (piorDelay - Barracks.getValueOfAttribute (Barracks.Attributes.AttackDelay, classIndex) ) * 100/ piorDelay); //porcentagem de qualidade

		//Calcula o numero de quadradinho equivalente
		restoDiv = valueAgility %5;
		//Arrendonda para baixo
		if(restoDiv == 0 || restoDiv == 1 || restoDiv == 2)
			valueAgility = (int)((valueAgility - restoDiv )/5); //numero de quadrados para pintar

		//Arrendonda para cima
		else
			valueAgility =  (int)((valueAgility + (5-restoDiv ))/5); //numero de quadrados para pintar 

		barSidePos = new Rect(barPos.x + 0.0175f*barPos.width, barPos.y + (0.175f*barPos.height), sideBar_fst.width * Screen.width/2048, sideBar_fst.height * Screen.height/1536);
		GUI.DrawTexture (barSidePos, sideBar_fst);
		
		barSidePos.x += barSidePos.width + barPos.width * 0.0028f;
		GUI.DrawTexture (barSidePos, sideBar);
		
		for(j=3; j <= (valueAgility-1);j++){
			barSidePos.x += barSidePos.width + barPos.width * 0.0040f;
			GUI.DrawTexture (barSidePos, sideBar);
		}
		
		if(valueAgility == 20) {//Caso cheio
			barSidePos.x += barSidePos.width + barPos.width * 0.0040f;
			barSidePos.y -= Screen.height * 0.001f;
			GUI.DrawTexture (barSidePos, sideBar_lst);
		}
		else if(j <= valueAgility){ //Caso geral apenas coloca o ultimo
			barSidePos.x += barSidePos.width + barPos.width * 0.0040f;
			GUI.DrawTexture (barSidePos, sideBar);
		}
		
		//Coloca icone de price
		pricePos = new Rect (barPos.x + barPos.width + (priceIcon.width * Screen.width/2048/1.3f)/4, agility.y + agility.height/2 - (priceIcon.height * Screen.height/1536/1.3f)/2, priceIcon.width * Screen.width/2048/1.3f, priceIcon.height * Screen.height/1536/1.3f);
		GUI.DrawTexture(pricePos, priceIcon);
		//GUI.Button (pricePos, "");
		
		//Coloca preco do up
		style.fontSize = (int)(0.4f*Screen.height/5);
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.UpperLeft;
		
		priceUp = (int)Barracks.getValueToUpgradeAttribute (classIndex, Barracks.Attributes.AttackDelay);
		priceUp = priceUp + (int)(2 * valueAgility); //Faz calculo do preco: BASE + x * NUMER_PROP.
		
		priceValuePos = new Rect (pricePos.x + pricePos.width, pricePos.y*0.97f, pricePos.width , pricePos.height);
		GUI.Label(priceValuePos, priceUp.ToString(), style);
		//GUI.Button (priceValuePos, "");
		
		//Coloca botao de upgrade
		upButtonPos = new Rect (priceValuePos.x + priceValuePos.width, agility.y + agility.height/2 - (upgradeButton.height* Screen.height/1536)/2, upgradeButton.width* Screen.width/2048 ,  upgradeButton.height* Screen.height/1536);


		if(GUI.RepeatButton (upButtonPos, "")){
			//Vai para estado pressionado
			upgradeButtonControl[2] = (Texture) Util.LoadImageResource(STR.Resources.UP_PRESSED);
			
			//Compra upgrade aumentando UM da propriedade
			if(!buyButtonClicked_agil){
				if(Database.getPlayerMoney() >= priceUp && valueAgility < 20){ //Caso tenha dinheiro suficiente E nao esta no limite dessa propriedade
					//Atualiza dinheiro no banco removendo o preco
					Database.updatePlayerMoney((float) Database.getPlayerMoney() - priceUp);

					//sounds
					SoundController.playSoundEffect (effects, Resources.Load ("Sounds/cashEffect")as AudioClip);
					
					//Atualiza valor da propriedade na base
					//DEVEMOS "AUMENTAR"(melhorar) EM 5% do valor atual
					float newValueUp = Barracks.getValueOfAttribute (Barracks.Attributes.AttackDelay, classIndex)- (0.05F * piorDelay);
					Barracks.updateValueOfAttribute(Barracks.Attributes.AttackDelay, classIndex, newValueUp);
				}
				else if(valueAgility < 20){ //Caso nao tem dinheiro
					errorNoMoney();//Falta implementar

				}
				else{ //Caso esta no maximo da propriedade
					errorFullAttribute(); //Falta implementar
				}
			}
			
			buyButtonClicked_agil = true;
			
			timePassed = 0;
		}
		else if(buyButtonClicked_agil && timePassed > threshold){
			//Volta para estado despressionado
			upgradeButtonControl[2] = (Texture) Util.LoadImageResource(STR.Resources.UP);
			buyButtonClicked_agil = false;
		}
		
		GUI.DrawTexture(upButtonPos, upgradeButtonControl[2]);



		/* PROPRIEDADE DE ATAQUE */
		Rect attack = new Rect (agility.x, agility.y + agility.height + espace.height, sizeProp, attackIcon.height * Screen.height/1536);
		
		//Coloca icone
		sizeProp = (bar.width * Screen.width/2048) + 3*(attackIcon.width * Screen.width/2048)/2 + (upgradeButton.width* Screen.width/2048) + 5*(priceIcon.width * Screen.width/2048/1.3f)/2; 
		Rect attackIconPos = new Rect (Screen.width - sizeProp - Screen.width/20, attack.y,  attackIcon.width * Screen.width/2048, attackIcon.height * Screen.height/1536);
		GUI.DrawTexture(attackIconPos, attackIcon);
		//GUI.Button (new Rect(attackIconPos.x, attack.y, sizeProp, attack.height), "");
		
		
		//Coloca golpes
		barPos = new Rect (attackIconPos.x + 3*attackIconPos.width/2, attack.y + (attackIcon.height * Screen.height/1536)/2 - (bar.height * Screen.height/1536)/2, bar.width * Screen.width/2048, bar.height * Screen.height/1536);
		//GUI.Button (barPos, "");

		int golpeAvailable = (int)Barracks.getValueOfAttribute (Barracks.Attributes.MaxAttack, classIndex);

		float spaceEntreGolpe = (barPos.width - 3 * (golpes[0].width * Screen.width/2048))/2;

		for (int i=0; i<golpeAvailable; i++) {
			Rect posGolpeButton = new Rect(barPos.x + i * (golpes[0].height * Screen.height/1536) + i * spaceEntreGolpe, attack.y + (attackIcon.height * Screen.height/1536)/2 - (golpes[0].height * Screen.height/1536)/2, golpes[i].width * Screen.width/2048, golpes[i].height * Screen.height/1536);
			if(GUI.Button(posGolpeButton, "") && !animationIsPlaying){ //Caso clicou em um botao de golpe
				//Volta botoes para apagado
				inicializaTextureGolpe();

				//Cola imagem de estado de botao de golpe ligado
				if(name.Contains("b") && name.Contains(""+i.ToString())){ //Caso clique em botao ja ligado, entao, deliga botao
					name = "golpe" + i.ToString();

					//Volta para idle
				}
				else{ //Caso escolheu botao nao ligado, liga o mesmo.
					name = "golpe" + i.ToString() + "b";

					animatorReferences[classIndex].SetTrigger( getStrikeHashID(i) );
					animationIsPlaying = true;
					Invoke("inicializaTextureGolpe", strikesAnimationLength[classIndex][i+1]);

				}

				golpes[i] = (Texture)Util.LoadImageResource (name);
			}
			GUI.DrawTexture(posGolpeButton, golpes[i]);
		}

		//Coloca icone de price
		pricePos = new Rect (barPos.x + barPos.width + (priceIcon.width * Screen.width/2048/1.3f)/4, attack.y + attack.height/2 - (priceIcon.height * Screen.height/1536/1.3f)/2, priceIcon.width * Screen.width/2048/1.3f, priceIcon.height * Screen.height/1536/1.3f);
		GUI.DrawTexture(pricePos, priceIcon);
		//GUI.Button (pricePos, "");
		
		//Coloca preco do up
		style.fontSize = (int)(0.4f*Screen.height/5);
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.UpperLeft;
		
		priceUp = (int)Barracks.getValueToUpgradeAttribute (classIndex, Barracks.Attributes.MaxAttack);
		priceUp = priceUp + (int)(15 * golpeAvailable); //Faz calculo do preco: BASE + x * NUMER_PROP.
		
		priceValuePos = new Rect (pricePos.x + pricePos.width, pricePos.y*0.97f, pricePos.width , pricePos.height);
		GUI.Label(priceValuePos, priceUp.ToString(), style);
		//GUI.Button (priceValuePos, "");
		
		//Coloca botao de upgrade
		upButtonPos = new Rect (priceValuePos.x + priceValuePos.width, attack.y + attack.height/2 - (upgradeButton.height* Screen.height/1536)/2, upgradeButton.width* Screen.width/2048 ,  upgradeButton.height* Screen.height/1536);
		
		
		if(GUI.RepeatButton (upButtonPos, "")){
			//Vai para estado pressionado
			upgradeButtonControl[3] = (Texture) Util.LoadImageResource(STR.Resources.UP_PRESSED);
			
			//Compra upgrade aumentando UM da propriedade
			if(!buyButtonClicked_attk){
				if(Database.getPlayerMoney() >= priceUp &&  golpeAvailable < 3){ //Caso tenha dinheiro suficiente E nao esta no limite dessa propriedade
					//Atualiza dinheiro no banco removendo o preco
					Database.updatePlayerMoney((float) Database.getPlayerMoney() - priceUp);

					//sounds
					SoundController.playSoundEffect (effects, Resources.Load ("Sounds/cashEffect")as AudioClip);
					
					//Atualiza valor da propriedade na base
					//Devemos aumentar o proximo golpe
					float newValueUp = Barracks.getValueOfAttribute (Barracks.Attributes.MaxAttack, classIndex) + 1;
					Barracks.updateValueOfAttribute(Barracks.Attributes.MaxAttack, classIndex, newValueUp);

					//MOSTRA ANIMACAO DO GOLPE "newValueUp"
				}
				else if(golpeAvailable < 3){ //Caso nao tem dinheiro
					errorNoMoney();//Falta implementar

				}
				else{ //Caso esta no maximo da propriedade
					errorFullAttribute(); //Falta implementar
				}
			}
			
			buyButtonClicked_attk = true;
			
			timePassed = 0;
		}
		else if(buyButtonClicked_attk && timePassed > threshold){
			//Volta para estado despressionado
			upgradeButtonControl[3] = (Texture) Util.LoadImageResource(STR.Resources.UP);
			buyButtonClicked_attk = false;
		}
		
		GUI.DrawTexture(upButtonPos, upgradeButtonControl[3]);


		//Pega quantidade do heroi em questao
		int quantityHero = Barracks.getHeroCount (classIndex);

		/*** Coloca icone de preco ***/
		Rect priceRecrutarIcon = new Rect ( (lifeIconPos.x/2 - (priceIcon.width * Screen.width /2048))*0.8f, pergaminhoPos.y + pergaminhoPos.height/2 - ((buyButton.height * Screen.height/1536) + (priceIcon.height * Screen.height /1536))/2, priceIcon.width * Screen.width /2048, priceIcon.height * Screen.height /1536);
		GUI.DrawTexture (priceRecrutarIcon, priceIcon);
		//GUI.Button (priceRecrutarIcon, "");

		/*** Coloca String com valor ***/
		style.fontSize = (int)(0.4f*Screen.height/3);
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleLeft;
		
		priceRecrutarIcon.x += priceRecrutarIcon.width;
		int priceHero = Barracks.getValueToBuyHero (classIndex);
		GUI.Label (priceRecrutarIcon, priceHero+"", style);

		/*** Coloca Botao pra recrutar um heroi ***/
		Rect buyButtonPos = new Rect(lifeIconPos.x/2 - (buyButton.width * Screen.width/2048)/2, (priceRecrutarIcon.y + priceRecrutarIcon.height)*1.02f, buyButton.width * Screen.width/2048, buyButton.height * Screen.height/1536);
		if(GUI.RepeatButton (buyButtonPos, "")){
			//Vai para estado pressionado
			buyButton = (Texture) Util.LoadImageResource(STR.TranslatedResources.RECRIUT_BUTTON_PRESSED);

			//Caso é uma compra
			if(!buyButtonClicked){
				if(Database.getPlayerMoney() >= priceHero){

					//sounds
					SoundController.playSoundEffect (effects, Resources.Load ("Sounds/baracksEffect2")as AudioClip);

					//Atualiza dinheiro no banco removendo o preco
					Database.updatePlayerMoney((float) Database.getPlayerMoney() - priceHero);

					//Aumenta numero de herois dessa classe
					Barracks.updateHeroCount(classIndex, quantityHero+1);
				}
				else {

					errorNoMoney();

				}
			}
			
			buyButtonClicked = true;
			
			timePassed = 0;
		}
		else if(buyButtonClicked && timePassed > threshold){
			//Volta para estado despressionado
			buyButton = (Texture) Util.LoadImageResource(STR.TranslatedResources.RECRUIT_BUTTON);
			buyButtonClicked = false;
		}
		
		GUI.DrawTexture (buyButtonPos, buyButton);

	}

	//Metodo chamado quando usuario compra algo e nao possui dinheiro suficiente.
	//Ele deve implementar um tratador para da ro feedback ao usuario
	void errorNoMoney(){
		//sounds
		SoundController.playSoundEffect (effects, Resources.Load ("Sounds/noCashEffect")as AudioClip);

	}

	//Metodo chamado quando usuario compra um upgrade de um atributo, mas esse ja esta no maximo.
	//Ele deve implementar um tratador para da ro feedback ao usuario
	void errorFullAttribute(){
		SoundController.playSoundEffect (effects, Resources.Load ("Sounds/noCashEffect")as AudioClip);
	}
	
}
