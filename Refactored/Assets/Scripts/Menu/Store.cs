﻿using UnityEngine;
using System.Collections;

public class Store : MonoBehaviour {

	private const float topHeight = 10;
	private const float middleHeight = 67;
	private const float bottomHeight = 23;
	private const float middleColumn = 35;
	private Texture2D texture;

	//sounds
	public GameObject musicBox;
	public AudioSource effects, music;
	
	public GUISkin skin = null;

	//Limitante de tempo para efeito de botao pressionar
	const float threshold = 0.08F;

	//Booleanos para fazer efeito de click
	bool backButtonClicked	= false; 
	bool buyButtonClicked	= false;

	//Imagem dos botoes
	Texture backButton;
	Texture buyButton;

	//Imagens usadas na tela
	Texture coin;
	Texture quantityItemIcon;


	//Array que guarda a imagem de cada item de acordo com o estado de ligado/desligado.
	Texture[] itemImage = new Texture[8];

	void ImageItemInitial(){

		itemImage [0] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.Teletransport) + STR.get(STR.Resources.BUTTON_SUFFIX));
		itemImage [1] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.AttackThunder) + STR.get(STR.Resources.BUTTON_SUFFIX));
		itemImage [2] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.AttackBlackHole) + STR.get(STR.Resources.BUTTON_SUFFIX));
		itemImage [3] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.UpgradeDefense) + STR.get(STR.Resources.BUTTON_SUFFIX));
		itemImage [4] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.UpgradeForce) + STR.get(STR.Resources.BUTTON_SUFFIX));
		itemImage [5] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.UpgradeAgility) + STR.get(STR.Resources.BUTTON_SUFFIX));
		itemImage [6] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.CureCharacter) + STR.get(STR.Resources.BUTTON_SUFFIX));
		itemImage [7] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.CureCristal) + STR.get(STR.Resources.BUTTON_SUFFIX));
		
	}

	//Salva status no dicionario, caso termine aplicacao nessa tela
	void OnApplicationQuit(){
		Database.writeDictionaryOnDisk ();
	}
	
	void OnApplicationPause (){
		Database.writeDictionaryOnDisk ();
	}

	void Start(){
		//Inicializa vetor com as imagens sem nenhuma clicada
		ImageItemInitial ();

		//Inicializa imagens de botoes
		backButton = (Texture) Util.LoadImageResource(STR.Resources.EXIT_BUTTON);
		buyButton = (Texture)Util.LoadImageResource (STR.TranslatedResources.BUY_BUTTON);

		//Inicializa imagens icones usadas na tela
		coin 				= (Texture)Util.LoadImageResource (STR.Resources.PRICE_OVER); //MUITO PEQUENO, TEM QUE AUMENTAR!!!
		quantityItemIcon	= (Texture) Util.LoadImageResource(STR.Resources.QUANTITY_BACKGROUND);


		//sounds
		/*cria um objeto filho para receber o audiosource de musicas*/
		musicBox = new GameObject ("MusicBox");
		musicBox.transform.position = gameObject.transform.position;
	
		/*inicializacao da parte sonora*/
		effects = gameObject.AddComponent<AudioSource> ();
		music = musicBox.AddComponent<AudioSource>();

		/*efeito inicial e loop da musica*/
		SoundController.playSoundEffect (effects, Resources.Load ("Sounds/storeEffect")as AudioClip);
		SoundController.playSoundMusic (music, Resources.Load ("Sounds/storeMusic")as AudioClip);
		effects.volume = 0.3F;


		
	}

	GUIStyle style = new GUIStyle();

	Barracks.ItemClass selected = Barracks.ItemClass.None;

	float timePassed = -1;

	void Update (){

		/*verifica se a musica nao foi mutada*/
		SoundController.muteSounds (music, effects);

		timePassed += Time.deltaTime;

		if(backButtonClicked && timePassed > threshold)
			Application.LoadLevel("MainMenu");

		//sounds
		/*verifica se a musica nao foi mutada*/
		SoundController.muteSounds (music, effects);

	}

	void OnGUI() {  
		GUI.backgroundColor = Color.clear;
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);

		drawBackgrounds ();


		showInfoItem ();
		
		GUI.skin = skin;
		
		//Fonte usada
		style.fontSize = (int)(0.4f*Screen.height/5);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
		style.alignment = TextAnchor.UpperLeft;
		
		
		Rect coinPos = new Rect(Screen.width - Screen.width/35 - (coin.width * Screen.width/2048) - 14* Screen.width /150, Screen.height/ 45, coin.width * Screen.width/2048, coin.height * Screen.height/1536);
		Rect valueCoinPos = new Rect(Screen.width  - Screen.width/35 - 14* Screen.width /150, 0f , 12* Screen.width /150, coin.height);
		
		GUI.DrawTexture(coinPos, coin, ScaleMode.StretchToFill, true,  0.5f* Screen.width / 6);
		GUI.Label(valueCoinPos, ""+Database.getPlayerMoney(), style);
		
		
		
		//Desenhar botao para retornar
		Rect backButtonPos = new Rect (Screen.width / 35, 0.2f * Screen.height / 50, backButton.width * Screen.width / 2048, backButton.height * Screen.height / 1536);
		if(GUI.RepeatButton (backButtonPos,"")){
			backButton = (Texture) Util.LoadImageResource(STR.Resources.EXIT_BUTTON_PRESSED );
			
			backButtonClicked = true;
			
			timePassed = 0;
			
		}
		
		GUI.DrawTexture(backButtonPos, backButton);


	}
	
	
	void drawBackgrounds(){
		
		GUIStyle style = new GUIStyle();
		
		
		
		Rect position = new Rect (0, 0, Screen.width, Screen.height);
		GUI.DrawTexture (position, (Texture)Util.LoadImageResource (STR.Resources.STORE_BACKGROUND));


		//Pergaminho que aparecera informacoes
		position = new Rect (0.36f*Screen.width, (1 *Screen.height/10), 289 * Screen.width/1024, 414 * Screen.height/768);
		GUI.DrawTexture(position,(Texture)Util.LoadImageResource(STR.Resources.SCROLL));

		
		/*Seta tamanho da fonte da quantitade de item */
		style.fontSize = (int)(Screen.height/23);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleCenter;
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);

		 

		/**** Primeira posicao na esquerda => TELESTRANSPORTE ****/
		position = new Rect (Screen.width/8,(2* Screen.height/11), Screen.width/6, Screen.height/6);

		if (GUI.Button (position, "")) {
			//Marca como selecionado
			selected = Barracks.ItemClass.Teletransport;

			//Volta todos itens para estado de nao clicado
			ImageItemInitial();

			//Coloca esse item como clicado
			itemImage[0] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.Teletransport) + STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX)); 

			//sounds 
			//efeito apos selecionar um item
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/teletransportEffect")as AudioClip);

		}
		GUI.DrawTexture(new Rect(position.x-Screen.width/15, position.y - Screen.height/9,  300 * Screen.width/1024, 300 * Screen.height/768), itemImage[0]);

		//Desenha label com quantidade disponivel
		Rect iconeQuantPos = new Rect (position.x + 3.5F * 3* Screen.width/112, position.y+3.5F*Screen.height/28, quantityItemIcon.width*Screen.width/2048, quantityItemIcon.height*Screen.height/1536);
		GUI.DrawTexture(iconeQuantPos, quantityItemIcon);
		Rect valueQuantPos = new Rect (position.x + 0.096F * Screen.width, position.y + 3.6F * Screen.height/28, Screen.width/33, Screen.height/22);
		GUI.Label(valueQuantPos, Barracks.getItemCount(Barracks.ItemClass.Teletransport).ToString(),  style);


		/**** Segunda posicao na esquerda => ATAQUE THUNDER ****/
		float distanceBetweenHeight = Screen.height / 5; 
		position = new Rect (Screen.width/8,(distanceBetweenHeight) + (2*Screen.height/10), Screen.width/6, Screen.height/6);
		
		if (GUI.Button (position,"")) {
			selected = Barracks.ItemClass.AttackThunder;

			//Volta todos itens para estado de nao clicado
			ImageItemInitial();
			
			//Coloca esse item como clicado
			itemImage[1] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.AttackThunder) + STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX));

			//sounds 
			//efeito apos selecionar um item
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/thunderStormEffect")as AudioClip);
			
		}
		GUI.DrawTexture(new Rect(position.x-Screen.width/15, position.y - Screen.height/9, 300 * Screen.width/1024, 300 * Screen.height/768),  itemImage[1]);

		//Desenha label com quantidade disponivel
		iconeQuantPos = new Rect (position.x + 3.5F * 3* Screen.width/112, position.y+3.5F*Screen.height/28, quantityItemIcon.width*Screen.width/2048, quantityItemIcon.height*Screen.height/1536);
		GUI.DrawTexture(iconeQuantPos, quantityItemIcon);
		valueQuantPos = new Rect (position.x + 0.096F * Screen.width, position.y + 3.6F * Screen.height/28, Screen.width/33, Screen.height/22);
		GUI.Label(valueQuantPos, Barracks.getItemCount(Barracks.ItemClass.AttackThunder).ToString(),  style);


		/**** Terceira posicao na esquerda => BURACO NEGRO ****/
		position = new Rect (Screen.width/8,(2*distanceBetweenHeight) + (2*Screen.height/10), Screen.width/6, Screen.height/6);
		
		if (GUI.Button (position, "")) {
			selected = Barracks.ItemClass.AttackBlackHole;

			//Volta todos itens para estado de nao clicado
			ImageItemInitial();
			
			//Coloca esse item como clicado
			itemImage[2] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.AttackBlackHole) + STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX));

			//sounds 
			//efeito apos selecionar um item
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/blackHoleEffect")as AudioClip);
			
		}
		GUI.DrawTexture(new Rect(position.x-Screen.width/15, position.y - Screen.height/9, 300 * Screen.width/1024, 300 * Screen.height/768), itemImage[2]);

		//Desenha label com quantidade disponivel
		iconeQuantPos = new Rect (position.x + 3.5F * 3* Screen.width/112, position.y+3.5F*Screen.height/28, quantityItemIcon.width*Screen.width/2048, quantityItemIcon.height*Screen.height/1536);
		GUI.DrawTexture(iconeQuantPos, quantityItemIcon);
		valueQuantPos = new Rect (position.x + 0.096F * Screen.width, position.y + 3.6F * Screen.height/28, Screen.width/33, Screen.height/22);
		GUI.Label(valueQuantPos, Barracks.getItemCount(Barracks.ItemClass.AttackBlackHole).ToString(),  style);


		/**** Primeira posicao na direita => UPGRADE DE DEFESA ****/
		position = new Rect (8 * Screen.width/11,(3 *Screen.height/16), Screen.width/6, Screen.height/6);
		
		if (GUI.Button (position, "")) {
			selected = Barracks.ItemClass.UpgradeDefense;

			//Volta todos itens para estado de nao clicado
			ImageItemInitial();
			
			//Coloca esse item como clicado
			itemImage[3] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.UpgradeDefense) + STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX));

			//sounds 
			//efeito apos selecionar um item
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/upgradeDefenseEffect")as AudioClip);
			
		}

		GUI.DrawTexture(new Rect(position.x-Screen.width/15, position.y - Screen.height/8, 300 * Screen.width/1024, 300 * Screen.height/768), itemImage[3]);

		//Desenha label com quantidade disponivel
		iconeQuantPos = new Rect (position.x + 3.5F * 3* Screen.width/112, position.y+3.5F*Screen.height/28, quantityItemIcon.width*Screen.width/2048, quantityItemIcon.height*Screen.height/1536);
		GUI.DrawTexture(iconeQuantPos, quantityItemIcon);
		valueQuantPos = new Rect (position.x + 0.096F * Screen.width, position.y + 3.6F * Screen.height/28, Screen.width/33, Screen.height/22);
		GUI.Label(valueQuantPos, Barracks.getItemCount(Barracks.ItemClass.UpgradeDefense).ToString(),  style);

		
		/**** Segundo posicao na direita => UPGRADE FORCA ****/
		position = new Rect (8 * Screen.width/11,(distanceBetweenHeight) + (3 *Screen.height/15), Screen.width/6, Screen.height/6);
		
		if (GUI.Button (position, "")) {
			selected = Barracks.ItemClass.UpgradeForce;

			//Volta todos itens para estado de nao clicado
			ImageItemInitial();
			
			//Coloca esse item como clicado
			itemImage[4] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.UpgradeForce) + STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX));

			//sounds 
			//efeito apos selecionar um item
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/upgradeForceEffect")as AudioClip);
			
		}

		GUI.DrawTexture(new Rect(position.x-Screen.width/15, position.y - Screen.height/9 - Screen.height/110, 300 * Screen.width/1024, 300 * Screen.height/768), itemImage[4]);

		//Desenha label com quantidade disponivel
		iconeQuantPos = new Rect (position.x + 3.5F * 3* Screen.width/112, position.y+3.5F*Screen.height/28, quantityItemIcon.width*Screen.width/2048, quantityItemIcon.height*Screen.height/1536);
		GUI.DrawTexture(iconeQuantPos, quantityItemIcon);
		valueQuantPos = new Rect (position.x + 0.096F * Screen.width, position.y + 3.6F * Screen.height/28, Screen.width/33, Screen.height/22);
		GUI.Label(valueQuantPos, Barracks.getItemCount(Barracks.ItemClass.UpgradeForce).ToString(),  style);


		/**** Terceiro posicao na direita => UPGRADE AGILIDADE ****/
		position = new Rect (8 * Screen.width/11,(2*distanceBetweenHeight) + (3 *Screen.height/14), Screen.width/6, Screen.height/6);
		
		if (GUI.Button (position, "")) {
			selected = Barracks.ItemClass.UpgradeAgility;

			//Volta todos itens para estado de nao clicado
			ImageItemInitial();
			
			//Coloca esse item como clicado
			itemImage[5] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.UpgradeAgility) + STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX));

			//sounds 
			//efeito apos selecionar um item
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/upgradeAgilityEffect")as AudioClip);
			
		}

		GUI.DrawTexture(new Rect(position.x-Screen.width/15, position.y - Screen.height/9 - Screen.height/110, 300 * Screen.width/1024, 300 * Screen.height/768), itemImage[5]);

		//Desenha label com quantidade disponivel
		iconeQuantPos = new Rect (position.x + 3.5F * 3* Screen.width/112, position.y+3.5F*Screen.height/28, quantityItemIcon.width*Screen.width/2048, quantityItemIcon.height*Screen.height/1536);
		GUI.DrawTexture(iconeQuantPos, quantityItemIcon);
		valueQuantPos = new Rect (position.x + 0.096F * Screen.width, position.y + 3.6F * Screen.height/28, Screen.width/33, Screen.height/22);
		GUI.Label(valueQuantPos, Barracks.getItemCount(Barracks.ItemClass.UpgradeAgility).ToString(),  style);

		
		/**** Ultimo estante embaixo e direita => CURAR CRISTAL ****/
		position = new Rect (  Screen.width/2, 103 * Screen.height/115 - Screen.height/6 , Screen.width/6, Screen.height/6);
		
		if (GUI.Button (position, "")) {
			selected = Barracks.ItemClass.CureCristal;

			//Volta todos itens para estado de nao clicado
			ImageItemInitial();
			
			//Coloca esse item como clicado
			itemImage[7] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.CureCristal) + STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX));

			//sounds 
			//efeito apos selecionar um item
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/crystalHealEffect")as AudioClip);
		}

		GUI.DrawTexture(new Rect(position.x - Screen.width/12, position.y - Screen.height/14, 300 * Screen.width/1024, 300 * Screen.height/768), itemImage[7]);

		//Desenha label com quantidade disponivel
		iconeQuantPos = new Rect (position.x + 3.5F * 3* Screen.width/112, position.y+3.5F*Screen.height/28, quantityItemIcon.width*Screen.width/2048, quantityItemIcon.height*Screen.height/1536);
		GUI.DrawTexture(iconeQuantPos, quantityItemIcon);
		valueQuantPos = new Rect (position.x + 0.096F * Screen.width, position.y + 3.6F * Screen.height/28, Screen.width/33, Screen.height/22);
		GUI.Label(valueQuantPos, Barracks.getItemCount(Barracks.ItemClass.CureCristal).ToString(),  style);


		
		/**** Ultimo estante embaixo e esquerda => CURAR CARACTER ****/
		position = new Rect (Screen.width/2 - Screen.width/6 ,103 * Screen.height/115 - Screen.height/6, Screen.width/6, Screen.height/6);
		
		if (GUI.Button (position, "")) {
			selected = Barracks.ItemClass.CureCharacter;

			//Volta todos itens para estado de nao clicado
			ImageItemInitial();
			
			//Coloca esse item como clicado
			itemImage[6] = (Texture)Util.LoadImageResource (Barracks.getNameOfClassItem (Barracks.ItemClass.CureCharacter) + STR.get(STR.Resources.BUTTON_PRESSED_SUFFIX));

			//sounds 
			//efeito apos selecionar um item
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/healTroopsEffect")as AudioClip);
			
		}
		GUI.DrawTexture(new Rect(position.x - Screen.width/13, position.y - Screen.height/8, 300 * Screen.width/1024, 300 * Screen.height/768), itemImage[6]);

		//Desenha label com quantidade disponivel
		iconeQuantPos = new Rect (position.x + 3.5F * 3* Screen.width/112, position.y+3.5F*Screen.height/28, quantityItemIcon.width*Screen.width/2048, quantityItemIcon.height*Screen.height/1536);
		GUI.DrawTexture(iconeQuantPos, quantityItemIcon);
		valueQuantPos = new Rect (position.x + 0.096F * Screen.width, position.y + 3.6F * Screen.height/28, Screen.width/33, Screen.height/22);
		GUI.Label(valueQuantPos, Barracks.getItemCount(Barracks.ItemClass.CureCharacter).ToString(),  style);

	}

	/* Metodo para mostrar informacoes de um item clicado e opcao para comprar
	 * no papiro no centro da tela.
	 * */
	void showInfoItem (){
		//Posicao do header no papiro central
		//Fonte usada
		style.fontSize = (int)(Screen.height/20);
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleCenter;
		
		Rect pergaminho = new Rect (0.36f*Screen.width, (1 *Screen.height/10), 289 * Screen.width/1024, 414 * Screen.height/768);

		float distComponent = pergaminho.height / 20;

		//Header
		style.fontStyle = FontStyle.Bold;
		string headerText;
		if (selected == Barracks.ItemClass.None)
			headerText =  STR.get("Selecione um Item");
		else
			headerText = Barracks.getShowableItemName (selected);

		Rect header = new Rect (pergaminho.x, pergaminho.y + distComponent, pergaminho.width, pergaminho.height/9);
		GUI.Label (header, headerText, style);

		if(selected != Barracks.ItemClass.None){
			//Descricao
			style.fontStyle = FontStyle.Normal;
			style.fontSize = (int)(Screen.height / 22);
			style.wordWrap = true;

			string text = Barracks.getDescriptionOfClassItem(selected);
			Rect description = new Rect(header.x*1.03f, header.y*1.1f + header.height, pergaminho.width*0.90f, 3.2f*pergaminho.height/10);
			GUI.TextField (description, text, style);


			//Icone de preco
			Rect priceIcon = new Rect (description.x*0.9f + description.width/2 - 70 * Screen.width/1024, description.y*1.1f + description.height, 70 * Screen.width/1024,70 * Screen.height/768 ); 
			GUI.DrawTexture (priceIcon, (Texture)Util.LoadImageResource (STR.Resources.COIN_OVER));


			//Valor do preco
			style.fontStyle = FontStyle.Bold;
			style.wordWrap = false; //Volta pra estado padrao
			style.fontSize	= (int)(Screen.height / 8);
			style.alignment = TextAnchor.MiddleCenter;

			Rect priceValue = new Rect (priceIcon.x*1.1f + priceIcon.width, priceIcon.y, priceIcon.width, priceIcon.height);
			int valuePrice = Barracks.getPriceOfClassItem (selected);
			GUI.Label (priceValue, valuePrice.ToString (), style);

			//Botao de comprar
			Rect buyButtonPos = new Rect ( pergaminho.x + (pergaminho.width - 150 * Screen.width / 1024)/2, priceValue.y + priceValue.height, 150 * Screen.width / 1024, 100 * Screen.height / 768);

			if(GUI.RepeatButton(buyButtonPos, "")){
				buyButton = (Texture)Util.LoadImageResource (STR.TranslatedResources.BUY_BUTTON_PRESSED);

				if(!buyButtonClicked){
					if(Database.getPlayerMoney() >= valuePrice){
						//Atualiza dinheiro no banco removendo o preco
						Database.updatePlayerMoney((float) Database.getPlayerMoney() - valuePrice);

						//sounds
						SoundController.playSoundEffect (effects, Resources.Load ("Sounds/cashEffect")as AudioClip);
						
						//Aumenta numero de itens do mesmo
						Barracks.updateItemCount(selected, Barracks.getItemCount(selected) + 1);
					}
					else {

						errorNoMoney();
					}
				}


				buyButtonClicked = true;

				timePassed = 0;

			}
			else if(buyButtonClicked && timePassed > threshold){
				buyButton = (Texture)Util.LoadImageResource (STR.TranslatedResources.BUY_BUTTON);

				buyButtonClicked = false;
			}

			GUI.DrawTexture (buyButtonPos, buyButton);
		}
	}

	//Metodo chamado quando usuario compra algo e nao possui dinheiro suficiente.
	//Ele deve implementar um tratador para da ro feedback ao usuario
	void errorNoMoney(){

		//sounds
		SoundController.playSoundEffect (effects, Resources.Load ("Sounds/noCashEffect")as AudioClip);
		
	}

}




	

