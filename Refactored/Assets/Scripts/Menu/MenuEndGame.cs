﻿using UnityEngine;
using System.Collections;

public class MenuEndGame : MonoBehaviour {
	/*Interface para saber resultado */
	public	static bool		wonGame;
	public	static int[]	heroDeadQuantity;
	public	static int		earnedMoneyValue;
	
	//Variavel para desenhar estilo
	GUIStyle style = new GUIStyle();
	
	/**Variavel para realizar efeito de pressionar botao**/
	float timePassed = 0; //Controla o tempo pasado de ate um dado frame
	const float threshold = 0.1F;//Limitante de tempo para efeito de botao pressionar
	
	/*Variaveis com relacao ao background*/
	Rect	backImagePos;
	Texture backImage;
	
	/*Variaveis de info se venceu/perdeu batalha*/
	Rect	resultOfBattlePos;
	string	resultOfBattleInf;
	
	/*Variaveis de header texto heroi mortos em batalha*/
	Rect	infoDeadHeaderPos;
	string	infoDeadHeader;
	
	/*Variaveis de cada heroi morto*/
	Rect[]	heroDeadImagePos;
	Texture[] heroDeadImage;
	Rect[]	heroDeadQuantityPos;
	
	
	/*Variavel de info dim obtido*/
	Rect	infoMoneyHeaderPos;
	string	infoMoneyHeader;
	
	/*Variaveis de valor dim ganho*/
	Rect	earnedMoneyImagePos;
	Texture earnedMoneyImage;
	Rect	earnedMoneyValuePos;
	
	/*Variaveis com relacao ao botao voltar para map*/
	Rect	goToMapPos;
	Texture goToMap;
	bool	clickedGoToMap;
	
	
	void Awake(){
		//MIGUE PARA CORRIGIR BUG DA MUDANCA DE ESTADO E PAUSE
		Time.timeScale = 1;
		
		//Fonte usada
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		
		
		/*Carrega background*/
		backImagePos = new Rect(0,0, Screen.width, Screen.height);
		
		if(wonGame)
			backImage = (Texture)Util.LoadImageResource (STR.Resources.BACK_END_GAME_VICTORY);
		else
			backImage = (Texture)Util.LoadImageResource (STR.Resources.BACK_END_GAME_DEFEAT);
		
		
		/*Carrega info header*/
		resultOfBattlePos = new Rect (0, 0.27f*Screen.height, Screen.width, 0.1f*Screen.height);
		if(wonGame)
			resultOfBattleInf	  = STR.get("InfoHeaderWon");
		else
			resultOfBattleInf	  = STR.get("InfoHeaderLost");
		
		
		float space = Screen.height / 20;
		
		/*Carrega header de herois mortos info*/
		infoDeadHeaderPos 	= new Rect (Screen.width/2 - (Screen.width/10)/2, resultOfBattlePos.y + resultOfBattlePos.height + space/2, Screen.width/10 , resultOfBattlePos.height/2);
		infoDeadHeader	= STR.get("InfoHeaderHeroDead");
		
		
		/*Carrega valores de herois mortos*/
		heroDeadImage 		= new Texture[4];
		heroDeadImagePos	= new Rect[4];
		heroDeadQuantityPos = new Rect[4];
		
		
		for(int i =0; i < 4; i++){
			//Pega heroi equivalente
			Barracks.HeroClass cls = (Barracks.HeroClass)((int)Barracks.HeroClass.Fighter + i);
			
			
			/*Carrega imagem do heroi morto*/
			heroDeadImage[i]   =  Util.LoadImageResource(Barracks.getNameOfClass(cls) +  STR.get(STR.Resources.ICON_SUFFIX_DEAD)) as Texture;
			
			if(i < 2)
				/*Calcula posicao equivalente da imagem*/
				heroDeadImagePos[i] = new Rect(infoDeadHeaderPos.x - (heroDeadImage[i].width * Screen.width/2048), infoDeadHeaderPos.y + infoDeadHeaderPos.height  + (i+1) * (space/4) + i*(heroDeadImage[i].height * Screen.height/1536), heroDeadImage[i].width * Screen.width/2048, heroDeadImage[i].height * Screen.height/1536);
			else
				heroDeadImagePos[i] = new Rect((infoDeadHeaderPos.x + infoDeadHeaderPos.width) - (heroDeadImage[i].width * Screen.width/2048), infoDeadHeaderPos.y + infoDeadHeaderPos.height + (i-1) * (space/4) + (i-2)*(heroDeadImage[i].height * Screen.height/1536), heroDeadImage[i].width * Screen.width/2048, heroDeadImage[i].height * Screen.height/1536);
			
			/*Calcula posicao equivalente do texto*/
			heroDeadQuantityPos[i] = new Rect(heroDeadImagePos[i].x + heroDeadImagePos[i].width, heroDeadImagePos[i].y, heroDeadImagePos[i].width, heroDeadImagePos[i].height);
		}
		
		/*Carrega info header de dinheiro ganhado*/
		infoMoneyHeaderPos	= new Rect (infoDeadHeaderPos.x, heroDeadImagePos[3].y + heroDeadImagePos[3].height + space/2,infoDeadHeaderPos.width, infoDeadHeaderPos.height);
		infoMoneyHeader		= STR.get("InfoHeaderMoneyEarned");
		
		
		/*Carrega valor ganhado*/
		earnedMoneyImage 		= (Texture)Util.LoadImageResource (STR.Resources.PRICE_OVER);
		earnedMoneyImagePos		= new Rect(Screen.width/2 - (earnedMoneyImage.width * Screen.width/2048), infoMoneyHeaderPos.y + infoMoneyHeaderPos.height + space/4 , earnedMoneyImage.width * Screen.width/2048, earnedMoneyImage.height * Screen.height/1536);
		earnedMoneyValuePos		= new Rect(earnedMoneyImagePos.x + earnedMoneyImagePos.width, earnedMoneyImagePos.y - 0.6f*earnedMoneyImagePos.height, earnedMoneyImagePos.width, 2*earnedMoneyImagePos.height);
		
		
		/*Carrega botao de voltar para mapa*/
		goToMap		= (Texture)Util.LoadImageResource (STR.TranslatedResources.GO_TO_MAP);
		goToMapPos	= new Rect (Screen.width/2 - (goToMap.width * Screen.width/2048)/2, earnedMoneyImagePos.y + earnedMoneyImagePos.height + space/2, goToMap.width * Screen.width/2048, goToMap.height * Screen.height/1536);
		
	}
	
	void Update (){
		timePassed += Time.deltaTime;

	}
	
	void OnGUI(){
		//Remove efeito de box
		GUI.backgroundColor = Color.clear; 
		
		/*Coloca background*/
		GUI.DrawTexture (backImagePos, backImage);
		
		/*Coloca header se venceu ou perdeu*/
		style.fontSize = (int)(0.4f*Screen.height/6);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
		style.alignment = TextAnchor.MiddleCenter;
		
		GUI.Label (resultOfBattlePos, resultOfBattleInf, style);
		GUI.Button (resultOfBattlePos, "");
		
		/*Coloca subheader de info dead heroes*/
		style.fontSize = (int)(0.4f*Screen.height/8);
		style.fontStyle = FontStyle.Normal;
		style.normal.textColor = Color.white;
		
		GUI.Label (infoDeadHeaderPos, infoDeadHeader, style);
		GUI.Button (infoDeadHeaderPos, "");
		
		
		/*Coloca herois e seus numeros mortos*/
		style.fontSize = (int)(0.4f*Screen.height/8);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
		
		style.normal.textColor = Color.white;
		for(int i =0; i < 4; i++){
			
			//Coloca imagem
			GUI.DrawTexture(heroDeadImagePos[i], heroDeadImage[i]);
			
			//Coloca valor
			GUI.Label(heroDeadQuantityPos[i], heroDeadQuantity[i].ToString(), style);
			GUI.Button (heroDeadQuantityPos[i], "");
			
		}
		
		
		/*Coloca subheader de info de money earned*/
		style.fontSize = (int)(0.4f*Screen.height/8);
		style.fontStyle = FontStyle.Normal;
		style.normal.textColor = Color.white;
		
		GUI.Label(infoMoneyHeaderPos, infoMoneyHeader, style);
		GUI.Button (infoMoneyHeaderPos, "");
		
		/*Coloca valor ganho*/
		style.fontSize = (int)(80);
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
		style.alignment = TextAnchor.MiddleCenter;
		
		GUI.DrawTexture (earnedMoneyImagePos, earnedMoneyImage);
		GUI.Label		(earnedMoneyValuePos, earnedMoneyValue.ToString(), style);
		GUI.Button (earnedMoneyValuePos, "");
		
		/*Coloca botao de voltar para map*/
		backMapButton ();
	}
	
	
	void backMapButton(){
		if(GUI.RepeatButton (goToMapPos, "")  ){
			//Vai para estado pressionado
			goToMap = (Texture) Util.LoadImageResource(STR.TranslatedResources.GO_TO_MAP_PRESSED);
			clickedGoToMap = true;
			timePassed = 0;
			
			GUI.DrawTexture(goToMapPos, goToMap);
			
		}
		else if(clickedGoToMap && timePassed > threshold){
			//Faz operacao
			Application.LoadLevel ("MainMenu");
			
			//Volta para estado despressionado
			goToMap = (Texture) Util.LoadImageResource(STR.TranslatedResources.GO_TO_MAP);
			clickedGoToMap = true;
			
			GUI.DrawTexture(goToMapPos, goToMap);
		}
		else
			GUI.DrawTexture(goToMapPos, goToMap);
		
	}
}
