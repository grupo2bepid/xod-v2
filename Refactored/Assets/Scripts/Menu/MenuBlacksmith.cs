﻿using UnityEngine;
using System.Collections;

public class MenuBlacksmith : MonoBehaviour {
	
//	private const float topHeight = 10;
//	private const float middleHeight = 67;
//	private const float bottomHeight = 23;
//	private const float middleColumn = 35;
//	
//	private static string heroPrefabPath = "Prefabs/";
//	
//	private Texture2D texture;
//	
//	private Object heroPrefab;
//	
//	public Transform Target;
//	
//	Barracks.HeroClass currentHero = (Barracks.HeroClass) 0;
//	
//	// Use this for initialization
//	void Start () {
//		loadPrefab ();
//	}
//	
//	// Update is called once per frame
//	void Update () {
//		
//	}
//	
//	void OnGUI() {
//		
//		drawBackgrounds ();
//		drawHUD ();
//		drawButtons ();
//		
//	}
//	
//	void drawRect(Rect position, Color color){
//		if (texture != null) {
//			Texture2D.DestroyImmediate (texture, true);
//		}
//		texture = new Texture2D(1, 1);
//		texture.SetPixel(0,0,color);
//		texture.Apply();
//		GUI.skin.box.normal.background = texture;
//		GUI.Box(position, GUIContent.none);
//	}
//	
//	void drawBackgrounds(){
//		
//		Rect position = new Rect (0, 0, Screen.width, Screen.height*topHeight/100);
//		Color color = Color.red;
//		drawRect (position, color);
//		
//		position = new Rect (Screen.width*middleColumn/100, Screen.height*topHeight/100, 0.65f*Screen.width, Screen.height*middleHeight/100);
//		color = Color.yellow;
//		drawRect (position, color);
//		
//		position = new Rect (0, Screen.height - Screen.height*bottomHeight/100, Screen.width, Screen.height*bottomHeight/100);
//		color = Color.green;
//		drawRect (position, color);
//		
//		
//		//Level
//		position = new Rect(Screen.width*middleColumn/100 + 0.10f*(Screen.width - Screen.width*middleColumn/100) , Screen.height * (topHeight/100 + 0.05f*middleHeight/100), 0.10f*Screen.height*middleHeight/100, 0.10f*Screen.height*middleHeight/100);
//		color = Color.green;
//		drawRect (position, color);
//		
//		int quantity = 5;
//		for (int i = 0; i<quantity; i++) {
//			position = new Rect(Screen.width*middleColumn/100 + 0.10f*(Screen.width - Screen.width*middleColumn/100) , Screen.height * (topHeight/100 + (0.20f + i*0.12f)*middleHeight/100), 0.8f*(Screen.width - Screen.width*middleColumn/100), 0.10f*Screen.height*middleHeight/100);
//			color = Color.green;
//			drawRect (position, color);
//			
//			//Icon
//			position = new Rect(Screen.width*middleColumn/100 + 0.10f*(Screen.width - Screen.width*middleColumn/100) , Screen.height * (topHeight/100 + (0.20f + i*0.12f)*middleHeight/100), 0.1f*(Screen.width - Screen.width*middleColumn/100), 0.10f*Screen.height*middleHeight/100);
//			color = Color.red;
//			drawRect (position, color);
//			
//			//Bar
//			position = new Rect(Screen.width*middleColumn/100 + 0.225f*(Screen.width - Screen.width*middleColumn/100), Screen.height * (topHeight/100 + (0.20f + i*0.12f)*middleHeight/100), 0.35f*(Screen.width - Screen.width*middleColumn/100), 0.10f*Screen.height*middleHeight/100);
//			color = Color.gray;
//			drawRect (position, color);
//			
//			//Details
//			position = new Rect(Screen.width*middleColumn/100 + 0.60f*(Screen.width - Screen.width*middleColumn/100), Screen.height * (topHeight/100 + (0.20f + i*0.12f)*middleHeight/100), 0.30f*(Screen.width - Screen.width*middleColumn/100), 0.10f*Screen.height*middleHeight/100);
//			color = Color.blue;
//			drawRect (position, color);
//		}
//		
//		//Update Button
//		position = new Rect(Screen.width*middleColumn/100 + 0.30f*(Screen.width - Screen.width*middleColumn/100) , Screen.height * (topHeight/100 + 0.85f*middleHeight/100), 0.4f*(Screen.width - Screen.width*middleColumn/100), 0.10f*Screen.height*middleHeight/100);
//		color = Color.green;
//		drawRect (position, color);
//		
//	}
//	
//	void drawHUD(){
//		
//		GUIStyle style = new GUIStyle();
//		
//		//Fonte usada
//		style.fontSize = (int)(0.4f*Screen.height*topHeight/100);
//		style.fontStyle = FontStyle.Bold;
//		style.normal.textColor = Color.white;
//		style.alignment = TextAnchor.MiddleCenter;
//		
//		GUI.DrawTexture(new Rect(0,0, 0.5f* Screen.width / 6, 0.5f* Screen.width / 6), (Texture) Util.LoadImageResource("gold"), ScaleMode.StretchToFill, true,  0.5f* Screen.width / 6);
//		GUI.Label(new Rect(0.75f* Screen.width / 6, 0.1f*Screen.height*topHeight/100, 0.5f* Screen.width / 6, 0.3f*Screen.height*topHeight/100), ""+Player.money, style);
//		
//		GUI.Label(new Rect(Screen.width*middleColumn/100 + 0.25f*(Screen.width - Screen.width*middleColumn/100) , Screen.height * (topHeight/100 + 0.075f*middleHeight/100), 0.5f*(Screen.width - Screen.width*middleColumn/100), 0.05f*Screen.height*middleHeight/100), Barracks.getNameOfClass(currentHero), style);
//		
//		int quantity = 5;
//		for (int i = 0; i<quantity; i++) {
//			
//			//Bar
//			GUI.Label(new Rect(Screen.width*middleColumn/100 + 0.225f*(Screen.width - Screen.width*middleColumn/100), Screen.height * (topHeight/100 + (0.20f + i*0.12f)*middleHeight/100), 0.35f*(Screen.width - Screen.width*middleColumn/100), 0.10f*Screen.height*middleHeight/100),""+Hero.getNameOfAttribute((Hero.Attributes)i)+" "+ Barracks.getValueOfAttribute((Hero.Attributes)i, currentHero));
//			
//		}
//		
//	}
//	
//	void drawButtons(){
//		
//		//MainMenu
//		if (GUI.Button (new Rect (0.9f * Screen.width, 0.0f * Screen.height * topHeight / 100,  Screen.height * topHeight / 100, Screen.height * topHeight / 100), "X")) {
//			Application.LoadLevel ("MainMenu"); //Carrega scene inicial.
//			
//			
//		}
//		
//		else if(GUI.Button (new Rect(Screen.width*middleColumn/100 + 0.30f*(Screen.width - Screen.width*middleColumn/100) , Screen.height * (topHeight/100 + 0.85f*middleHeight/100), 0.4f*(Screen.width - Screen.width*middleColumn/100), 0.10f*Screen.height*middleHeight/100), "Upgrade")){
//			if(Blacksmith.upgradeClass(currentHero)){
//				StartCoroutine(EffectCoroutine());
//
//			}
//		}
//		
//		//Load Hero Buttons
//		else {
//			int quantity = 4;
//			float space = 0.005f*Screen.width;
//			float width = (Screen.width - (quantity - 1)*space)/quantity;
//			
//			for(int i = 0; i < quantity; i++){
//				
//				if(GUI.Button (new Rect (i*(width + space), Screen.height * (1 - 0.9f*bottomHeight/100) , width, 0.8f*Screen.height*bottomHeight/100), ""+Barracks.getNameOfClass((Barracks.HeroClass) i ))){
//					currentHero = (Barracks.HeroClass) i;
//					loadPrefab();
//				}
//				
//			}
//			
//		}
//		
//		int qnt = 5;
//		for (int i = 0; i< qnt; i++) {
//			
//			
//			if(GUI.Button (new Rect(Screen.width*middleColumn/100 + 0.60f*(Screen.width - Screen.width*middleColumn/100), Screen.height * (topHeight/100 + (0.20f + i*0.12f)*middleHeight/100), 0.30f*(Screen.width - Screen.width*middleColumn/100), 0.10f*Screen.height*middleHeight/100), "Upgrade $"+Blacksmith.getAttributePrice((Hero.Attributes) i, currentHero))){
//				currentHero = (Barracks.HeroClass) i;
//				Blacksmith.upgradeAttribute((Hero.Attributes) i, currentHero);
//				loadPrefab();
//			}
//			
//		}
//		
//		
//		
//	}
//
//	IEnumerator EffectCoroutine(){
//
//		loadUpgradeEffect();
//	
////		LeanTween.alpha((GameObject)heroPrefab, 0.0f, 1.0f);
//		yield return new WaitForSeconds(1.2f);    //Wait one frame
//
//		loadPrefab();
//
//	}
//
//	void loadUpgradeEffect(){
//
//		GameObject effectPrefab = (GameObject) Util.LoadPrefabResource("sacredGround");
//		effectPrefab.transform.LookAt (Target);
//		effectPrefab.transform.position = Vector3.zero;
//		effectPrefab.transform.localScale = Vector3.one;
//		GameObject.Instantiate (effectPrefab);
//		
//	}
//
//	void loadPrefab(){
//		
//		if(heroPrefab)
//			GameObject.DestroyObject (heroPrefab);
//		
//		GameObject testPrefab = (GameObject) Util.LoadPrefabForMenuOfHeroClass (currentHero);
//		testPrefab.transform.LookAt (Target);
//		testPrefab.transform.position = Vector3.zero;
//		testPrefab.transform.localScale = Vector3.one;
//		heroPrefab = GameObject.Instantiate (testPrefab);
//		
//	}
}
