﻿using UnityEngine;
using System.Collections;

public class BlinkEffect : MonoBehaviour {

	private float blinkInterval = 0.3f;
	private float timeSinceLastBlink = -0.3f;

	private void alternateMeshState(){
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);

		foreach (Renderer component in rendererComponents) {
			component.enabled = !component.enabled ;
		}
	}

	private void turnOffColliders(){
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		foreach (Collider component in colliderComponents)
			component.enabled = false;
	}
	
	void Awake(){
		turnOffColliders ();
	}

	void Update(){
		timeSinceLastBlink += Time.deltaTime;
		if( timeSinceLastBlink > blinkInterval ){
			timeSinceLastBlink = 0f;
			alternateMeshState();
		}
	}

}
