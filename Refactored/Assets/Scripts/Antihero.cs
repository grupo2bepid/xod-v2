﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Antihero : Character {

	private List<Hero> waitingHeroes;
	private GameObject enemiesGivingDamageEffectPrefab;
	private Hero heroFightPartner;

	protected float acceptableDistanceForHitCristal;
	protected bool hittingCristal;
	protected Cristal cristal;
	protected float bountyMultiplier;
	protected float lifeMultiplier;
	protected float attackMultiplier;
	
	new protected void Awake(){
		base.Awake ();
		waitingHeroes = new List<Hero> ();
		Fighting = false;
		enemiesGivingDamageEffectPrefab = Util.LoadPrefabResource ("EnemiesGivingDamageEffect") as GameObject;

		float numberOfVictories = (float)Database.getNumberOfVictories ();
		bountyMultiplier = (numberOfVictories + 3f)*0.33f;
		lifeMultiplier = (numberOfVictories + 1f);
		attackMultiplier = (numberOfVictories + 1f);
	}

	new protected void Start(){
		base.Start ();
		cristal = GameController.cristal;

		if (cristal == null) {
			cristal = FindObjectOfType<Cristal>();
		}

		if (this is Boss) {
		}
		else StartCoroutine ( "checkingDistanceFromCristalRoutine" );
	}

	public void fightWithMe( Hero hero ){
		if (!Dying) {
			if (Fighting && !hittingCristal) {
					waitingHeroes.Add (hero);
			} else {
					hittingCristal = false;
					heroFightPartner = hero;
					lookToTarget = hero.transform;
					StopCoroutine ("checkingDistanceFromCristalRoutine");
					StartCoroutine ("onFightRoutine");
					lookToTarget = hero.transform;
					Fighting = true;
			}
		}
	}

	private void startFighting( Hero hero ){
		hittingCristal = false;
		heroFightPartner = hero;
		lookToTarget = hero.transform;
		StopCoroutine("checkingDistanceFromCristalRoutine");
		StartCoroutine("onFightRoutine");
		lookToTarget = hero.transform;
		Fighting = true;
	}
	
	protected IEnumerator checkingDistanceFromCristalRoutine(){
		while (true) {
			if( distance2D(transform.position, cristal.gameObject.transform.position ) <= acceptableDistanceForHitCristal ){
				lookToTarget = cristal.gameObject.transform;
				Fighting = true;
				hittingCristal = true;
				yield break;
			}
			yield return new WaitForSeconds (0.3f);
		}
	}

	private void getBackToCristalPath(){
		aipath.startWalk ();
		StartCoroutine ("checkingDistanceFromCristalRoutine");
	}

	private IEnumerator onFightRoutine(){
		while (true) {
			if( !hittingCristal && Fighting && ( heroFightPartner == null || heroFightPartner.Dying) ){

				Fighting = false;
				bool found = false;
				heroFightPartner = null;
				
				for( int i = waitingHeroes.Count - 1; i>=0; --i ){
					if(waitingHeroes[i] != null ){

						if(!Dying) animator.SetTrigger( HashIDs.forceIdleTrigger );
						yield return new WaitForSeconds(0.2f);



						startFighting( waitingHeroes[i] );
						waitingHeroes.RemoveAt(i);
						found = true;
						break;
					}
					else{
						waitingHeroes.RemoveAt(i);
					}
				}

				if(!found){
					if( this is Boss ){
						if(!Dying) animator.SetTrigger( HashIDs.forceIdleTrigger );
					} 
					else{
						aipath.stopWalk();

						if(!Dying) animator.SetTrigger( HashIDs.forceIdleTrigger );

						Invoke("getBackToCristalPath", 0.2f);
					}

					yield break;
				}
			}
			yield return new WaitForSeconds( 0.3f );
		}
	}

	protected void applyDamage(int attackBaseStrength){
		attackBaseStrength = (int)(attackBaseStrength*attackMultiplier);

		if (heroFightPartner != null && !heroFightPartner.Dying){
			Vector3 pos = heroFightPartner.transform.position;
			pos.y += 1f;
			Instantiate( enemiesGivingDamageEffectPrefab, pos, heroFightPartner.transform.rotation);
			heroFightPartner.sufferDamage (attackBaseStrength);
		} else if (hittingCristal){
			if( this is Boss ) cristal.getHit ( attackBaseStrength*5 );
			else cristal.getHit ( attackBaseStrength );
		}
	}

	
}
