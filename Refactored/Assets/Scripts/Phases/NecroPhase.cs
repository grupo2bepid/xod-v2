﻿using UnityEngine;
using System.Collections;

public class NecroPhase : Phase {

	override protected void fillPhaseAttributes(){

		insertEnemy ("Ghoul", InitialValues.GHOUL_LEVEL, 4);
		insertEnemy ("Mummy", InitialValues.MUMMY_LEVEL, 4);
		insertEnemy ("Vampire", InitialValues.VAMPIRE_LEVEL, 3);
		insertEnemy ("Skeleton", InitialValues.SKELETON_LEVEL, 3);

		bossPrefabReference = Util.LoadPrefabResource ("DemonInFire") as GameObject;
		
		phaseDuration = 100;
		phaseHeight = 1.67f;
	}
	
}
