﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Diagnostics;


public class EnemyTuple{

	public GameObject prefabReference;
	public int level;
	public int number;
	public EnemyTuple(GameObject pr, int l, int n){
		prefabReference = pr;
		level = l;
		number = n;
	}
}

public abstract class Phase : MonoBehaviour {

	protected List<EnemyTuple>[] enemiesMatrix;
	protected GameObject bossPrefabReference;
	protected float phaseDuration;
	protected float phaseHeight;

	private int totalNumberOfEnemies;
	private EnemySpawn enemySpawn;
	private float elapsedTime = 0f;
	private float baseSpawnDelay;
	private int maxLevel = -1;

	protected void insertEnemy(string prefabName, int level, int number){
		if (number > 0) {
			GameObject prefabReference = Util.LoadPrefabResource (prefabName) as GameObject;
			EnemyTuple et = new EnemyTuple (prefabReference, level, number);
			maxLevel = (level > maxLevel) ? level : maxLevel;
			totalNumberOfEnemies += number;
			enemiesMatrix [level].Add (et);
		}
	}

	protected void Awake(){
		enemiesMatrix = new List<EnemyTuple>[10];
		for (int i = 0; i < enemiesMatrix.Length; ++i)
			enemiesMatrix [i] = new List<EnemyTuple> ();
	}

	abstract protected void fillPhaseAttributes ();

	protected void Start(){
		if ( !Database.alreadyViewedTutorial() )
			playTutorial ();
		else
			playPhase ();
	}

	protected void Update(){
		elapsedTime += Time.deltaTime;
	}

	private void playTutorial(){
		GameObject inGameTutorialPrefab = Util.LoadPrefabResource ("InGameTutorial") as GameObject;
		InGameTutorial inGameTutorial =  (Instantiate (inGameTutorialPrefab) as GameObject).GetComponent<InGameTutorial>();
		inGameTutorial.setPhaseReference (this);
	}

	public void playPhase(){
		fillPhaseAttributes ();
		enemySpawn = FindObjectOfType (typeof(EnemySpawn)) as EnemySpawn;
		baseSpawnDelay = phaseDuration / totalNumberOfEnemies;
		
		Cristal cristal = FindObjectOfType ( typeof(Cristal) ) as Cristal;
		GameController.startNewFase (totalNumberOfEnemies, this, cristal, phaseHeight);
		StartCoroutine ("enemyIstantiationRoutine");
	}

	private GameObject getPrefabReferenceOfLevel(int level){
		GameObject prefabRef;

		int last = enemiesMatrix [level].Count - 1;
		prefabRef = enemiesMatrix [level] [last].prefabReference;
		if (--enemiesMatrix [level] [last].number <= 0) {
			enemiesMatrix[level].RemoveAt(last);
		}

		return prefabRef;
	}

	private GameObject nextEnemy(){

		List<int> remainingLevels = new List<int> ();


		for (int i = 1; i < enemiesMatrix.Length; ++i) {
			if( enemiesMatrix[i].Count > 0 ) remainingLevels.Add( i );
		}
		
		if (remainingLevels.Count == 0)
			return null;

		float elapsedTimeFraction = elapsedTime / phaseDuration;

		float levePositionMultiplier = Random.Range (0.3f, 1.2f);	//MAGIC CONSTANT
		int nextLevelPosition = (int)(remainingLevels.Count*elapsedTimeFraction*levePositionMultiplier);
		if (nextLevelPosition > remainingLevels.Count - 1)
			nextLevelPosition = remainingLevels.Count - 1;
		
		int nextEnemyLevel = remainingLevels [nextLevelPosition];
		
		GameObject nextPrefabRef = getPrefabReferenceOfLevel ( nextEnemyLevel );
		
		return nextPrefabRef;
	}

	public void invokeBoss(){
		enemySpawn.instantiateCharacter ( bossPrefabReference );

		//Avisa HUD que boss esta ativo
		HUD_refactored.isBossActived = true;
	}

	private IEnumerator enemyIstantiationRoutine(){
		while (true) {
			float delayBeforeNextInstantiation = baseSpawnDelay*Random.Range(0.7f,1.3f);
			yield return new WaitForSeconds( delayBeforeNextInstantiation );

			GameObject next = nextEnemy();

			if( next != null ){
				enemySpawn.instantiateCharacter( next );
			}
			else{
				yield break;
			}
		}
	}
}
