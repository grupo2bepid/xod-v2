﻿using UnityEngine;
using System.Collections;

public class InstantiationSystem : MonoBehaviour {
	
	private string objectName;
	private string tagSpot;
	private GameObject effectPrefab;
	private GameObject effectGameObject;
	private GameObject heroBeingTeletransported = null;

	public bool isTeleportation = false;

	public void startInstantiationSystem(string name){
		objectName = name;

		isTeleportation = ( objectName == "Teletransport" );
		tagSpot = Barracks.getTag (objectName);
		effectPrefab = Util.LoadPrefabResource ("LegalPlaceEffect") as GameObject;
		effectGameObject = Instantiate (effectPrefab) as GameObject;
		effectGameObject.SetActive (false);

		if( name != "AttackBlackHole" && name != "CureCristal" ) StartCoroutine ("particlePositionRoutine");
	}

	private bool objectIsHero(){
		if (objectName == Barracks.getNameOfClass (Barracks.HeroClass.AxeWarrior))
			return true;
		else if (objectName == Barracks.getNameOfClass (Barracks.HeroClass.MaceWarrior))
			return true;
		else if (objectName == Barracks.getNameOfClass (Barracks.HeroClass.WarhammerWarrior))
			return true;
		else if (objectName == Barracks.getNameOfClass (Barracks.HeroClass.Fighter))
			return true;
		else
			return false;

	}

	private IEnumerator particlePositionRoutine(){
		while (true) {
			Ray ray = Camera.main.ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0));
			RaycastHit hit;

			if ( Physics.Raycast (ray, out hit) == true && hit.collider != null && ( (tagSpot == "ANY") ? true : ( hit.collider.gameObject.tag == tagSpot ) ) ){
				Vector3 point = hit.point;
				if( tagSpot == "ApplicableItemArea" ) point = hit.collider.transform.position;
				effectGameObject.SetActive(true);
				effectGameObject.transform.position = point;
			}
			else{
				effectGameObject.SetActive(false);
			}

			yield return new WaitForSeconds(0.1f);
		}
	}

	void OnDestroy(){
		if (effectGameObject != null)
			Destroy (effectGameObject);
	}

	public bool instantiate(){
		Ray ray = Camera.main.ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0f));
		RaycastHit hit;

		if (isTeleportation) {
			if (Physics.Raycast (ray, out hit) == true && hit.collider != null && hit.collider.gameObject.tag == tagSpot && hit.collider.gameObject.transform.parent != null ) {
				Vector3 instPoint = hit.point;
				heroBeingTeletransported = hit.collider.gameObject.transform.parent.gameObject;
				Hero hero = heroBeingTeletransported.GetComponent<Hero>();
				if( hero != null && !hero.Dying ){
					Instantiate (Util.LoadPrefabResource ("TeletransportEffect") as GameObject, instPoint, Quaternion.identity);
					hero.cancelOnFightRoutine();
					hero.Fighting = false;
					hero.Dying = true;
					hero.waitToDisappear(0.5f);
					isTeleportation = false;
					objectName = Barracks.getNameOfClass( hero.ClassType );
					tagSpot = Barracks.getTag(objectName);
					return true;
				}
			}
			return false;

		}
		else {

			if( objectName == "AttackBlackHole" || objectName == "CureCristal" ){
				Instantiate( Util.LoadPrefabResource(objectName) as GameObject );
				return true;
			}

			if (Physics.Raycast (ray, out hit) == true && hit.collider != null && ((tagSpot == "ANY") ? true : (hit.collider.gameObject.tag == tagSpot))) {
					Vector3 instPoint = hit.point;

					if (objectIsHero ()) {
							Instantiate (Util.LoadPrefabResource ("TeletransportEffect") as GameObject, instPoint, Quaternion.identity);
							GameObject aux;
							if( heroBeingTeletransported == null ){
								aux = Instantiate (Util.LoadPrefabResource (objectName) as GameObject, instPoint, Quaternion.Euler (Vector3.zero)) as GameObject;
								Hero hero = aux.GetComponent<Hero> ();
								hero.disappearAndReapper (0.8f);
							}
							else{
								aux = heroBeingTeletransported;
								aux.transform.position = instPoint;
								Hero hero = aux.GetComponent<Hero>();
								hero.wakeUpFromTeletransport(0.8f);
							}

					} else
							Instantiate (Util.LoadPrefabResource (objectName) as GameObject, instPoint, Quaternion.identity);

					return true;
			}
			return false;
		}
	}
}
