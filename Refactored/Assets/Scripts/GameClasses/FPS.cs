﻿using UnityEngine;
using System.Collections;

public class FPS : MonoBehaviour {

	private float _fps;
	private GUIStyle label_style;


	void Start(){
		StartCoroutine (updateFPS());
		label_style = new GUIStyle ();
		label_style.fontSize = 80;
		label_style.normal.textColor = Color.white;
	}


	void OnGUI(){
		GUI.Label ( new Rect (10, Screen.height*0.9f , 100, 20), "FPS: " + _fps.ToString("0.00"), label_style);
	}

	private IEnumerator updateFPS(){
		while (true) {
			_fps = 1.0f/Time.deltaTime;
			yield return new WaitForSeconds(0.3f);
		}
	}
}
