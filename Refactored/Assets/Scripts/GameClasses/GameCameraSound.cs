using UnityEngine;
using System.Collections;

public class GameCameraSound : MonoBehaviour {
	
	public static bool bossAlive = false;

	public GameObject musicBox;
	public AudioSource music;
	//public static AudioListener listener;
	
	void Start(){

		//listener = gameObject.AddComponent<AudioListener>();

		/*cria um objeto filho para receber o audiosource de musicas*/
		musicBox = new GameObject ("MusicBox");
		musicBox.transform.position = gameObject.transform.position;
			
		/*inicializacao da parte sonora*/
		music = musicBox.AddComponent<AudioSource>();
		music.maxDistance = 100000;

		/*efeito inicial e loop da musica*/
		SoundController.playSoundMusic (music, Resources.Load ("Sounds/lv1Music")as AudioClip);
		music.volume = 0.2F;
	}
	
	void Update(){

//		if (SoundController.isEffectOff() == true) {
//
//			AudioSource[] allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[]; 
//
//			int cont=0;
//			while(cont < allAudioSources.Length){
//				allAudioSources[cont].mute = true;
//				cont++;
//			}
//
//			music.mute = false;
//		}
//
//		SoundController.muteSounds (music,null);
//
//		if (bossAlive == true)
//			SoundController.playSoundMusic(music,Resources.Load ("Sounds/lv1BossMusic")as AudioClip);



		/*MUGUE MONSTRO PARA CORTAR TODOS SONS IN GAME*/

		if (SoundController.isMusicOff () == true || SoundController.isEffectOff () == true)
			AudioListener.volume = 0.0F;
//			(gameObject.GetComponent(typeof(AudioListener)) as AudioListener).enabled = false;
		else
			AudioListener.volume = 1.0F;
//			(gameObject.GetComponent(typeof(AudioListener)) as AudioListener).enabled = true;
		/*migue*/
	
	
	}




	
}