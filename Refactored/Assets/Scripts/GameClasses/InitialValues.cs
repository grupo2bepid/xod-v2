using UnityEngine;
using System.Collections;

public  class InitialValues : MonoBehaviour {

	/* Importante ter em mente os limitantes das propriedades dos herois:
	 * Vida  : 0 ate 100 
	 * Range : ? ate ?
	 * Delay : <Pior_setado*1.5> ate 0
	 * Attk  : 1, 2 e 3 golpe!
	 * */
	public const int FIGHTER_PRICE = 120;
	public const int FIGHTER_LIFE = 20;
	public const float FIGHTER_RANGE = 1f;
	public const int FIGHTER_MAX_ATTACK = 1;
	public const float FIGHTER_ATTACK_DELAY = 0.5f;
	public const int FIGHTER_UPGRADE_RANGE_PRICE = 1;
	public const int FIGHTER_UPGRADE_LIFE_PRICE = 1;
	public const int FIGHTER_UPGRADE_MAX_ATTACK_PRICE = 1;
	public const int FIGHTER_UPGRADE_ATTACK_DELAY_PRICE = 1;
	public const int FIGHTER_COUNT = 2;
	public const string FIGHTER_NAME = "Fighter";

	public const int AXEWARRIOR_PRICE = 100;
	public const int AXEWARRIOR_LIFE = 15;
	public const float AXEWARRIOR_RANGE = 1f;
	public const int AXEWARRIOR_MAX_ATTACK = 2;
	public const float AXEWARRIOR_ATTACK_DELAY = 1.2f;
	public const int AXEWARRIOR_UPGRADE_RANGE_PRICE = 1;
	public const int AXEWARRIOR_UPGRADE_MAX_ATTACK_PRICE = 1;
	public const int AXEWARRIOR_UPGRADE_LIFE_PRICE = 1;
	public const int AXEWARRIOR_UPGRADE_ATTACK_DELAY_PRICE = 1;
	public const int AXEWARRIOR_COUNT = 0;
	public const string AXEWARRIOR_NAME = "AxeWarrior";


	public const int WARHAMMERWARRIOR_PRICE = 110;
	public const int WARHAMMERWARRIOR_LIFE = 15;
	public const float WARHAMMERWARRIOR_RANGE = 1f;
	public const int WARHAMMERWARRIOR_MAX_ATTACK = 1;
	public const float WARHAMMERWARRIOR_ATTACK_DELAY = 1.2f;
	public const int WARHAMMERWARRIOR_UPGRADE_RANGE_PRICE = 1;
	public const int WARHAMMERWARRIOR_UPGRADE_MAX_ATTACK_PRICE = 1;
	public const int WARHAMMERWARRIOR_UPGRADE_LIFE_PRICE = 1;
	public const int WARHAMMERWARRIOR_UPGRADE_ATTACK_DELAY_PRICE = 1;
	public const int WARHAMMERWARRIOR_COUNT = 0;
	public const string WARHAMMERWARRIOR_NAME = "WarhammerWarrior";

	public const int MACEWARRIOR_PRICE = 110;
	public const int MACEWARRIOR_LIFE = 15;
	public const float MACEWARRIOR_RANGE = 2f;
	public const int MACEWARRIOR_MAX_ATTACK = 1;
	public const float MACEWARRIOR_ATTACK_DELAY = 1f;
	public const int MACEWARRIOR_UPGRADE_RANGE_PRICE = 1;
	public const int MACEWARRIOR_UPGRADE_MAX_ATTACK_PRICE = 1;
	public const int MACEWARRIOR_UPGRADE_LIFE_PRICE = 1;
	public const int MACEWARRIOR_UPGRADE_ATTACK_DELAY_PRICE = 1;
	public const int MACEWARRIOR_COUNT = 0;
	public const string MACEWARRIOR_NAME= "MaceWarrior";
	
	public const int GHOUL_BOUNTY = 50;
	public const int GHOUL_LIFE = 14;
	public const int GHOUL_LEVEL = 1;
	public const float GHOUL_ATTACK_DELAY = 0.25f;
	public const string GHOUL_NAME = "Ghoul";

	public const int MUMMY_BOUNTY = 50;
	public const int MUMMY_LIFE = 14;
	public const int MUMMY_LEVEL = 1;
	public const float MUMMY_ATTACK_DELAY = 0f;
	public const string MUMMY_NAME = "Mummy";

	public const int SKELETON_BOUNTY = 70;
	public const int SKELETON_LIFE = 16;
	public const int SKELETON_LEVEL = 2;
	public const float SKELETON_ATTACK_DELAY = 0f;
	public const string SKELETON_NAME = "Skeleton";

	public const int VAMPIRE_BOUNTY = 100;
	public const int VAMPIRE_LIFE = 14;
	public const int VAMPIRE_LEVEL = 3;
	public const float VAMPIRE_ATTACK_DELAY = 0f;
	public const string VAMPIRE_NAME = "Vampire";

	public const int DEMON_BOUNTY = 300;
	public const int DEMON_LIFE = 120;
	public const float DEMON_ATTACK_DELAY = 0f;
	public const string DEMON_NAME = "Demon";

	//Player valores 
	public   const int MONEY = 600;

	//Vida do cristal
	public const int CRISTAL_LIFE = 100;
	
	/* Preco dos itens */
	public   const int ATTACKBLACKHOLE_PRICE 	= 300;
	public   const int ATTACKTHUNDER_PRICE   	= 75;
	public   const int CURECHARACTER_PRICE   	= 40;  
	public   const int CURECRISTAL_PRICE 		= 25;
	public	 const int TELETRANSPORT_PRICE		= 40;
	public	 const int UPGRADE_AGILITY_PRICE	= 50;
	public	 const int UPGRADE_FORCE_PRICE		= 50;
	public	 const int UPGRADE_DEFENSE_PRICE	= 50;

	public const int UPGRADE_ITENS_DURATION = 20;
	public const float SUPER_FORCE_ON_BOSS_MULTIPLIER = 3f;

	public   const int ATTACKBLACKHOLE_COUNT 	= 2;
	public   const int ATTACKTHUNDER_COUNT   	= 0;
	public   const int CURECHARACTER_COUNT   	= 0;  
	public   const int CURECRISTAL_COUNT		= 0;
	public	 const int TELETRANSPORT_COUNT		= 2;
	public	 const int UPGRADE_AGILITY_COUNT	= 0;
	public	 const int UPGRADE_FORCE_COUNT		= 2;
	public	 const int UPGRADE_DEFENSE_COUNT	= 0;

	public const bool ALREADY_VIEWED_TUTORIAL = false;

	public const float MUSIC_OFF  = 0;
	public const float EFFECT_OFF = 0;

	public const string DEFAULT_FONT_NAME = "PrinceValiant";
	
}
