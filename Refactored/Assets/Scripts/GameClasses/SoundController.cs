using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

	public static void playSoundEffect(AudioSource audioPlayer, AudioClip effect){
		//audioPlayer.volume = 1.0F;
		audioPlayer.PlayOneShot (effect);
	}
	
	public static void playSoundMusic(AudioSource audioPlayer, AudioClip music){
		audioPlayer.volume = 0.2F;
		audioPlayer.clip = music;
		audioPlayer.Play();
		audioPlayer.loop = true;
	}
	
	
	public static void muteSounds(AudioSource audioPlayerMusic, AudioSource audioPlayerEffect){
		if(audioPlayerMusic != null)
			audioPlayerMusic.mute = isMusicOff();

		if(audioPlayerEffect != null)
			audioPlayerEffect.mute = isEffectOff();
		
	}


	/**Metodos para controle do estado de som e efeito
	 * do jogo */
	
	public static bool isMusicOff(){
		return Database.isMusicOff ();
	}

	public static bool isEffectOff(){
		return Database.isEffectOff();
	}

	public static void musicIsOff(bool state){
		Database.musicIsOff (state);
	}

	public static void effectIsOff(bool state){
		Database.effectIsOff (state);
	}
}

