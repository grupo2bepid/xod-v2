﻿using UnityEngine;
using System.Collections;

public class Cristal : AugmentedRealityObject {

	private int _life = InitialValues.CRISTAL_LIFE;
	private GameObject cristalDamageEffect;
	private Vector3 effectPosition;
	private int maxLife = InitialValues.CRISTAL_LIFE; //Vida maxima que o cristal deve ter

	bool isDead = false;

	//Usado no HUD para colocar a vida do mesmo
	public int initialLife = InitialValues.CRISTAL_LIFE;

	//sounds
	public static AudioSource effects;

	void Awake(){
		cristalDamageEffect = Util.LoadPrefabResource ("CristalDamageEffect") as GameObject;
		effectPosition = gameObject.transform.position;
		effectPosition.y += 0.8f;
	}

	public int Life{
		get{
			return _life;
		}
		set{
			_life = value;
			if( _life <= 0 ){

				//sounds
				effects = gameObject.AddComponent<AudioSource> ();
				SoundController.playSoundEffect (effects, Resources.Load ("Sounds/crystalExplosionEffect")as AudioClip);

				if(!isDead){
					isDead = true;
					GameController.endPhase(false);
				}
			}
		}
	}

	public void getHit( int value ){
		Instantiate ( cristalDamageEffect, effectPosition, Quaternion.identity );
		Life -= value;
	}

	//Cura a vida do cristal com 15% a mais da vida INICIAL.
	public void cure(){
		if((int)((initialLife * 0.15f) + Life) >= maxLife)
			Life = maxLife;
		else
			Life = (int)(Life + (initialLife * 0.15f));
	}
}
