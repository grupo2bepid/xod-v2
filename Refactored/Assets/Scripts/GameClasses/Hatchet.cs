﻿using UnityEngine;
using System.Collections;

public class Hatchet : MonoBehaviour {

	private float lifeTime = 20f;
	private Vector3 shotDirection;
	private float velocity = 8f;
	private float rotationSpeed = 250f;
	private bool setted = false;
	private GameObject hitEffect;
	private int damage = 2;
	private Vector3 targetPosition;

	public Transform rotationPointTransform;

	public void setTarget( Vector3 tgtPos ){
		targetPosition = tgtPos;
		targetPosition.y += 0.5f;
		shotDirection = (targetPosition - rotationPointTransform.position).normalized;
		setted = true;
	}

	void Awake(){
		hitEffect = Util.LoadPrefabResource ("HatchetHitEffect") as GameObject;
	}

	void Update () {
		lifeTime -= Time.deltaTime;
		if (lifeTime <= 0f)
			Destroy (gameObject);

		if (setted) {
			transform.position += shotDirection * Time.deltaTime * velocity;
		}

		Vector3 rotationPoint = rotationPointTransform.position;
		transform.RotateAround ( rotationPoint,  transform.forward , rotationSpeed * Time.deltaTime );
	}

	void OnTriggerEnter(Collider col){

		Character enemyCharacter = col.gameObject.GetComponent (typeof(Character)) as Character;

		if (enemyCharacter != null && !enemyCharacter.Dying && !( enemyCharacter is Hero )) {
			Vector3 pos = enemyCharacter.gameObject.transform.position;
			pos.y += 1f;
			Instantiate( hitEffect, pos, Quaternion.identity );
			enemyCharacter.sufferDamage(damage);
		}

		Destroy( gameObject );
	}

}
