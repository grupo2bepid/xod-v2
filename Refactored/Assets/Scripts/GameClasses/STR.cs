﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class STR : object {
	


	public enum Resources{
		EMPTY,
		COUP0,
		COUP1,
		COUP2,
		EMPTY_BAR ,
		HP , 
		RANGE_ICON , 
		AGILITY , 
		ATK ,
		SIDEBAR ,
		SIDEBAR_FST , 
		SIDEBAR_LST ,
		COIN_OVER ,
		PRICE_OVER ,
		ROLL , 
		EXIT_BUTTON ,
		UP , 
		UP_PRESSED ,  
		STORE_BUTTON , 
		BARRACKS_BUTTON , 
		SETTINGS_BUTTON ,
		MUSIC_BUTTON , 
		MUSIC_BUTTON_PRESSED , 
		EFFECTS , 
		EFFECTS_PRESSED , 
		HOME , 
		DESERT_LEVEL , 
		MENU_PAUSED_BACK , 
		DESERT_LEVEL_PRESSED , 
		CAPITAL_BUTTON , 
		CAPITAL_BUTTON_PRESSED ,
		HOME_PRESSED ,
		KING ,
		BACK_MENU_ITEM ,
		BACK_MENU_ITEM_PRESSED ,
		END_GAME_BACKGROUND,
		LOAD_BACKGROUND,
		QUANTITY_BACKGROUND ,
		STORE_BACKGROUND ,
		SCROLL ,
		EXIT_BUTTON_PRESSED,
		BUTTON_SUFFIX,
		BUTTON_PRESSED_SUFFIX,
		ICON_SUFFIX_DEAD,
		FIRST_SCREEN_BACKGROUND ,
		CONFIRM_POPUP_BACKGROUND , 
		CONFIRM_BUTTON ,
		DENY_BUTTON , 
		CONFIRM_BUTTON_PRESSED ,
		DENY_BUTTON_PRESSED ,
		ARMY_MENU ,
		ITEM_MENU ,
		LIFE_CRYSTAL_ICON , 
		LIFE_CRYSTAL_BAR , 
		LIFE_BOSS_ICON , 
		LIFE_BOSS_BAR , 
		SOUND_BUTTON , 
		SOUND_BUTTON_PRESSED , 
		EFFECTS_BUTTON , 
		EFFECTS_BUTTON_PRESSED , 
		BACK_MENU_HERO , 
		BACK_MENU_HERO_PRESSED ,
		AIM ,
		ITEM_MENU_PRESSED ,
		ARMY_MENU_PRESSED ,
		BACK_END_GAME_VICTORY,
		BACK_END_GAME_DEFEAT,
		HOW_TO_PLAY_BACKGROUND ,
		IPAD_HOLD_TARGET,
		TABLE_TARGET,
		TARGET,
		TARGET_CINZA,
		IPAD_HOLD_TARGET_AR,
		DOWNLOAD,
		DOWNLOAD_PRESSED


	}

	public enum TranslatedResources{
		NEW_GAME , 
		CONTINUE , 
		HOW_TO_PLAY , 
		CREDITS , 
		NEW_GAME_PRESSED , 
		CONTINUE_PRESSED , 
		HOW_TO_PLAY_PRESSED , 
		CREDITS_PRESSED,
		RECRIUT_BUTTON_PRESSED , 
		RECRUIT_BUTTON , 
		BUY_BUTTON ,
		BUY_BUTTON_PRESSED ,
		QUIT_BATTLE,
		QUIT_BATTLE_PRESSED,
		GO_TO_MAP,
		GO_TO_MAP_PRESSED,
		MAP,
		STORE_BUTTON,
		STORE_BUTTON_PRESSED,
		SETTINGS_BUTTON,
		SETTINGS_BUTTON_PRESSED,
		BARRACKS_BUTTON,
		BARRACKS_BUTTON_PRESSED,
		SKIP,
		SKIP_PRESSED,
		LOADING_UM,
		LOADING_DOIS,
		LOADING_TRES,
	}

	private static Dictionary<Resources, string> resourceDictionary;
	
	private static Dictionary<SystemLanguage, Dictionary<string, string>> dict;
	private static Dictionary<SystemLanguage, Dictionary<TranslatedResources, string>> transResourcesDictionary;

	public static void initiate(){
		dict = new Dictionary<SystemLanguage, Dictionary<string, string>> ();
		
		dict [SystemLanguage.English] = new Dictionary<string, string> ();
		dict [SystemLanguage.Portuguese] = new Dictionary<string, string> ();



		insertIntoPortugueseAndEnglish("tut0", "Toque para visualizar as classes de heróis", "Tap to view the heroes classes");
		insertIntoPortugueseAndEnglish("tut1", "Agora selecione um guerreiro da Classe dos Lutadores", "Now select a hero from the Fighters Class");
		insertIntoPortugueseAndEnglish("tut2", "Para colocá-lo no jogo, escolha um lugar válido na tela e confirme", "To put him in the game, choose a valid place on screen and confirm");
		insertIntoPortugueseAndEnglish("tut3", "O Lutador irá proteger o círculo limitado pelo anel. Agora toque para visualizar seu inventário", "The Fighter will protect the circle limited by the ring. Now tap to see your inventory");
		insertIntoPortugueseAndEnglish("tut4", "Para mudar a região protegida por um herói é necessário utilizar o Teletransporte, selecione este item", "To change de region defended by a hero is necessary to use the Teletransport, select this item");
		insertIntoPortugueseAndEnglish("tut5", "Posicione a mira sobre o lutador e confirme", "Place the aim over the Fighter and confirm");
		insertIntoPortugueseAndEnglish("tut6", "Escolha um ponto na arena para ser o centro da nova região defendida pelo herói", "Select one point in the arena to be the center of the new region defended by the hero");
		insertIntoPortugueseAndEnglish("tut7", "Toque novamente para visualizar seu inventário", "Tap again to view your inventory");
		insertIntoPortugueseAndEnglish("tut8", "Escolha a Super Força", "Select the Super Force");
		insertIntoPortugueseAndEnglish("tut9", "Para ativá-la, posicione a mira em cima do Lutador e confirme", "To activate it, place the aim over the Fighter and confirm");
		insertIntoPortugueseAndEnglish("tut10", "Vamos olhar para um terceiro tipo de item, toque novamente para visualizar seu inventário", "Let's take a look on a third kind of item, tap again to view your inventory");
		insertIntoPortugueseAndEnglish("tut11", "Escolha a Fumaça Mortífera", "Choose the Deadly Smoke");
		insertIntoPortugueseAndEnglish("tut12", "Para ativá-la, não é necessário mirar, basta confirmar", "To activate it, there is no need to aim, just confirm");
		insertIntoPortugueseAndEnglish("tut13", "Excelente, agora prepare-se para a ação!", "Excellent, now get ready for some action!");


		//TODO: Melhorar os textos
		insertIntoPortugueseAndEnglish ("Cap0.0","Outrora, sob a grande cordilheira, existia um reino pacífico de anões","Formerly, under the great mountain range, existed a peaceful kingdom of dwarfs");
		insertIntoPortugueseAndEnglish ("Cap0.1","A mineração de gemas preciosas constituia o cerne desta civilização","The mining of precious gems constituted the core of this civilization");
		insertIntoPortugueseAndEnglish ("Cap0.2","Não é surpresa que o império ascendeu graças à mineração","Not surprisingly, the empire ascended thanks to the mining activity");
		insertIntoPortugueseAndEnglish ("Cap0.3","Escondida pelo o tempo e pelas as rochas, o Cristal Divino foi encontrado","Hidden by time and by the rocks, the Divine Crystal was found");
		insertIntoPortugueseAndEnglish ("Cap0.4","A mística força obtida pelo cristal levou os anões a trocarem a picareta pelo machado","A mystical force obtained by the crystal led the dwarves to exchange the pickaxe for the ax");
		insertIntoPortugueseAndEnglish ("Cap0.5","A ganância os fez expandir suas bordas","Greed led the expansion of their borders");
		insertIntoPortugueseAndEnglish ("Cap0.6","Uma a uma, as raças de nossa terra foram se diluindo, algumas até foram extintas","One by one, the races of our land were diminishing, some were extinct");
		insertIntoPortugueseAndEnglish ("Cap0.7","Para manter o domínio sobre o território, o cristal foi dividido","To maintain their dominance over the territory, the crystal was divided");
		insertIntoPortugueseAndEnglish ("Cap0.8","Suas partes espalhadas estrategicamente pelo império","Its parts were strategically spread throughout the empire");
		insertIntoPortugueseAndEnglish ("Cap0.9","Os anões tornaram-se, então, praticamente invencíveis","The Dwarves became thus practically invincible");
		insertIntoPortugueseAndEnglish ("Cap0.10","Durante anos vivemos uma vida sem qualquer tipo de expressão de ideias","For years we lived a life without any expression of ideas");
		insertIntoPortugueseAndEnglish ("Cap0.11","Apenas tentando sobreviver","Just trying to survive");
		insertIntoPortugueseAndEnglish ("Cap0.12","Hoje, contudo, estou pintando.","Today, however, I am painting.");
		insertIntoPortugueseAndEnglish ("Cap0.13","Hoje, eu tenho esperança.","Today, I have hope.");
		insertIntoPortugueseAndEnglish ("Cap0.14","Nós somos a Coalizão Rebelde!","We are the Rebel Coalition!");
		insertIntoPortugueseAndEnglish ("Cap0.15","Hoje, o Império cai!","Today, the Empire will fall!");

		//CHAPTER 0

		insertIntoPortugueseAndEnglish ("Ch0.0","Filho, preste atenção!","Son, pay attention!");
		insertIntoPortugueseAndEnglish ("Ch0.1","Os conselheiros me informaram de um movimento rebelde se formando no deserto.","My advisors just inform me of a rebel movement forming in the desert.");
		insertIntoPortugueseAndEnglish ("Ch0.2","O pior é que os rebeldes descobriram nossa arma secreta.","The worst is that the rebels have discovered our secret weapon.");
		insertIntoPortugueseAndEnglish ("Ch0.3","Eles pretendem destruir as partes do Cristal Divino espalhadas pelo território.","They plan to destroy all parts of the Divine Crystal scattered throughout the territory.");
		insertIntoPortugueseAndEnglish ("Ch0.4","É de extrema importância a defesa destes locais sagrados.","It is extremely important to defend these holy places.");
		insertIntoPortugueseAndEnglish ("Ch0.5","Eu desconfio de um espião na corte, por isso confiarei o exército a você.","I suspect we have a spy in the court, so I will trust the army to you.");
		insertIntoPortugueseAndEnglish ("Ch0.6","Você vai precisar, entretanto, de treinar novos soldados no quartel.","You will need, however, to train new soldiers in the barracks.");
		insertIntoPortugueseAndEnglish ("Ch0.7","Não se esqueça de, sempre que puder, melhorar o equipamento das tropas.","Do not forget: Whenever you can, improve the equipment of the troops.");
		insertIntoPortugueseAndEnglish ("Ch0.8","Consulte ainda a guilda de artesãos mágicos. Seus produtos são úteis em combate.","Consult also our guild of magic craftsmen. Their products are handy in combat.");
		insertIntoPortugueseAndEnglish ("Ch0.9","Vá, meu filho. Nosso destino está em suas mãos!","Go, my son. Our destiny is in your hands!");

		//TODO: Trocar as chaves para Enum

		// How To Play
		insertIntoPortugueseAndEnglish ("HtP0","Para poder aproveitar o jogo deve-se imprimir um target","To enjoy the game you should print a target");
		insertIntoPortugueseAndEnglish ("HtP1","O jogo também funciona com o target em tons de cinza","The game also works with the target printed in shades of gray");
		insertIntoPortugueseAndEnglish ("HtP2","Após imprimí-lo, posicione-o sobre a mesa","After printing it, place it on the table");
		insertIntoPortugueseAndEnglish ("HtP3","Quando requisitado pelo jogo, mire a câmera do iPad no target","When asked by the game, aim the iPad camera at the target");
		insertIntoPortugueseAndEnglish ("HtP4","O jogo reconhecerá a imagem rapidamente e você estará pronto para jogar","The game will recognize the image quickly and you're ready to play");
		insertIntoPortugueseAndEnglish ("HtP5","Baixe e imprima o target para começar a jogar!","Download and print the target so you can start to play!");

		insertIntoPortugueseAndEnglish("UpgradeDefenseDesc", "Faça com que um herói não sofra dano por 20 s", "Make a hero suffers no damage for 20 s");
		insertIntoPortugueseAndEnglish("UpgradeForceDesc", "Faça com que um herói desfira golpes mortais por 20 s", "Make a hero delivers deadly strikes for 20 s");
		insertIntoPortugueseAndEnglish("UpgradeAgilityDesc", "Faça com que um herói golpeie sem intervalos por 20 s", "Make a hero delivers no delay strikes for 20 s");
		insertIntoPortugueseAndEnglish("CureCristalDesc", "Restaura parte da vida do Crista", "Restores part of the life of Crystal");
		insertIntoPortugueseAndEnglish("CureCharacterDesc", "Recupere a vida inteira de um herói", "Potion able to heal the entire life of a hero");
		insertIntoPortugueseAndEnglish("AttackThunderDesc", "Extermina todos próximos ao relâmpago", "Exterminate all the characters near the lightning");
		insertIntoPortugueseAndEnglish("AttackBlackHoleDesc", "Destroi todos os personagens na arena", "Destroy all the characters in the arena");
		insertIntoPortugueseAndEnglish("TeletransportDesc", "Mude a região coberta por um herói", "Change the region covered by one hero");

		insertIntoPortugueseAndEnglish("UpgradeDefenseTitle", "Super Defesa", "Super Defence");
		insertIntoPortugueseAndEnglish("UpgradeForceTitle", "Super Força", "Super Force");
		insertIntoPortugueseAndEnglish("UpgradeAgilityTitle", "Super Agilidade", "Super Agility");
		insertIntoPortugueseAndEnglish("CureCristalTitle", "Poção Restauradora", "Restore Potion");
		insertIntoPortugueseAndEnglish("CureCharacterTitle", "Poção Revigorante", "Refresh Potion");
		insertIntoPortugueseAndEnglish("AttackThunderTitle", "Trovoada", "Thunder Attack");
		insertIntoPortugueseAndEnglish("AttackBlackHoleTitle", "Fumaça Mortífera", "Deadly Smoke");
		insertIntoPortugueseAndEnglish ("TeletransportTitle", "Teletransporte", "Teletransport");

		insertIntoPortugueseAndEnglish("Fighter", "Lutador", "Fighter");
		insertIntoPortugueseAndEnglish("AxeWarrior", "Degolador de Trolls", "Beheader of Troll");
		insertIntoPortugueseAndEnglish("MaceWarrior", "Caçador de Bestas", "Beasthunter");
		insertIntoPortugueseAndEnglish("WarhammerWarrior", "Dracocida", "Dragonslayer");

		insertIntoPortugueseAndEnglish ("Selecione um Item", "Escolha um Item", "Select one Item");

		//Menu EndGame
		insertIntoPortugueseAndEnglish ("BattleWon", "Batalha Vencida!", "Battle Won!");
		insertIntoPortugueseAndEnglish ("BatleLost", "Batalha Perdida!", "Battle Lost!");
		insertIntoPortugueseAndEnglish ("Reward", "Recompensas", "Reward");
		insertIntoPortugueseAndEnglish ("Casualties", "Baixas", "Casualties");
		insertIntoPortugueseAndEnglish ("InitialMenu", "Menu Inicial", "Initial Menu");


		//Loading

		insertIntoPortugueseAndEnglish ("Loading", "Carregando", "Loading");

		// MenuBegin

		insertIntoPortugueseAndEnglish ("DeleteSavedGame",  "Deletar Jogo Salvo?", "Delete saved game?");
		insertIntoPortugueseAndEnglish ("NewAndDeleteSavedGame",  "Voce tem certeza que deseja iniciar um novo jogo deletando o antigo salvo?", "Are you sure you want to start a new game and delete the saved one?");

		//HUD
		insertIntoPortugueseAndEnglish ("NoTarget",  "Sem Target", "No Target");
		insertIntoPortugueseAndEnglish ("AbandonBattle",  "Deseja Abandonar Batalha?", "Do you wish to abandon battle?");
		insertIntoPortugueseAndEnglish ("ConfirmQuitBattle",  "Voce tem certeza que deseja abandonar a batalha perdendo o progresso na mesma?", "Are you sure you want to abandon battle and lose all the progress done?");
		insertIntoPortugueseAndEnglish ("PAUSED",  "PAUSADO", "PAUSED");


		//CREDIT
//		insertIntoPortugueseAndEnglish ("developers",  "Desenvolvedores", "Developers");
//		insertIntoPortugueseAndEnglish ("developersDis1",  "- Felipe", "- Felipe");
//		insertIntoPortugueseAndEnglish ("developersDis2",  "- Guilherme", "- Guilherme");
//		insertIntoPortugueseAndEnglish ("developersDis3",  "- Gustavo", "- Gustavo");
//		insertIntoPortugueseAndEnglish ("developersDis4",  "- João", "- João");
//
//		insertIntoPortugueseAndEnglish ("sounds","Sons", "Sounds");
//		insertIntoPortugueseAndEnglish ("soundsDis1",  "http://www.freesfx.co.uk", "http://www.freesfx.co.uk");
//		insertIntoPortugueseAndEnglish ("soundsDis2",  "Rafael Alberti - soundcloud.com/rafaelpaste", "Rafael Alberti - soundcloud.com/rafaelpaste");
//
//		insertIntoPortugueseAndEnglish ("target","Textura do Target", "Target Texture");
//		insertIntoPortugueseAndEnglish ("targetDis1","www.cgtextures.com", "www.cgtextures.com");
//
//		insertIntoPortugueseAndEnglish ("art","Arte", "Art");

		insertIntoPortugueseAndEnglish ("creditBackgroundTexture","creditBackgroundTexturePT", "creditBackgroundTextureEN");

		//Menu End Game
		insertIntoPortugueseAndEnglish ("InfoHeaderWon",  "Parabéns, Batalha Vencida!", "Congratulations, Battle Won!");
		insertIntoPortugueseAndEnglish ("InfoHeaderLost",  "Infelizmente, Batalha Perdida!", "Unfortunately, Battle Lost!");
		insertIntoPortugueseAndEnglish ("InfoHeaderHeroDead",  "Heróis Mortos em Batalha", "Heroes Killed in Battle");
		insertIntoPortugueseAndEnglish ("InfoHeaderMoneyEarned",  "Dinheiro Conquistado em Batalha", "Money Earned in Battle");



		initiateResources ();

		initiateTranslatedResources ();

	}
	
	private static void insert(string key, SystemLanguage lg, string value){
		dict [lg] [key] = value;
	}
	
	private static void insertIntoPortugueseAndEnglish(string key, string v1, string v2){
		dict [SystemLanguage.Portuguese] [key] = v1;
		dict [SystemLanguage.English] [key] = v2;	
	}
	
	public static string get(string key){
		if( Application.systemLanguage == SystemLanguage.Portuguese ) return dict [SystemLanguage.Portuguese] [key];
		else return dict [ SystemLanguage.English ] [key];
	}

	private static void insertResource(Resources key, string value){
		resourceDictionary [key] = value;
	}

	private static void insertResource(TranslatedResources key, string v1, string v2){
		transResourcesDictionary [SystemLanguage.Portuguese] [key] = v1;
		transResourcesDictionary [SystemLanguage.English] [key] = v2;
	}

	public static string get(Resources key){
		return resourceDictionary[key];
	}

	public static string get(TranslatedResources key){
		if( Application.systemLanguage == SystemLanguage.Portuguese ) return transResourcesDictionary [SystemLanguage.Portuguese] [key];
		else return transResourcesDictionary [ SystemLanguage.English ] [key];
	}

	private static void initiateTranslatedResources(){
		transResourcesDictionary = new Dictionary<SystemLanguage, Dictionary<TranslatedResources, string>> ();
		
		transResourcesDictionary [SystemLanguage.English] = new Dictionary<TranslatedResources, string> ();
		transResourcesDictionary [SystemLanguage.Portuguese] = new Dictionary<TranslatedResources, string> ();

		insertResource (TranslatedResources.NEW_GAME , "newGamePt", "newGameEn");
		insertResource (TranslatedResources.NEW_GAME_PRESSED , "newGamebPt", "newGamebEn");

		insertResource (TranslatedResources.CONTINUE , "continuePt", "continueEn");
		insertResource (TranslatedResources.CONTINUE_PRESSED , "continuebPt", "continuebEn");

		insertResource (TranslatedResources.HOW_TO_PLAY , "howToPlayPt", "howToPlayEn");
		insertResource (TranslatedResources.HOW_TO_PLAY_PRESSED , "howToPlaybPt", "howToPlaybEn");

		insertResource (TranslatedResources.CREDITS , "creditsPt", "creditsEn");
		insertResource (TranslatedResources.CREDITS_PRESSED , "creditsbPt", "creditsbEn");

		insertResource (TranslatedResources.RECRIUT_BUTTON_PRESSED , "recrutarbPt", "recrutarbEn");
		insertResource (TranslatedResources.RECRUIT_BUTTON , "recrutarPt", "recrutarEn");

		insertResource (TranslatedResources.BUY_BUTTON , "buttoncomprar1_2048x1536Pt", "buttoncomprar1_2048x1536En");
		insertResource (TranslatedResources.BUY_BUTTON_PRESSED , "buttoncomprar1b_2048x1536Pt", "buttoncomprar1b_2048x1536En");

		insertResource (TranslatedResources.QUIT_BATTLE , "backMainButtonPt", "backMainButtonEn");
		insertResource (TranslatedResources.QUIT_BATTLE_PRESSED, "backMainButtonbPt", "backMainButtonbEn");

		insertResource (TranslatedResources.GO_TO_MAP , "returnGameMapPt", "returnGameMapEn");
		insertResource (TranslatedResources.GO_TO_MAP_PRESSED , "returnGameMapbPt", "returnGameMapbEn");

		insertResource (TranslatedResources.SKIP , "pular_a", "skip_a");
		insertResource (TranslatedResources.SKIP_PRESSED , "pular_b", "skip_b");

		insertResource (TranslatedResources.MAP , "mapPt", "mapEn");

		insertResource (TranslatedResources.STORE_BUTTON , "storeButtPt", "storeButtEn");
		insertResource (TranslatedResources.STORE_BUTTON_PRESSED , "storeButtbPt", "storeButtbEn");

		insertResource (TranslatedResources.SETTINGS_BUTTON , "settingPt", "settingEn");
		insertResource (TranslatedResources.SETTINGS_BUTTON_PRESSED , "settingbPt", "settingbEn");

		insertResource (TranslatedResources.BARRACKS_BUTTON , "quartelButtPt", "quartelButtEn");
		insertResource (TranslatedResources.BARRACKS_BUTTON_PRESSED , "quartelButtbPt", "quartelButtbEn");

		insertResource (TranslatedResources.LOADING_UM , "loadingPt", "loadingEn");
		insertResource (TranslatedResources.LOADING_DOIS , "loadingbPt", "loadingbEn");
		insertResource (TranslatedResources.LOADING_TRES , "loadingcPt", "loadingcEn");
	}

	private static void initiateResources(){

		resourceDictionary = new Dictionary<Resources, string> ();

		insertResource (Resources.EMPTY, "");
		insertResource (Resources.COUP0, "golpe0");
		insertResource (Resources.COUP1, "golpe1");
		insertResource (Resources.COUP2, "golpe2");
		insertResource (Resources.EMPTY_BAR , "barra_vazia");
		insertResource (Resources.HP , "hp");
		insertResource (Resources.RANGE_ICON , "rangeIcon");
		insertResource (Resources.AGILITY , "agility");
		insertResource (Resources.ATK , "atk");
		insertResource (Resources.SIDEBAR , "sideBar");
		insertResource (Resources.SIDEBAR_FST , "sideBar_fst");
		insertResource (Resources.SIDEBAR_LST , "sideBar_lst");
		insertResource (Resources.COIN_OVER , "coin2048x1536_over");
		insertResource (Resources.PRICE_OVER , "price_over");
		insertResource (Resources.ROLL , "Roll");
		insertResource (Resources.EXIT_BUTTON , "buttonsair1_2048x1536");
		insertResource (Resources.UP , "up");
		insertResource (Resources.UP_PRESSED , "upb");
		insertResource (Resources.CAPITAL_BUTTON , "kingdomButt");
		insertResource (Resources.STORE_BUTTON , "storeButt");
		insertResource (Resources.BARRACKS_BUTTON , "quartelButt");
		insertResource (Resources.SETTINGS_BUTTON , "setting");
		insertResource (Resources.MUSIC_BUTTON , "music");
		insertResource (Resources.MUSIC_BUTTON_PRESSED , "musicb");
		insertResource (Resources.EFFECTS , "effect");
		insertResource (Resources.EFFECTS_PRESSED , "effectb");
		insertResource (Resources.HOME , "home");
		insertResource (Resources.DESERT_LEVEL , "necroPhase");
		insertResource (Resources.MENU_PAUSED_BACK , "menuPausedBack");
		insertResource (Resources.DESERT_LEVEL_PRESSED , "necroPhaseb");
		insertResource (Resources.CAPITAL_BUTTON , "kingdomButt");
		insertResource (Resources.CAPITAL_BUTTON_PRESSED , "kingdomButtb");
		insertResource (Resources.HOME_PRESSED , "homeb");
		insertResource (Resources.KING , "King");
		insertResource (Resources.BACK_MENU_ITEM , "backMenuItem");
		insertResource (Resources.BACK_MENU_ITEM_PRESSED , "backMenuItemb");
		insertResource (Resources.END_GAME_BACKGROUND , "endGame_background");
		insertResource (Resources.LOAD_BACKGROUND , "loadBackground");
		insertResource (Resources.QUANTITY_BACKGROUND , "quantityBack");
		insertResource (Resources.STORE_BACKGROUND , "background2048x1536");
		insertResource (Resources.SCROLL , "scroll5_2048x1536");
		insertResource (Resources.EXIT_BUTTON_PRESSED , "buttonsair1b_2048x1536");
		insertResource (Resources.BUTTON_SUFFIX , "Butt");
		insertResource (Resources.ICON_SUFFIX_DEAD , "Dead");
		insertResource (Resources.BUTTON_PRESSED_SUFFIX , "Buttb");
		insertResource (Resources.FIRST_SCREEN_BACKGROUND , "firstScreenBack");
		insertResource (Resources.CONFIRM_POPUP_BACKGROUND , "confirmPergaminho");
		insertResource (Resources.CONFIRM_BUTTON , "confirm");
		insertResource (Resources.DENY_BUTTON , "deny");
		insertResource (Resources.CONFIRM_BUTTON_PRESSED , "confirmb");
		insertResource (Resources.DENY_BUTTON_PRESSED , "denyb");
		insertResource (Resources.ARMY_MENU , "armyMenu");
		insertResource (Resources.ITEM_MENU , "itemMenu");
		insertResource (Resources.LIFE_CRYSTAL_ICON , "lifeCristalIcon");
		insertResource (Resources.LIFE_CRYSTAL_BAR , "lifeCristalBar");
		insertResource (Resources.LIFE_BOSS_ICON , "lifeBossIcon");
		insertResource (Resources.LIFE_BOSS_BAR , "lifeBossBar");
		insertResource (Resources.SOUND_BUTTON , "soundButton");
		insertResource (Resources.SOUND_BUTTON_PRESSED , "soundButtonb");
		insertResource (Resources.EFFECTS_BUTTON , "effectButton");
		insertResource (Resources.EFFECTS_BUTTON_PRESSED , "effectButtonb");
		insertResource (Resources.BACK_MENU_HERO , "backMenuHero");
		insertResource (Resources.BACK_MENU_HERO_PRESSED , "backMenuHerob");
		insertResource (Resources.AIM , "aim");
		insertResource (Resources.ITEM_MENU_PRESSED , "itemMenub");
		insertResource (Resources.ARMY_MENU_PRESSED ,"armyMenub" );
		insertResource (Resources.BACK_END_GAME_VICTORY ,"victory" );
		insertResource (Resources.BACK_END_GAME_DEFEAT ,"defeat" );
		insertResource (Resources.HOW_TO_PLAY_BACKGROUND , "HowToPlayBG");
		insertResource (Resources.IPAD_HOLD_TARGET , "IpadHold");
		insertResource (Resources.IPAD_HOLD_TARGET_AR , "IpadHoldAR");
		insertResource (Resources.TABLE_TARGET , "tableTarget");
		insertResource (Resources.TARGET , "target");
		insertResource (Resources.TARGET_CINZA , "targetCinza");
		insertResource (Resources.DOWNLOAD , "down");
		insertResource (Resources.DOWNLOAD_PRESSED , "downb");



	}
	
}
