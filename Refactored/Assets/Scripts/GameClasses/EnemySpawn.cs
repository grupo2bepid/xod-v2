﻿using UnityEngine;
using System.Collections;


using System.Diagnostics;

public class EnemySpawn : MonoBehaviour {
	
	public Transform[] pts;

	public void instantiateCharacter(GameObject prefabRef){

		Stopwatch sw = new Stopwatch();
		sw.Start();

		Instantiate(prefabRef,  pts[ Random.Range(0, pts.Length) ].position  , Quaternion.identity);

		sw.Stop();
		
		print ("TIME TO INSTANTIATEF FOR SPAWN = " + sw.ElapsedMilliseconds);

	}
}
