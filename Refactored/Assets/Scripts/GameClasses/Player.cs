﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeroNode{
	public int quantity;
	public GameObject prefabReference;
	public string name;
	public bool ableToPerformDistanceAttacks;
	
	public HeroNode(int q, GameObject p, bool d, string n){
		ableToPerformDistanceAttacks = d;
		name = n;
		quantity = q;
		prefabReference = p;
	}
}

public static class Player {
	
	public static int money = InitialValues.MONEY;
	
	//Informacoes dos Herois
	public static Dictionary<string, HeroNode> heroes = new Dictionary<string, HeroNode> ();
	public static GameObject AxeWarriorPrefab = (GameObject)Util.LoadPrefabResource("AxeWarrior"), FighterPrefab = (GameObject)Util.LoadPrefabResource("Fighter"), MaceWarriorPrefab = (GameObject)Util.LoadPrefabResource("MaceWarrior"), WarhammerWarriorPrefab = (GameObject)Util.LoadPrefabResource("WarhammerWarrior");
	
	//Informacoes do Itens
	public static Dictionary<string, HeroNode> itens  = new Dictionary<string, HeroNode> ();
	public static GameObject AttackBlackHolePrefab, AttackFirePrefab, AttackThunderPrefab, CureCharacterPrefab, CureWallPrefab, UpgradeDefensePrefab, UpgradeForcePrefab, UpgradeSpeedPrefab;

	static Player(){
		
		//Inicia player com os herois existentes
		heroes [Barracks.getNameOfClass(Barracks.HeroClass.AxeWarrior)] = new HeroNode (0, AxeWarriorPrefab, false, Barracks.getNameOfClass(Barracks.HeroClass.AxeWarrior));
		heroes [Barracks.getNameOfClass(Barracks.HeroClass.Fighter)] = new HeroNode (0, FighterPrefab, false, Barracks.getNameOfClass(Barracks.HeroClass.Fighter));
		heroes [Barracks.getNameOfClass(Barracks.HeroClass.MaceWarrior)] = new HeroNode (0, MaceWarriorPrefab, true, Barracks.getNameOfClass(Barracks.HeroClass.MaceWarrior));
		heroes [Barracks.getNameOfClass(Barracks.HeroClass.WarhammerWarrior)] = new HeroNode (0, WarhammerWarriorPrefab, true, Barracks.getNameOfClass(Barracks.HeroClass.WarhammerWarrior));
		
		//Inicia player com os itens existentes
		itens [Barracks.getNameOfClassItem(Barracks.ItemClass.UpgradeDefense)] = new HeroNode (0, UpgradeDefensePrefab, true, Barracks.getNameOfClassItem(Barracks.ItemClass.UpgradeDefense));
		itens [Barracks.getNameOfClassItem(Barracks.ItemClass.UpgradeForce)] = new HeroNode (0, UpgradeForcePrefab, true, Barracks.getNameOfClassItem(Barracks.ItemClass.UpgradeForce));
		//		itens [Barracks.getNameOfClassItem(Barracks.ItemClass.UpgradeSpeed)] = new HeroNode (0, UpgradeSpeedPrefab, true, Barracks.getNameOfClassItem(Barracks.ItemClass.UpgradeSpeed));
		itens [Barracks.getNameOfClassItem(Barracks.ItemClass.CureCharacter)] = new HeroNode (0, CureCharacterPrefab, true, Barracks.getNameOfClassItem(Barracks.ItemClass.CureCharacter));
		//		itens [Barracks.getNameOfClassItem(Barracks.ItemClass.CureWall)] = new HeroNode (0, CureWallPrefab, true, Barracks.getNameOfClassItem(Barracks.ItemClass.CureWall));
		itens [Barracks.getNameOfClassItem(Barracks.ItemClass.AttackBlackHole)] = new HeroNode (0, AttackBlackHolePrefab, true, Barracks.getNameOfClassItem(Barracks.ItemClass.AttackBlackHole));
		//		itens [Barracks.getNameOfClassItem(Barracks.ItemClass.AttackFire)] = new HeroNode (0, AttackFirePrefab, true, Barracks.getNameOfClassItem(Barracks.ItemClass.AttackFire));
		itens [Barracks.getNameOfClassItem(Barracks.ItemClass.AttackThunder)] = new HeroNode (0, AttackThunderPrefab, true, Barracks.getNameOfClassItem(Barracks.ItemClass.AttackThunder));
		
	}
	public static void addMoney(int q){
		money += q; 
	}

	/* Metodos para cuidar do exercito.*/
	
	public static int getHeroQuantityOfClass(Barracks.HeroClass heroClass){
		return heroes[ Barracks.getNameOfClass(heroClass) ].quantity;
	}
	
	public static int getHeroQuantityOfClass(string heroClass){
		return heroes[ heroClass ].quantity;
	}
	
	public static int getItemQuantityOfClass(Barracks.ItemClass itemClass){
		return itens[ Barracks.getNameOfClassItem(itemClass) ].quantity;
	}
	
	public static List<HeroNode> getHeroesRange(){
		List<HeroNode> l = new List<HeroNode> ();
		foreach (KeyValuePair<string, HeroNode> p in heroes) {
			if( p.Value.ableToPerformDistanceAttacks && p.Value.quantity > 0 ) l .Add(p.Value);
		}
		return l;
	}
	
	public static List<HeroNode> getHeroesSword(){
		List<HeroNode> l = new List<HeroNode> ();
		foreach (KeyValuePair<string, HeroNode> p in heroes) {
			if( !p.Value.ableToPerformDistanceAttacks && p.Value.quantity > 0) l .Add(p.Value);
		}
		return l;
	}
	
	public static void addHeroOfClass(Barracks.HeroClass heroClass, int q=1){
		HeroNode n = heroes [Barracks.getNameOfClass (heroClass)];
		if (q * (int)Barracks.getValueOfAttribute (Barracks.Attributes.Price, heroClass) <= money) {
			n.quantity += q;
			Debug.Log(""+q + " "+ n.quantity);
			money -= q * (int)Barracks.getValueOfAttribute (Barracks.Attributes.Price, heroClass);
		}
	}
	
	
	public static void addHeroOfClass(string heroClass, int q=1){
		HeroNode n = heroes [heroClass];
		n.quantity += q;	
	}
	
	public static void removeHeroOfClass(Barracks.HeroClass heroClass, int q=1){
		HeroNode n = heroes [Barracks.getNameOfClass (heroClass)];
		n.quantity -= q;
	}
	
	public static void removeHeroOfClass(string heroClass, int q=1){
		HeroNode n = heroes [heroClass];
		n.quantity -= q;
	}
	
	
	/* Metodos para cuidar dos itens.*/
	
	public static List<HeroNode> getItensCure(){
		List<HeroNode> l = new List<HeroNode> ();
		foreach (KeyValuePair<string, HeroNode> p in itens) {
			if( p.Value.name.Contains("Cure") && p.Value.quantity > 0 ) l .Add(p.Value);
		}
		return l;
	}
	
	public static List<HeroNode> getItensAttack(){
		List<HeroNode> l = new List<HeroNode> ();
		foreach (KeyValuePair<string, HeroNode> p in itens) {
			if( p.Value.name.Contains("Attack") && p.Value.quantity > 0) l .Add(p.Value);
		}
		return l;
	}
	
	public static List<HeroNode> getItensUpgrade(){
		List<HeroNode> l = new List<HeroNode> ();
		foreach (KeyValuePair<string, HeroNode> p in itens) {
			if( p.Value.name.Contains("Upgrade") && p.Value.quantity > 0) l .Add(p.Value);
			
		}
		return l;
	}
	
	public static void addItemOfClass(Barracks.ItemClass itemClass, int q=1){
		
		HeroNode n = itens [Barracks.getNameOfClassItem (itemClass)];
		
		if (q * Barracks.getPriceOfClassItem (itemClass) <= money) {
			n.quantity += q;
			//Debug.Log(""+q + " "+ n.quantity);
			money -= q * Barracks.getPriceOfClassItem (itemClass);
		}
	}
	
	public static void addItemOfClassTESTE(Barracks.ItemClass itemClass, int q=1){
		HeroNode n = itens [Barracks.getNameOfClassItem (itemClass)];
		//f (q * Barracks.getPriceOfClassItem (itemClass) <= money) {
		
		n.quantity += q;
		//Debug.Log(""+q + " "+ n.quantity);
		//	money -= q * Barracks.getPriceOfClassItem (itemClass);
		//}
	}
	
	public static void addItemOfClass(string itemClass, int q=1){
		HeroNode n = itens [itemClass];
		n.quantity += q;	
	}
	
	public static void removeItemOfClass(Barracks.ItemClass itemClass, int q=1){
		HeroNode n = itens [Barracks.getNameOfClassItem (itemClass)];
		
		n.quantity -= q;
	}
	
	public static void removeItemOfClass(string itemClass, int q=1){
		HeroNode n = itens [itemClass];
		n.quantity -= q;
	}
	
	
	
	
	
	
}
