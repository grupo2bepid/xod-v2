﻿using UnityEngine;
using System.Collections;

public static class HashIDs : object {
	public static int attack1Trigger;
	public static int attack2Trigger;
	public static int attack3Trigger;
	public static int throwHatchetTrigger;
	public static int dieTrigger;
	public static int walkBool;
	public static int runBool;
	public static int speedFloat;
	public static int forceIdleTrigger;
	public static int roarTrigger;
	public static int flyTrigger;
	public static int[]  attacksIDs;

	public static void fillHashIDs(){
		attack1Trigger = Animator.StringToHash ("attack1");
		attack2Trigger = Animator.StringToHash ("attack2");
		attack3Trigger = Animator.StringToHash ("attack3");

		attacksIDs = new int[5];
		attacksIDs [1] = attack1Trigger;
		attacksIDs [2] = attack2Trigger;
		attacksIDs [3] = attack3Trigger;

		throwHatchetTrigger = Animator.StringToHash ("throwHatchet");
		dieTrigger = Animator.StringToHash ("die");
		walkBool = Animator.StringToHash ("walk");
		runBool = Animator.StringToHash ("run");
		speedFloat = Animator.StringToHash ("speed");
		forceIdleTrigger = Animator.StringToHash ("forceIdle");
		roarTrigger = Animator.StringToHash ("roar");
		flyTrigger = Animator.StringToHash ("fly");
	}
}
