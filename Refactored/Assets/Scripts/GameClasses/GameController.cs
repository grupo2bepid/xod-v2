﻿using UnityEngine;
using System.Collections;

public static class GameController : object {
	public delegate void PauseResumeAction();
	public static event PauseResumeAction onPause;
	public static event PauseResumeAction onResume;
	
	public static int bountyEarnedOnPhase;
	public static int heroesKilledOnCurrentPhase = 0;
	public static Cristal cristal;
	
	public static HUD_refactored hudCurrente;
	
	//Primeira vez que executa um novo jgo
	public static bool firstPlay;
	
	//Primeira vez que executa um novo jogo no DEVICE
	public static bool firstRunDevice;
	
	public enum GameStateEnum{
		Undefined,
		Menu,
		Paused,
		Play,
		OnTutorial,
		GameOver
	}
	
	private static GameStateEnum _state = GameStateEnum.Menu; //Comeca jogo no meu
	public static GameStateEnum State{
		get{
			return _state;
		}
		set{
			if(value != _state){
				if(value == GameStateEnum.Paused && (_state == GameStateEnum.Play || _state == GameStateEnum.Menu )) //Se quer trocar para paused, so é valido quando nao esta no play
					_state = value;
				else if(value == GameStateEnum.Menu || value == GameStateEnum.Play) //Se quer setar outro q nao eh paused, vai direto, sem restricao
					_state = value;
				
				if(_state == GameStateEnum.Paused && onPause != null){
					Time.timeScale = 0;
					onPause();
				}
				else if (_state == GameStateEnum.Play && onResume != null){
					Time.timeScale = 1;
					onResume();
				}
				else if (_state == GameStateEnum.Menu && onPause != null){
					Time.timeScale = 1;
				}
			}
		}
	}
	
	
	private static int enemiesKilledOnCurrentPhase;
	private static int[] heroesKilledByType = new int[100]; 
	private static int totalNumberOfEnemiesOnCurrentPhase;
	private static Phase currentPhase;
	public static float phaseHeight = 1.6f;
	
	static public void startNewFase(int num, Phase phase, Cristal cr, float height){
		currentPhase = phase;
		cristal = cr;
		totalNumberOfEnemiesOnCurrentPhase = num;
		bountyEarnedOnPhase = 0;
		enemiesKilledOnCurrentPhase = 0;
		phaseHeight = height;
		for (int i=0; i < 100; ++i)
			heroesKilledByType [i] = 0;
	}
	
	public static void endPhase(bool won){

		if (won) Database.increaseByOneNumberOfVictories ();

		MenuEndGame.wonGame = won;
		MenuEndGame.heroDeadQuantity = heroesKilledByType;
		MenuEndGame.earnedMoneyValue = bountyEarnedOnPhase;
		
		/*Persiste informacao de heroi*/
		for(int i =0; i < 4; i++){
			//Pega heroi equivalente
			Barracks.HeroClass cls = (Barracks.HeroClass)((int)Barracks.HeroClass.Fighter + i);
			
			//Calcula quantidade que restou
			int newQuantity = Barracks.getHeroCount(cls);
			newQuantity -= heroesKilledByType[i];
			
			//Salva novos dados no dicionario
			Barracks.updateHeroCount(cls, (float)newQuantity);
		}
		
		/*Periste informacao de item*/
		for(int i =0; i < hudCurrente.itemMenu.subButton.Length; i++){
			
			//Pega heroi equivalente
			Barracks.ItemClass item = (Barracks.ItemClass)((int)Barracks.ItemClass.UpgradeDefense + i);
			
			//Salva itens que sobraram
			Barracks.updateItemCount(item, hudCurrente.itemMenu.subButton[i].count);
		}
		
		/*Salva novo dinheiro*/
		Database.updatePlayerMoney ((float)(Database.getPlayerMoney() + bountyEarnedOnPhase));
		
		/*Salva novo status no arquivo*/
		Database.writeDictionaryOnDisk ();
		
		GameController.State = GameController.GameStateEnum.Menu;

		hudCurrente.callEndScene = true; //Avisa hud que é pra chamar a endScene
	}


	static public void registerEnemyDeath( int bounty ){
		bountyEarnedOnPhase += bounty;
		++enemiesKilledOnCurrentPhase;
		
		if (enemiesKilledOnCurrentPhase == totalNumberOfEnemiesOnCurrentPhase) {
			currentPhase.invokeBoss();
		}
	}
	
	static public void registerHeroDeath(Barracks.HeroClass cls){
		++heroesKilledByType [(int)cls];
	}
	
	
}
