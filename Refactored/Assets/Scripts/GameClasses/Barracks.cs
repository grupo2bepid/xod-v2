using UnityEngine;
using System.Collections;

public class Barracks : Object {
	
	// HeroClass possui o mesmo index da ordem pre selecionada no array
	public enum HeroClass
	{
		Fighter = 0,
		AxeWarrior,
		MaceWarrior,
		WarhammerWarrior,
		Quantity
	};
	
	public enum Attributes
	{
		Life = 0,
		Range,
		MaxAttack,
		AttackDelay,
		Price,     
		Quantity
	};
	
	public static string getNameOfAttribute(Attributes att){
		switch (att) {
			case Attributes.AttackDelay: return "ATTACK_DELAY";
			case Attributes.Life: return "LIFE";
			case Attributes.MaxAttack: return "MAX_ATTACK";
			case Attributes.Range: return "RANGE";
			default: return "";
		}
	}

	public static HeroClass getHeroClass(string hero){
		switch (hero) {
			case "Fighter": return HeroClass.Fighter;
			case "AxeWarrior": return HeroClass.AxeWarrior;
			case "MaceWarrior": return HeroClass.MaceWarrior;
			case "WarhammerWarrior": return HeroClass.WarhammerWarrior;
			default: return HeroClass.Quantity; //Error!
		}

	}

	public static string getNameOfClass(HeroClass heroClass){
		switch (heroClass) {
			case HeroClass.Fighter: return "Fighter";
			case HeroClass.AxeWarrior: return "AxeWarrior";
			case HeroClass.MaceWarrior: return "MaceWarrior";
			case HeroClass.WarhammerWarrior: return "WarhammerWarrior";
			default: return "";
		}
	}

	
	public static string getTag(string className){
		switch (className) {
			case "Fighter": return "CollidersPath";
			case "AxeWarrior": return "CollidersPath";
			case "MaceWarrior": return "CollidersPath";
			case "WarhammerWarrior": return "CollidersPath";

			case "AttackBlackHole": return "ANY";
			case "AttackThunder": return "ANY";
			case "CureCristal": return "ANY";

			case "CureCharacter": return "ApplicableItemArea";
			case "UpgradeForce": return "ApplicableItemArea";
			case "UpgradeAgility": return "ApplicableItemArea";
			case "UpgradeDefense": return "ApplicableItemArea";
			case "Teletransport": return "ApplicableItemArea";

			default: return "";
		}
	}

	public static int getValueToBuyHero(HeroClass cls){
		switch (cls) {
		case HeroClass.Fighter: return InitialValues.FIGHTER_PRICE;
		case HeroClass.AxeWarrior: return InitialValues.AXEWARRIOR_PRICE;
		case HeroClass.MaceWarrior: return InitialValues.MACEWARRIOR_PRICE;
		case HeroClass.WarhammerWarrior: return InitialValues.WARHAMMERWARRIOR_PRICE;
		default: return 0;
		}
		
	}


	public static float getValueOfAttribute(Attributes att, HeroClass heroClass){
		return Database.getValueOfAttribute ( getNameOfClass(heroClass), getNameOfAttribute(att) );
	}

	public static void updateValueOfAttribute(Attributes att, HeroClass heroClass, float val){
		Database.updateValueOfAttribute ( getNameOfClass(heroClass), getNameOfAttribute(att), val );
	}

	/* Parte do código usada para cuidar dos itens do  jogo.
	 * Pode ser mudado para outra classe, nao em "Barracks" */
	
	public enum ItemClass
	{	
		UpgradeDefense = 0,
		UpgradeForce, 
		UpgradeAgility,

		CureCristal,
		CureCharacter,
		 
		AttackThunder,
		AttackBlackHole,
		Teletransport,
		None		
	
	};

	public static string getNameOfClassItem(ItemClass itemClass){
		switch (itemClass) {
			case ItemClass.UpgradeDefense: return "UpgradeDefense";
			case ItemClass.UpgradeForce: return "UpgradeForce";
			case ItemClass.UpgradeAgility: return "UpgradeAgility";
			case ItemClass.CureCristal: return "CureCristal";
			case ItemClass.CureCharacter: return "CureCharacter";
			case ItemClass.AttackThunder: return "AttackThunder";
			case ItemClass.AttackBlackHole: return "AttackBlackHole";
			case ItemClass.Teletransport: return "Teletransport";
			default: return "";
		}	
	}

	public static ItemClass getItemClass(string item){
		switch (item) {
		case "UpgradeDefense": return ItemClass.UpgradeDefense;
		case "UpgradeForce": return ItemClass.UpgradeForce;
		case "UpgradeAgility": return ItemClass.UpgradeAgility;
		case "CureCristal": return ItemClass.CureCristal;
		case "CureCharacter": return ItemClass.CureCharacter;
		case "AttackThunder": return ItemClass.AttackThunder;
		case "AttackBlackHole": return ItemClass.AttackBlackHole;
		case "Teletransport": return ItemClass.Teletransport;

		default: return ItemClass.None; //Error!
		}
		
	}

	public static string getShowableItemName( ItemClass itemClass ){
		return STR.get( getNameOfClassItem(itemClass) + "Title" );
	}

//	public static GroupItem getGroupOfItem(ItemClass item){
//		if (item == ItemClass.CureCharacter || item == ItemClass.CureCristal)
//						return GroupItem.Cure;
//		else if(item == ItemClass.AttackBlackHole || item == ItemClass.Teletransport || item == ItemClass.AttackThunder)
//			return GroupItem.Special;
//		else
//			return GroupItem.Upgrade;
//	}


	public static int getPriceOfClassItem(ItemClass itemClass){
		return (int)Database.getItemPrice( getNameOfClassItem(itemClass) );
	}

	public static void updatePriceOfClassItem(ItemClass itemClass, int value){
		Database.updateItemPrice( getNameOfClassItem(itemClass), value );
	}

	public static int getItemCount(ItemClass itemClass){
		return (int)Database.getItemCount( getNameOfClassItem(itemClass) );
	}
	
	public static void updateItemCount(ItemClass itemClass, int value){
		Database.updateItemCount( getNameOfClassItem(itemClass), (float)value );
	}

	public static int getHeroCount(HeroClass heroClass){
		return (int)Database.getHeroCount( getNameOfClass(heroClass) );
	}

	public static void updateHeroCount(HeroClass heroClass, float value){
		Database.updateHeroCount( getNameOfClass(heroClass), value );
	}

	public static int getValueToUpgradeAttribute(HeroClass cls, Attributes att){
		return (int)Database.getValueToUpgradeAttribute ( getNameOfClass(cls), getNameOfAttribute(att) );
	}

	public static void updateValueToUpgradeAttribute(HeroClass cls, Attributes att, int val){
		Database.updateValueToUpgradeAttribute ( getNameOfClass(cls), getNameOfAttribute(att), (float)val );
	}
	
	public static string getDescriptionOfClassItem(ItemClass itemClass){
		return STR.get( getNameOfClassItem(itemClass) + "Desc" );
	}

}