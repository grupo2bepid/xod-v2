﻿using UnityEngine;
using System.Collections;

public class HeroBase : AugmentedRealityObject {
	
	private float radius = 2.0f;
	private int numSegments = 256;
	private LineRenderer lineRenderer;

	public Color c1 = Color.red;
	private float _lifeFraction = 1f;

	public void setTargetAndRadius( Vector3 basePos, float r){
		float y = basePos.y;
		y += 0.15f;

		radius = r;
		transform.position = basePos;
		lineRenderer = gameObject.GetComponent ("LineRenderer") as LineRenderer;
		lineRenderer.material = new Material ( Shader.Find("Particles/Additive") );
		lineRenderer.SetColors (c1,c1);
		lineRenderer.SetWidth (0.1f,0.1f);
		lineRenderer.SetVertexCount ( numSegments + 1 );
		
		float deltaTheta = (2.0f * Mathf.PI) / numSegments;
		float theta = 0f;

		for (int i = 0; i <= numSegments; ++i) {
			float x = radius*Mathf.Cos(theta) + transform.position.x;
			float z = radius*Mathf.Sin(theta) + transform.position.z;
			Vector3 pos = new Vector3(x,y,z);
			lineRenderer.SetPosition(i, pos);
			theta += deltaTheta;
		}
	}


	public float LifeFraction{
		get{
			return _lifeFraction;
		}
		set{
			if( value != _lifeFraction ){
				_lifeFraction = value;
				c1.g =_lifeFraction;
				c1.r = 1f - c1.g;
				lineRenderer.SetColors(c1,c1);
			}
		}
	}

}
