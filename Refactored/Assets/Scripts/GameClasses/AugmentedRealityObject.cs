﻿using UnityEngine;
using System.Collections;

public class AugmentedRealityObject : MonoBehaviour {

	protected float distance2D( Vector3 v1, Vector3 v2 ){
		float dx = v1.x - v2.x;
		float dz = v1.z - v2.z;
		return Mathf.Sqrt ( dx*dx + dz*dz );
	}
	
	protected float sqrDistance2D( Vector3 v1, Vector3 v2 ){
		float dx = v1.x - v2.x;
		float dz = v1.z - v2.z;
		return ( dx*dx + dz*dz );
	}

	public void pause(){
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		
		foreach (Renderer component in rendererComponents)
			component.enabled = false;
		
		foreach (Collider component in colliderComponents)
			component.enabled = false;
	}
	
	public void resume(){
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		
		foreach (Renderer component in rendererComponents)
			component.enabled = true;
		
		foreach (Collider component in colliderComponents)
			component.enabled = true;
	}
	
	protected void OnEnable(){
		GameController.onPause += pause;
		GameController.onResume += resume;
	}
	
	protected void OnDisable(){
		GameController.onPause -= pause;
		GameController.onResume -= resume;
	}
}
