﻿using UnityEngine;
using System.Collections;

public class FirstScreen : MonoBehaviour {

	private bool canContinue;

	//sounds
	public AudioSource music;

	void Awake(){
		HashIDs.fillHashIDs ();
		STR.initiate ();
		canContinue = Database.loadInitialValuesAndCheckDatabaseExistence ();

		//sounds
		/*inicializacao da parte sonora*/
		if (SoundController.isMusicOff() == false) {
			music = gameObject.AddComponent<AudioSource> ();
			SoundController.playSoundEffect (music, Resources.Load ("Sounds/firstSceneMusic")as AudioClip);
		}
	}

	float min(float x, float y){
		if (x < y)
			return x;
		return y;
	}



	void OnGUI(){

		float quadSide = min (Screen.width, Screen.height) * 0.3f;

		Rect r1 = new Rect ( Screen.width*0.5f - quadSide*0.5f,  Screen.height*0.3f - quadSide*0.5f, quadSide, quadSide );
		Rect r2 = new Rect ( Screen.width*0.5f - quadSide*0.5f,  Screen.height*0.3f + quadSide*0.7f, quadSide, quadSide );
		Rect r3 = new Rect ( Screen.width*0.5f - quadSide*0.5f,  Screen.height*0.3f + quadSide*1.7f, quadSide, quadSide );
		Rect r4 = new Rect ( Screen.width*0.25f - quadSide*0.5f,  Screen.height*0.3f + quadSide*1.7f, quadSide, quadSide );


		if (GUI.Button (r1, "New Game")) {
			Database.createNewDatabase();
			GameController.firstPlay = true;
			Application.LoadLevel("MainMenu");
		}
		else if ( canContinue && GUI.Button (r2, "Continue")) {
			Database.loadExistentDatabase();
			GameController.firstPlay = false;
			Application.LoadLevel("MainMenu");
		}
		else if (GUI.Button (r3, "Debug ModeLv1-1.1")) {
			Database.createNewDatabase();
			GameController.firstPlay = false;
			Application.LoadLevel("Lv1-1.1");
		}

		else if (GUI.Button (r4, "Debug Mode")) {
			Database.createNewDatabase();
			GameController.firstPlay = false;
			Application.LoadLevel("Intro");
		}

	}

}
