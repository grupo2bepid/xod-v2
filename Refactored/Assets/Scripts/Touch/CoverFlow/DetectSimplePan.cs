﻿using TouchScript.Gestures;
using TouchScript.Gestures.Simple;
using UnityEngine;
	public class DetectSimplePan : MonoBehaviour
	{
		public float Threshold = 0.1f;
		public float dragDistance = 2.5f;

		public void OnEnable()
		{
			GetComponent<SimplePanGesture>().StateChanged += HandleSimplePanStateChanged;
		}
		public void OnDisable()
		{
			GetComponent<SimplePanGesture>().StateChanged -= HandleSimplePanStateChanged;
		}
		private void HandleSimplePanStateChanged(object sender, GestureStateChangeEventArgs e)
		{
			SimplePanGesture target = sender as SimplePanGesture;
			switch (e.State)
			{
				case Gesture.GestureState.Began:
				case Gesture.GestureState.Changed:
					if (target.LocalDeltaPosition != Vector3.zero){
						FlowView.Instance.Flow(target.LocalDeltaPosition.x / dragDistance);
					}

					break;
				case Gesture.GestureState.Ended:
					float velocity = (target.LocalDeltaPosition.x / dragDistance) * 0.5f;
					if (Mathf.Abs(velocity) > Threshold)
						FlowView.Instance.Inertia(velocity);
					else
						FlowView.Instance.Flow();
					break;
			}
		}
	}
