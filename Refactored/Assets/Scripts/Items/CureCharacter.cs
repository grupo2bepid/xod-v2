﻿using UnityEngine;
using System.Collections;

public class CureCharacter : Item {

	private Hero hero;
	
	void Start(){
		hero = findClosestHero ();
		if (hero != null) {
			GameObject effect = Util.LoadPrefabResource ("CureCharacterEffect") as GameObject;
			hero.heal ();
			Instantiate (effect, hero.gameObject.transform.position, hero.gameObject.transform.rotation);
		}
	}

}