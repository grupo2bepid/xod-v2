﻿using UnityEngine;
using System.Collections;

public class ItemEffect : AugmentedRealityObject {

	public float lifeTime = (float)InitialValues.UPGRADE_ITENS_DURATION;
	private Vector3 offset;
	private Transform followedTransform;
	private bool setted = false;
	private Hero hero;

	public void follow(Transform t, Vector3 ofs ){
		followedTransform = t;
		hero = followedTransform.gameObject.GetComponent (typeof(Hero)) as Hero;
		offset = ofs;
		setted = true;
	}

	void Update(){
		if (setted) {
			if (hero == null || hero.Dying || lifeTime <= 0f)
				Destroy ( gameObject );

			gameObject.SetActive( hero.gameObject.activeSelf );

			if ( gameObject.activeSelf ){
				lifeTime -= Time.deltaTime;
				gameObject.transform.position = followedTransform.position + offset;
			}
		}
	}
}
