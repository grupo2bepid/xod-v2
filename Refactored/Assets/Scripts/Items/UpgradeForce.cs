﻿using UnityEngine;
using System.Collections;

public class UpgradeForce : Item {

	private Hero hero = null;
	
	void Start(){
		hero = findClosestHero ();
		if (hero != null) {
			hero.giveSuperForce();
		}
	}

}
