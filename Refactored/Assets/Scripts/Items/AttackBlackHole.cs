﻿using UnityEngine;
using System.Collections;

public class AttackBlackHole : Item {

	void Start(){
		GameObject effect = Util.LoadPrefabResource ("AttackBlackHoleEffect") as GameObject;
		Vector3 insPos = new Vector3 (0.64f, 3.46f, -5f);
		Instantiate (effect, insPos, Quaternion.identity);

		Character[] v = (Character[])FindObjectsOfType ( typeof(Character) );
		foreach (Character c in v) {
			if( c.gameObject.activeInHierarchy && !(c is Boss) && !c.Dying){
				c.dieForced();
			}
		}
	}
}
