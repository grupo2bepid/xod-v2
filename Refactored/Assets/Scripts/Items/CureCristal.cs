﻿using UnityEngine;
using System.Collections;

public class CureCristal : Item {

	void Start () {
		Cristal cristal = FindObjectOfType (typeof(Cristal)) as Cristal;
		if (cristal != null) {
			cristal.cure();
			GameObject effect = Util.LoadPrefabResource("CureCristalEffect") as GameObject;
			Instantiate( effect, cristal.gameObject.transform.position, cristal.gameObject.transform.rotation );
		}
	}
}
