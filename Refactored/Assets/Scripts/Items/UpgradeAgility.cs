﻿using UnityEngine;
using System.Collections;

public class UpgradeAgility : Item {

	private Hero hero = null;
	
	void Start(){
		hero = findClosestHero ();
		if (hero != null) {
			hero.giveSuperAgility();
		}
	}

}
