﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttackThunder : Item {

	private List<Antihero> getReachableAntiheroes(){
		Vector3 pos = transform.position;
		List<Antihero> reachable = new List<Antihero>();
		
		Antihero[] antiheroes = (Antihero[])FindObjectsOfType (typeof(Antihero));
		if (antiheroes != null && antiheroes.Length > 0) {
			for(int i = 0; i < antiheroes.Length; ++i){
				if( sqrDistance2D(pos, antiheroes[i].transform.position) <= 5.0f && !antiheroes[i].Dying ){
					reachable.Add( antiheroes[i] );
				}
			}
		}
		return reachable;
	}

	void Start () {
		GameObject effect = Util.LoadPrefabResource ("AttackThunderEffect") as GameObject;
		Vector3 pos = transform.position;
		pos.y = GameController.phaseHeight + 1.72f;
		Instantiate (effect, pos, Quaternion.identity);

		List<Antihero> reachable = getReachableAntiheroes ();
		foreach(Antihero antihero in reachable){
			if( antihero is Boss ){
				if( antihero is Demon ){
					Demon demon = antihero as Demon;
					if( demon != null && !demon.isDemonFlying() ){
						demon.sufferDamage( (int)(demon.Life*0.1f) + 1  );
					}
				}
			}
			else antihero.dieForced();
		}
	}

}
