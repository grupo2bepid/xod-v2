﻿using UnityEngine;
using System.Collections;

public class UpgradeDefense : Item {

	private Hero hero = null;

	void Start(){
		hero = findClosestHero ();
		if (hero != null) {
			hero.giveSuperDefense();
		}
	}

}
