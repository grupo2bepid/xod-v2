﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public static class Database : object {
	
	private static string fileName;
	private static SortedDictionary<string, float> dic;


	static public bool loadInitialValuesAndCheckDatabaseExistence(){
		fillDictionaryWithInitialValues ();

		fileName = Application.persistentDataPath +  "/database.txt" ;

		return File.Exists (fileName);
	}

	static bool checkDatabaseExistence(){
		fileName = Application.persistentDataPath +  "/database.txt" ;
		
		return File.Exists (fileName);
	}

	static public void createNewDatabase(){
		if(!checkDatabaseExistence())
			writeDictionaryOnDisk ();
		else{ //Caso exista, pega antigo dados para mater preferencias e tutorial visto
			updateDictionary();

			//Apenas inicializa os dados de jogo (sem visto tutorial e preferencias de audio)
			fillDictionaryNoPreferences();
		}
	}

	static public void loadExistentDatabase(){
		updateDictionary ();
	}

	static void updateDictionary(){
		using (StreamReader reader = new StreamReader(fileName))
		{
			foreach (string key in dic.Keys) {
				string line = reader.ReadLine();
				if( line == null ) break;
				double val;
				Double.TryParse(line, out val);
				dic[key] = (float)val;
			}
		}
	}

	public static void writeDictionaryOnDisk(){
		using (StreamWriter writer = new StreamWriter(fileName))
		{
			foreach (float val in dic.Values) {
				writer.WriteLine(val);
			}
		}
	}

	private static string getKey( Barracks.HeroClass cls, Barracks.Attributes att, bool upgradePrice=false ){
		string key;
		if( upgradePrice ) key = (Barracks.getNameOfClass (cls) + "_UPGRADE_" + Barracks.getNameOfAttribute (att) + "_PRICE").ToUpper ();
		else key = (Barracks.getNameOfClass (cls) + '_' + Barracks.getNameOfAttribute (att)).ToUpper ();
		return key;
	}

	private static string getKey( Barracks.ItemClass item, bool count=false ){
		string key;
		if( count )
			key = (Barracks.getNameOfClassItem(item) + "_COUNT" ).ToUpper ();
		else
			key = (Barracks.getNameOfClassItem(item) + "_PRICE" ).ToUpper ();
		return key;
	}

	private static string getKey( Barracks.HeroClass cls ){
		string key = (Barracks.getNameOfClass (cls) + "_COUNT").ToUpper ();
		return key;
	}

	static void fillDictionaryWithInitialValues(){
		dic = new SortedDictionary<string, float> ();

		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.Life)] = InitialValues.FIGHTER_LIFE;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.Range)] = InitialValues.FIGHTER_RANGE;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.MaxAttack)] = InitialValues.FIGHTER_MAX_ATTACK;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.AttackDelay)] = InitialValues.FIGHTER_ATTACK_DELAY ;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.Life, true)] = InitialValues.FIGHTER_UPGRADE_LIFE_PRICE;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.Range,true)] = InitialValues.FIGHTER_UPGRADE_RANGE_PRICE;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.MaxAttack,true)] = InitialValues.FIGHTER_UPGRADE_MAX_ATTACK_PRICE;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.AttackDelay,true)] = InitialValues.FIGHTER_UPGRADE_ATTACK_DELAY_PRICE ;
		dic [getKey(Barracks.HeroClass.Fighter)] = InitialValues.FIGHTER_COUNT ;

		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.Life)] = InitialValues.WARHAMMERWARRIOR_LIFE;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.Range)] = InitialValues.WARHAMMERWARRIOR_RANGE;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.MaxAttack)] = InitialValues.WARHAMMERWARRIOR_MAX_ATTACK;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.AttackDelay)] = InitialValues.WARHAMMERWARRIOR_ATTACK_DELAY ;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.Life, true)] = InitialValues.WARHAMMERWARRIOR_UPGRADE_LIFE_PRICE;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.Range,true)] = InitialValues.WARHAMMERWARRIOR_UPGRADE_RANGE_PRICE;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.MaxAttack,true)] = InitialValues.WARHAMMERWARRIOR_UPGRADE_MAX_ATTACK_PRICE;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.AttackDelay,true)] = InitialValues.WARHAMMERWARRIOR_UPGRADE_ATTACK_DELAY_PRICE ;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior)] = InitialValues.WARHAMMERWARRIOR_COUNT ;

		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.Life)] = InitialValues.AXEWARRIOR_LIFE;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.Range)] = InitialValues.AXEWARRIOR_RANGE;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.MaxAttack)] = InitialValues.AXEWARRIOR_MAX_ATTACK;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.AttackDelay)] = InitialValues.AXEWARRIOR_ATTACK_DELAY ;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.Life, true)] = InitialValues.AXEWARRIOR_UPGRADE_LIFE_PRICE;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.Range,true)] = InitialValues.AXEWARRIOR_UPGRADE_RANGE_PRICE;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.MaxAttack,true)] = InitialValues.AXEWARRIOR_UPGRADE_MAX_ATTACK_PRICE;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.AttackDelay,true)] = InitialValues.AXEWARRIOR_UPGRADE_ATTACK_DELAY_PRICE ;
		dic [getKey(Barracks.HeroClass.AxeWarrior)] = InitialValues.AXEWARRIOR_COUNT ;

		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.Life)] = InitialValues.MACEWARRIOR_LIFE;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.Range)] = InitialValues.MACEWARRIOR_RANGE;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.MaxAttack)] = InitialValues.MACEWARRIOR_MAX_ATTACK;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.AttackDelay)] = InitialValues.MACEWARRIOR_ATTACK_DELAY ;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.Life, true)] = InitialValues.MACEWARRIOR_UPGRADE_LIFE_PRICE;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.Range,true)] = InitialValues.MACEWARRIOR_UPGRADE_RANGE_PRICE;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.MaxAttack,true)] = InitialValues.MACEWARRIOR_UPGRADE_MAX_ATTACK_PRICE;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.AttackDelay,true)] = InitialValues.MACEWARRIOR_UPGRADE_ATTACK_DELAY_PRICE ;
		dic [getKey(Barracks.HeroClass.MaceWarrior)] = InitialValues.MACEWARRIOR_COUNT ;

		dic ["MONEY"] = InitialValues.MONEY;
	
		dic [getKey (Barracks.ItemClass.AttackBlackHole)] = InitialValues.ATTACKBLACKHOLE_PRICE;
		dic [getKey (Barracks.ItemClass.AttackThunder)] = InitialValues.ATTACKTHUNDER_PRICE;
		dic [getKey (Barracks.ItemClass.CureCharacter)] = InitialValues.CURECHARACTER_PRICE;
		dic [getKey (Barracks.ItemClass.CureCristal)] = InitialValues.CURECRISTAL_PRICE;
		dic [getKey (Barracks.ItemClass.Teletransport)] = InitialValues.TELETRANSPORT_PRICE;
		dic [getKey (Barracks.ItemClass.UpgradeAgility)] = InitialValues.UPGRADE_AGILITY_PRICE;
		dic [getKey (Barracks.ItemClass.UpgradeForce)] = InitialValues.UPGRADE_FORCE_PRICE;
		dic [getKey (Barracks.ItemClass.UpgradeDefense)] = InitialValues.UPGRADE_DEFENSE_PRICE;

		dic [getKey (Barracks.ItemClass.AttackBlackHole, true)] = InitialValues.ATTACKBLACKHOLE_COUNT;
		dic [getKey (Barracks.ItemClass.AttackThunder, true)] = InitialValues.ATTACKTHUNDER_COUNT;
		dic [getKey (Barracks.ItemClass.CureCharacter, true)] = InitialValues.CURECHARACTER_COUNT;
		dic [getKey (Barracks.ItemClass.CureCristal,true)] = InitialValues.CURECRISTAL_COUNT;
		dic [getKey (Barracks.ItemClass.Teletransport,true)] = InitialValues.TELETRANSPORT_COUNT;
		dic [getKey (Barracks.ItemClass.UpgradeAgility,true)] = InitialValues.UPGRADE_AGILITY_COUNT;
		dic [getKey (Barracks.ItemClass.UpgradeForce,true)] = InitialValues.UPGRADE_FORCE_COUNT;
		dic [getKey (Barracks.ItemClass.UpgradeDefense,true)] = InitialValues.UPGRADE_DEFENSE_COUNT;

		dic ["CHAPTER"] = 0.0f;

		dic ["NUMBER_OF_VICTORIES"] = 0f;

		//Carregar preferencias do usuario que nao devem ser reinicializados com o new game.
		if(InitialValues.ALREADY_VIEWED_TUTORIAL) dic ["ALREADY_VIEWED_TUTORIAL"] = 1f;
		else dic ["ALREADY_VIEWED_TUTORIAL"] = 0f;

		dic ["isMusicOff"]  = InitialValues.MUSIC_OFF;
		dic ["isEffectOff"] = InitialValues.EFFECT_OFF;
	}

	static void fillDictionaryNoPreferences(){
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.Life)] = InitialValues.FIGHTER_LIFE;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.Range)] = InitialValues.FIGHTER_RANGE;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.MaxAttack)] = InitialValues.FIGHTER_MAX_ATTACK;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.AttackDelay)] = InitialValues.FIGHTER_ATTACK_DELAY ;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.Life, true)] = InitialValues.FIGHTER_UPGRADE_LIFE_PRICE;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.Range,true)] = InitialValues.FIGHTER_UPGRADE_RANGE_PRICE;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.MaxAttack,true)] = InitialValues.FIGHTER_UPGRADE_MAX_ATTACK_PRICE;
		dic [getKey(Barracks.HeroClass.Fighter, Barracks.Attributes.AttackDelay,true)] = InitialValues.FIGHTER_UPGRADE_ATTACK_DELAY_PRICE ;
		dic [getKey(Barracks.HeroClass.Fighter)] = InitialValues.FIGHTER_COUNT ;
		
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.Life)] = InitialValues.WARHAMMERWARRIOR_LIFE;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.Range)] = InitialValues.WARHAMMERWARRIOR_RANGE;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.MaxAttack)] = InitialValues.WARHAMMERWARRIOR_MAX_ATTACK;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.AttackDelay)] = InitialValues.WARHAMMERWARRIOR_ATTACK_DELAY ;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.Life, true)] = InitialValues.WARHAMMERWARRIOR_UPGRADE_LIFE_PRICE;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.Range,true)] = InitialValues.WARHAMMERWARRIOR_UPGRADE_RANGE_PRICE;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.MaxAttack,true)] = InitialValues.WARHAMMERWARRIOR_UPGRADE_MAX_ATTACK_PRICE;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior, Barracks.Attributes.AttackDelay,true)] = InitialValues.WARHAMMERWARRIOR_UPGRADE_ATTACK_DELAY_PRICE ;
		dic [getKey(Barracks.HeroClass.WarhammerWarrior)] = InitialValues.WARHAMMERWARRIOR_COUNT ;
		
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.Life)] = InitialValues.AXEWARRIOR_LIFE;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.Range)] = InitialValues.AXEWARRIOR_RANGE;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.MaxAttack)] = InitialValues.AXEWARRIOR_MAX_ATTACK;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.AttackDelay)] = InitialValues.AXEWARRIOR_ATTACK_DELAY ;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.Life, true)] = InitialValues.AXEWARRIOR_UPGRADE_LIFE_PRICE;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.Range,true)] = InitialValues.AXEWARRIOR_UPGRADE_RANGE_PRICE;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.MaxAttack,true)] = InitialValues.AXEWARRIOR_UPGRADE_MAX_ATTACK_PRICE;
		dic [getKey(Barracks.HeroClass.AxeWarrior, Barracks.Attributes.AttackDelay,true)] = InitialValues.AXEWARRIOR_UPGRADE_ATTACK_DELAY_PRICE ;
		dic [getKey(Barracks.HeroClass.AxeWarrior)] = InitialValues.AXEWARRIOR_COUNT ;
		
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.Life)] = InitialValues.MACEWARRIOR_LIFE;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.Range)] = InitialValues.MACEWARRIOR_RANGE;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.MaxAttack)] = InitialValues.MACEWARRIOR_MAX_ATTACK;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.AttackDelay)] = InitialValues.MACEWARRIOR_ATTACK_DELAY ;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.Life, true)] = InitialValues.MACEWARRIOR_UPGRADE_LIFE_PRICE;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.Range,true)] = InitialValues.MACEWARRIOR_UPGRADE_RANGE_PRICE;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.MaxAttack,true)] = InitialValues.MACEWARRIOR_UPGRADE_MAX_ATTACK_PRICE;
		dic [getKey(Barracks.HeroClass.MaceWarrior, Barracks.Attributes.AttackDelay,true)] = InitialValues.MACEWARRIOR_UPGRADE_ATTACK_DELAY_PRICE ;
		dic [getKey(Barracks.HeroClass.MaceWarrior)] = InitialValues.MACEWARRIOR_COUNT ;
		
		dic ["MONEY"] = InitialValues.MONEY;
		
		dic [getKey (Barracks.ItemClass.AttackBlackHole)] = InitialValues.ATTACKBLACKHOLE_PRICE;
		dic [getKey (Barracks.ItemClass.AttackThunder)] = InitialValues.ATTACKTHUNDER_PRICE;
		dic [getKey (Barracks.ItemClass.CureCharacter)] = InitialValues.CURECHARACTER_PRICE;
		dic [getKey (Barracks.ItemClass.CureCristal)] = InitialValues.CURECRISTAL_PRICE;
		dic [getKey (Barracks.ItemClass.Teletransport)] = InitialValues.TELETRANSPORT_PRICE;
		dic [getKey (Barracks.ItemClass.UpgradeAgility)] = InitialValues.UPGRADE_AGILITY_PRICE;
		dic [getKey (Barracks.ItemClass.UpgradeForce)] = InitialValues.UPGRADE_FORCE_PRICE;
		dic [getKey (Barracks.ItemClass.UpgradeDefense)] = InitialValues.UPGRADE_DEFENSE_PRICE;
		
		dic [getKey (Barracks.ItemClass.AttackBlackHole, true)] = InitialValues.ATTACKBLACKHOLE_COUNT;
		dic [getKey (Barracks.ItemClass.AttackThunder, true)] = InitialValues.ATTACKTHUNDER_COUNT;
		dic [getKey (Barracks.ItemClass.CureCharacter, true)] = InitialValues.CURECHARACTER_COUNT;
		dic [getKey (Barracks.ItemClass.CureCristal,true)] = InitialValues.CURECRISTAL_COUNT;
		dic [getKey (Barracks.ItemClass.Teletransport,true)] = InitialValues.TELETRANSPORT_COUNT;
		dic [getKey (Barracks.ItemClass.UpgradeAgility,true)] = InitialValues.UPGRADE_AGILITY_COUNT;
		dic [getKey (Barracks.ItemClass.UpgradeForce,true)] = InitialValues.UPGRADE_FORCE_COUNT;
		dic [getKey (Barracks.ItemClass.UpgradeDefense,true)] = InitialValues.UPGRADE_DEFENSE_COUNT;

		dic ["CHAPTER"] = 0.0f;

		//Carregar preferencias do usuario que nao devem ser reinicializados com o new game.
		if(InitialValues.ALREADY_VIEWED_TUTORIAL) dic ["ALREADY_VIEWED_TUTORIAL"] = 1f;
		else dic ["ALREADY_VIEWED_TUTORIAL"] = 0f;

	}


	public static bool alreadyViewedTutorial(){
		if (dic ["ALREADY_VIEWED_TUTORIAL"] > 0.5f)
			return true;
		else
			return false;
	}

	public static void setAlreadyViewedTutorial(bool tut){
		dic ["ALREADY_VIEWED_TUTORIAL"] = (tut ? 1f : 0f);
		writeDictionaryOnDisk ();
	}

	public static float currentChapter(){
		return dic ["CHAPTER"];
	}

	public static void nextChapter(){
		dic ["CHAPTER"] += 1.0f;
		writeDictionaryOnDisk ();
	}

	public static int getNumberOfVictories(){
		return (int)dic ["NUMBER_OF_VICTORIES"];
	}

	public static void increaseByOneNumberOfVictories(){
		dic ["NUMBER_OF_VICTORIES"] += 1f;
		writeDictionaryOnDisk ();
	}

	public static void nextChapter(float nextChapter){
		dic ["CHAPTER"] = nextChapter;
		writeDictionaryOnDisk ();
	}

	public static bool isMusicOff(){
		int state = (int)dic ["isMusicOff"];

		if(state == 0)
			return false;
		else
			return true;
	}

	public static bool isEffectOff(){
		int state = (int)dic ["isEffectOff"];
		if(state == 0)
			return false;
		else
			return true;
	}

	public static void musicIsOff(bool state){
		if(state)
			dic ["isMusicOff"] = (float)1.0;
		else
			dic ["isMusicOff"] = (float)0.0;
	}

	public static void effectIsOff(bool state){
		if(state)
			dic ["isEffectOff"] = (float)1.0;
		else
			dic ["isEffectOff"] = (float)0.0;
	}

	public static int getPlayerMoney(){
		return (int)dic ["MONEY"];
	}

	public static void updatePlayerMoney(float value){
		dic ["MONEY"] = value;
		//writeDictionaryOnDisk ();
	}

	public static float getValueOfAttribute(string hero, string att){
		string key = hero + "_" + att;
		key = key.ToUpper ();
		return dic [key];
	}

	public static void updateValueOfAttribute(string hero, string att, float value){
		string key = hero + "_" + att;
		key = key.ToUpper ();
		dic [key] = value;
		//writeDictionaryOnDisk ();
	}

	public static float getValueToUpgradeAttribute(string hero, string att){
		string key = hero + "_UPGRADE_" + att + "_PRICE";
		key = key.ToUpper ();
		return dic [key];
	}

	public static void updateValueToUpgradeAttribute(string hero, string att, float value){
		string key = hero + "_UPGRADE_" + att + "_PRICE";
		key = key.ToUpper ();
		dic [key] = value;
		//writeDictionaryOnDisk ();
	}

	public static float getItemPrice(string item){
		string key = item + "_PRICE";
		key = key.ToUpper ();
		return dic [key];
	}

	public static void updateItemPrice(string item, float value){
		string key = item + "_PRICE";
		key = key.ToUpper ();
		dic [key] = value;
		//writeDictionaryOnDisk ();
	}

	public static float getItemCount(string item){
		string key = item + "_COUNT";
		key = key.ToUpper ();
		return dic [key];
	}

	public static void updateItemCount(string item, float value){
		string key = item + "_COUNT";
		key = key.ToUpper ();
		dic [key] = value;
		//writeDictionaryOnDisk ();
	}

	public static float getHeroCount(string hero){
		string key = hero + "_COUNT";
		key = key.ToUpper ();
		return dic [key];
	}
	
	public static void updateHeroCount(string hero, float value){
		string key = hero + "_COUNT";
		key = key.ToUpper ();
		dic [key] = value;
		//writeDictionaryOnDisk ();
	}


}