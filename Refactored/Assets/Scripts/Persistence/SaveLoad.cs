﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoad {

	//Lista com 3 slots de jogos.
	public static List<Game> savedGames = new List<Game>()
	{
		new Game(0),
		new Game(1),
		new Game(2)

	};
	
	//it's static so we can call it from anywhere
	public static void Save() {
		//Atualiza a lista com o jogo corrente, removendo seu antigo estado salvo e colocando o novo.
		SaveLoad.savedGames.RemoveAt (Game.current.slotPosition);
		SaveLoad.savedGames.Insert (Game.current.slotPosition, Game.current);

		BinaryFormatter bf = new BinaryFormatter();
		//Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
		FileStream file = File.Create (Application.persistentDataPath + "/savedGames.gd"); //you can call it anything you want
		bf.Serialize(file, SaveLoad.savedGames);
		file.Close();
	}	
	
	public static void Load() {
		if(File.Exists(Application.persistentDataPath + "/savedGames.gd")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
			SaveLoad.savedGames = (List<Game>)bf.Deserialize(file);
			file.Close();
		}
	}
}