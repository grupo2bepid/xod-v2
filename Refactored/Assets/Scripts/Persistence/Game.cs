﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Game { //don't need ": Monobehaviour" because we are not attaching it to a game object
	
	public static Game current;

	//Informacao sobre estado desse slot
	public int slotPosition;
	public bool isNewGame = true;



	
	public Game (int slot) {
		this.slotPosition = slot;
	}
	
}
