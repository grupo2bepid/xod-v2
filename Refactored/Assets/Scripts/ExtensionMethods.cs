﻿using UnityEngine;
using System.Collections;

public static class ExtensionMethods {

	public static void startWalk( this AIPath aipath ){
		aipath.canSearch = true;
		aipath.canMove = true;
	}
	
	public static void stopWalk(this AIPath aipath){
		aipath.canMove = false;
		aipath.canSearch = false;
	}

}