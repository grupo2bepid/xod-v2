﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class AppropriateDistancesForFight : object {

	private static Dictionary<string,  Dictionary<string,float> > dic;

	private static void fillForFighter(){
		dic [InitialValues.FIGHTER_NAME] = new Dictionary<string, float> ();
		dic [InitialValues.FIGHTER_NAME] [InitialValues.MUMMY_NAME] = 0.85f;
		dic [InitialValues.FIGHTER_NAME] [InitialValues.GHOUL_NAME] = 0.85f;
		dic [InitialValues.FIGHTER_NAME] [InitialValues.VAMPIRE_NAME] = 0.85f;
		dic [InitialValues.FIGHTER_NAME] [InitialValues.SKELETON_NAME] = 0.85f;
		dic [InitialValues.FIGHTER_NAME] [InitialValues.DEMON_NAME] = 1.5f;
	}
	
	private static void fillForWarhammerWarrior(){
		dic [InitialValues.WARHAMMERWARRIOR_NAME] = new Dictionary<string, float> ();
		dic [InitialValues.WARHAMMERWARRIOR_NAME] [InitialValues.MUMMY_NAME] = 1f;
		dic [InitialValues.WARHAMMERWARRIOR_NAME] [InitialValues.GHOUL_NAME] = 1f;
		dic [InitialValues.WARHAMMERWARRIOR_NAME] [InitialValues.VAMPIRE_NAME] = 1f;
		dic [InitialValues.WARHAMMERWARRIOR_NAME] [InitialValues.SKELETON_NAME] = 1f;
		dic [InitialValues.WARHAMMERWARRIOR_NAME] [InitialValues.DEMON_NAME] = 1.5f;
	}

	private static void fillForAxeWarrior(){
		dic [InitialValues.AXEWARRIOR_NAME] = new Dictionary<string, float> ();
		dic [InitialValues.AXEWARRIOR_NAME] [InitialValues.MUMMY_NAME] = 1f;
		dic [InitialValues.AXEWARRIOR_NAME] [InitialValues.GHOUL_NAME] = 1f;
		dic [InitialValues.AXEWARRIOR_NAME] [InitialValues.VAMPIRE_NAME] = 1f;
		dic [InitialValues.AXEWARRIOR_NAME] [InitialValues.SKELETON_NAME] = 1f;
		dic [InitialValues.AXEWARRIOR_NAME] [InitialValues.DEMON_NAME] = 1.5f;
	}

	private static void fillForMaceWarrior(){
		dic [InitialValues.MACEWARRIOR_NAME] = new Dictionary<string, float> ();
		dic [InitialValues.MACEWARRIOR_NAME] [InitialValues.MUMMY_NAME] = 1f;
		dic [InitialValues.MACEWARRIOR_NAME] [InitialValues.GHOUL_NAME] = 1f;
		dic [InitialValues.MACEWARRIOR_NAME] [InitialValues.VAMPIRE_NAME] = 1f;
		dic [InitialValues.MACEWARRIOR_NAME] [InitialValues.SKELETON_NAME] = 1f;
		dic [InitialValues.MACEWARRIOR_NAME] [InitialValues.DEMON_NAME] = 1.5f;
	}

	public static void fillDictionary(){
		dic = new Dictionary<string, Dictionary<string, float>> ();
		fillForFighter ();
		fillForWarhammerWarrior ();
		fillForAxeWarrior ();
		fillForMaceWarrior ();
	}

	public static float getDistance(string heroName, string antiheroName){
		return dic [heroName] [antiheroName];
	}
}
