﻿using UnityEngine;
using System.Collections;

public class HatchetThrower : Hero {

	private GameObject hatchetPrefab;
	private GameObject activeHatchet;
	private GameObject idleHatchet;
	private Transform antiheroTransform ;
	private Vector3 targetInCaseOfTargetDeath;

	private void swapHatchet(){
		bool activeState = activeHatchet.activeSelf;
		bool idleState = idleHatchet.activeSelf;
		activeHatchet.SetActive ( !activeState );
		idleHatchet.SetActive ( !idleState );
	}
	
	private void releaseHatchet(){
		GameObject hatchetGameObject = (GameObject)Instantiate ( hatchetPrefab,  activeHatchet.transform.position , activeHatchet.transform.rotation );
		Hatchet hatchetScript = (Hatchet)hatchetGameObject.GetComponent (typeof(Hatchet));
		if( antiheroTransform != null )	hatchetScript.setTarget (antiheroTransform.position);
		else hatchetScript.setTarget ( targetInCaseOfTargetDeath );
		swapHatchet ();
		lookToTarget = null;
	}

	private void markEndOfHatchetThrowAnimation(){
		playingShootingHatchetAnimation = false;
	}
	
	protected void tryShotHatchet(){
		alreadyCalledShotMethod = false;
		if (!Fighting && currentSpeed <= 0.2f) {
			Antihero antihero = findClosestAntihero();

			if( antihero != null && !antihero.Dying && sqrDistance2D( antihero.transform.position, heroBase.gameObject.transform.position ) <= 16f*range*range ){	//TODO fix range
//				print ("eu (" + gameObject.name + ") vou pra cima do doido " + antihero.gameObject.name);
				antiheroTransform = antihero.gameObject.transform;
				targetInCaseOfTargetDeath = antihero.gameObject.transform.position;
				lookToTarget = antiheroTransform;
				playingShootingHatchetAnimation = true;

				Invoke("markEndOfHatchetThrowAnimation", shootingHatchetAnimationDelay);

				animator.SetTrigger( HashIDs.throwHatchetTrigger );
			}

		}
	}

	new protected void Awake () {
		base.Awake ();
		hatchetPrefab = Util.LoadPrefabResource ("Hatchet") as GameObject;
		activeHatchet =  transform.FindChild ( "HATCHET_ACTION" ).gameObject;
		idleHatchet =  transform.FindChild ( "HATCHET_IDLE" ).gameObject;
	}
}
