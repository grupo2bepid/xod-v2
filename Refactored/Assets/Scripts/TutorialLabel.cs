﻿using UnityEngine;
using System.Collections;

public class TutorialLabel : MonoBehaviour {

	private bool seted = false;
	private Texture arrow;
	private Vector2 arrowPosition;
	private Vector2 labelPosition;
	private float offset = 10f;
	private Vector2 arrowSize;
	private Vector2 labelSize;
	private Texture labelBackground;

	public void initiate(string text, Vector2 pointPosition){

		arrowSize = new Vector2 ( Screen.width*0.1f, Screen.height*0.1f );
		labelSize = new Vector2 ( Screen.width*0.2f, Screen.height*0.2f );

		char c = screenFrame ( pointPosition );

		switch (c) {
			case 'l':
				arrow = Util.LoadImageResource("LeftMenuArrow") as Texture;
				arrowPosition = pointPosition;
				arrowPosition.x += arrowSize.x + offset;
				labelPosition = arrowPosition;
				labelPosition.x += arrowSize.x + offset;
				break;
			case 'r':
				arrow = Util.LoadImageResource("RightMenuArrow") as Texture;
				arrowPosition = pointPosition;
				arrowPosition.x -= arrowSize.x + offset;
				labelPosition = arrowPosition;
				labelPosition.x -= arrowSize.x + offset;
				break;
			case 'd':
				arrow = Util.LoadImageResource("DownMenuArrow") as Texture;
				arrowPosition = pointPosition;
				arrowPosition.y -= arrowSize.y + offset;
				labelPosition = arrowPosition;
				labelPosition.y -= arrowSize.y + offset;
				break;
			default:
				arrow = Util.LoadImageResource("UpMenuArrow") as Texture;
				arrowPosition = pointPosition;
				arrowPosition.y += arrowSize.y + offset;
				labelPosition = arrowPosition;
				labelPosition.y += arrowSize.y + offset;
				break;
		}

		guiText.text = text;
		guiText.fontSize = 30;
		guiText.pixelOffset = labelPosition;
		seted = true;
	}

	void Awake(){
		gameObject.transform.position = Vector3.zero;
		labelBackground = Util.LoadImageResource ("Roll") as Texture;
	}

	private char screenFrame( Vector2 pos ){
		/*	____
		 * |LUUR|
		 * |LUUR|
		 * |LDDR|
		 * |LDDR|
		 * ------
		 */
		if( pos.x < Screen.width*0.333f ) return 'l';
		else if( pos.x > Screen.width*0.666f ) return 'r';
		else if ( pos.y > Screen.height*0.5f ) return 'd';
		else return 'u';
	} 


	Vector2 getMousePositionOnScreen(){

//		Event e = Event.current;
//		Debug.Log(e.mousePosition);


		Vector3 mousePos = Input.mousePosition;

//		Vector3 mousePos = e.mousePosition;

		mousePos.y = Screen.height - mousePos.y ;
		return new Vector2 (mousePos.x, mousePos.y);
	}

	void OnGUI(){

		Rect rx = new Rect (100f,100f,100f,100f);

		if (GUI.Button (rx, "testaaaando")) {
			print ("FUI PRESSIONADO");

			print ("EVENT TYPE = " + Event.current.type);
			print ("EVENT MOUSE = " + Event.current.mousePosition);

		}


		if (rx.Contains (getMousePositionOnScreen ())) {
			if( Event.current.isKey ){
				print ("OPA, PRESSIONARAM " + Event.current.character);
			}
		}
		
//		if (  Event.current.type == EventType.used ) {
//			print ("SUBIU NA POSICAO = " +  getMousePositionOnScreen() );
//		}


		if (seted) {
			Rect r1 = new Rect (arrowPosition.x, arrowPosition.y, arrowSize.x, arrowSize.y);
			GUI.DrawTexture (r1, arrow, ScaleMode.ScaleToFit);

			Rect r2 = new Rect (labelPosition.x, labelPosition.y, labelSize.x, labelSize.y);
			GUI.DrawTexture (r2, labelBackground, ScaleMode.ScaleToFit);
		}
	}

	public float hf = 0f;
	public float wf = 0f;

	void Update(){



		float x = Screen.width * wf;
		float y = Screen.height * hf;

		Vector2 pt = new Vector2 (x, y);
		string txt = "guilherme";
		initiate (txt, pt);
	}

}