﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IntermediateTargetsCentral : MonoBehaviour {

	private List<Transform>[] intermediateTargestsPerLevel;

	[HideInInspector]
	public int phaseMaxTargetLevel;

	private void getIntermediateTargets(){
		IntermediateTarget[] targets = FindObjectsOfType<IntermediateTarget> ();
		phaseMaxTargetLevel = -10;
		foreach (IntermediateTarget it in targets) {
			intermediateTargestsPerLevel[ it.targetLevel ].Add( it.gameObject.transform );
			if( it.targetLevel > phaseMaxTargetLevel ) phaseMaxTargetLevel = it.targetLevel;
		}
	}

	void Awake(){
		intermediateTargestsPerLevel = new List<Transform>[10];
		for (int i = 0; i < 10; ++i)
			intermediateTargestsPerLevel [i] = new List<Transform> ();
		getIntermediateTargets ();
	}

//	void Start(){
//	}

	public Transform getRandomTargetOfLevel(int l){
		int levelSize = intermediateTargestsPerLevel [l].Count;
		return intermediateTargestsPerLevel [l] [Random.Range (0, levelSize)];
	}
}
