﻿using UnityEngine;
using System.Collections;

public class Mummy : Enemy {

	void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 1.667f;
		attackAnimationDelays [2] = 2.333f;
	}

	private void fillAttributes(){
		acceptableDistanceForHitCristal = 2f;
		maxAttack = 2;

		delayBetweenAttacks = InitialValues.MUMMY_ATTACK_DELAY;
		Life = (int)(InitialValues.MUMMY_LIFE*lifeMultiplier);
		level = InitialValues.MUMMY_LEVEL;
		bounty = (int)(InitialValues.MUMMY_BOUNTY*bountyMultiplier);

		characterName = InitialValues.MUMMY_NAME;
	}


	new void Awake(){
		base.Awake ();
		fillAttackAnimationDelays ();
		fillAttributes ();
	}

}
