﻿using UnityEngine;
using System.Collections;

public class Vampire : Enemy {

	void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 1.167f;
		attackAnimationDelays [2] = 1.000f;
		attackAnimationDelays [3] = 2.500f;
	}

	private void fillAttributes(){
		acceptableDistanceForHitCristal = 3.2f;
		maxAttack = 3;
		roarAnimationDelay = 2.333f;

		delayBetweenAttacks = InitialValues.VAMPIRE_ATTACK_DELAY;
		Life = (int)(InitialValues.VAMPIRE_LIFE*lifeMultiplier);
		level = InitialValues.VAMPIRE_LEVEL;
		bounty = (int)(InitialValues.VAMPIRE_BOUNTY*bountyMultiplier);

		characterName = InitialValues.VAMPIRE_NAME;
	}
	
	new void Awake(){
		base.Awake ();
		fillAttackAnimationDelays ();
		fillAttributes ();
	}

}
