﻿using UnityEngine;
using System.Collections;

public class Skeleton : Enemy {

	void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 0.667f;
		attackAnimationDelays [2] = 0.667f;
		attackAnimationDelays [3] = 0.667f;
	}

	private void fillAttributes(){
		acceptableDistanceForHitCristal = 2f;
		maxAttack = 3;

		delayBetweenAttacks = InitialValues.SKELETON_ATTACK_DELAY;
		Life = (int)(InitialValues.SKELETON_LIFE*lifeMultiplier);
		level = InitialValues.SKELETON_LEVEL;
		bounty = (int)(InitialValues.SKELETON_BOUNTY*bountyMultiplier);

		characterName = InitialValues.SKELETON_NAME;
	}

	new void Awake(){
		base.Awake ();
		fillAttackAnimationDelays ();
		fillAttributes ();
	}

}
