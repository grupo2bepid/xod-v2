﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Demon : Boss {

	private float flightHeight;
	private float takingOffFraction;
	private float landingFraction;
	private bool takingOff = false;
	private bool landing = false;
	private GameObject fireballPrefab;
	private int numberOfAttacksPerSequence;
	private int attacksOnCurrentSequence = 0;
	private float inAirAttackAnimationLength;
	private Quaternion toRotation;
	private Quaternion fromRotation;
	private bool rotating = false;
	private bool firstFlight = true;
	private float firstFlightFraction;
	private float phaseHeight;
	private Vector3[] currentAttackSequenceTargets;
	private List<GameObject> flammeGameObjects;

	public Transform leftHandTransform;
	public Transform rightHandTransform;
	public Transform leftTargetHelper;
	public Transform rightTargetHelper;

	//sounds
	public AudioSource effects;

	public bool isDemonFlying(){
		return (currentSpeed > 0.2f);
	}

	private void fillAttributes(){
		delayBetweenAttacks = InitialValues.DEMON_ATTACK_DELAY;
		Life = (int)(InitialValues.DEMON_LIFE*lifeMultiplier);
		bounty = (int)(InitialValues.DEMON_BOUNTY*bountyMultiplier);
		initialLife = (int)(InitialValues.DEMON_LIFE*lifeMultiplier);
		phaseHeight = GameController.phaseHeight;
		numberOfAttacksPerSequence = 8;
		maxAttack = 1;
		acceptableDistanceForHitCristal = 2.4f;
		flightHeight = 2.25f;
		takingOffAnimationLength = 1.000f;
		landingAnimationLength = 0.667f;
		inAirAttackAnimationLength = 0.833f;
		takingOffFraction = flightHeight / takingOffAnimationLength;
		landingFraction = flightHeight / landingAnimationLength;
		firstFlightFraction = (flightHeight + phaseHeight) / takingOffAnimationLength;
		characterName = InitialValues.DEMON_NAME;

	}

	private void findObjectsOfFlammeParticleSystem( GameObject father ){
		foreach (Transform t in father.transform) {
			GameObject go = t.gameObject;
			if( t.name.Contains("flamme") ){
				flammeGameObjects.Add( go );
			}
			findObjectsOfFlammeParticleSystem(go) ;
		}
	}
	
	private void setFlammeState(bool newState){
		foreach (GameObject go in flammeGameObjects) {
			go.SetActive( newState );
		}
	}

	new void Awake(){
		base.Awake ();
		fillAttackAnimationDelays ();
		fillAttributes ();
		fireballPrefab = Util.LoadPrefabResource ("Fireball") as GameObject;
		currentAttackSequenceTargets = new Vector3[numberOfAttacksPerSequence + 10];
		flammeGameObjects = new List<GameObject> ();
		findObjectsOfFlammeParticleSystem ( gameObject );
		setFlammeState ( false );

		//sounds
		GameCameraSound.bossAlive = true;
		/*inicializacao da parte sonora*/
		effects = gameObject.AddComponent<AudioSource> ();

	}
	
	void fillArrayWithTargetsForCurrentPosition(){
		for (int i = 1; i <= numberOfAttacksPerSequence; ++i) {
			Vector3 tgt = Vector3.Lerp( leftTargetHelper.position, rightTargetHelper.position, (float)i/(float)(numberOfAttacksPerSequence+1) );
			tgt.y = 0.2f;
			currentAttackSequenceTargets[i-1] = tgt;
//			Debug.DrawLine(transform.position, tgt, Color.red, 60f);
		}
	}

	void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 2.000f;
	}
	
	void takeOff(){
		setFlammeState (true);
		takingOff = true;
		Invoke ("inAir",takingOffAnimationLength);
	}
	void inAir(){
		takingOff = false;
	}
	
	void land(){
		firstFlight = false;
		landing = true;
		Invoke ("inFloor", landingAnimationLength);
	}
	void inFloor(){
		setFlammeState (false);
		landing = false;
	}

	void registerFireAttack(){
		if (attacksOnCurrentSequence == 0) {
			aipath.canMove = false;
			fillArrayWithTargetsForCurrentPosition();
			rotating = true;
		}
		else if (attacksOnCurrentSequence >= numberOfAttacksPerSequence) {
			rotating = false;
			attacksOnCurrentSequence = 0;
			aipath.canMove = true;
			animator.SetTrigger( HashIDs.flyTrigger );
		}

	}

	private void rotateTowardNextTarget(){
		Vector3 tgt = currentAttackSequenceTargets[attacksOnCurrentSequence];
		Vector3 lookPos = tgt - transform.position;
		lookPos.y = 0;
		Quaternion toRotation = Quaternion.LookRotation(lookPos);
		transform.rotation = Quaternion.Slerp( transform.rotation , toRotation, Time.deltaTime/inAirAttackAnimationLength );
	}

	new void Update(){
		base.Update ();
		if (takingOff) {
			if( !firstFlight ) transform.Translate (new Vector3 (0f, takingOffFraction * Time.deltaTime, 0f));
			else transform.Translate (new Vector3 (0f, firstFlightFraction * Time.deltaTime, 0f));
		}
		else if (landing)
			transform.Translate (new Vector3 (0f, - landingFraction * Time.deltaTime, 0f));
		else if (rotating) {
			rotateTowardNextTarget();
		}

		//sounds
		if (Life <= 0 && GameCameraSound.bossAlive == true) {
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/bossDeadEffect")as AudioClip);
			GameCameraSound.bossAlive = false;
		}

	}

	void releaseFireball(){
		Vector3 betweenHandsPosition = Vector3.Lerp (leftHandTransform.position, rightHandTransform.position, 0.5f);
		Fireball fireballScript = (Instantiate (fireballPrefab, betweenHandsPosition, transform.rotation) as GameObject).GetComponent (typeof(Fireball)) as Fireball;
		Vector3 target = currentAttackSequenceTargets[attacksOnCurrentSequence];
		fireballScript.setTarget ( target );
		++attacksOnCurrentSequence;
		rotating = true;
	}

}
