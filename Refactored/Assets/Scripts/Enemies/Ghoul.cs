﻿using UnityEngine;
using System.Collections;

public class Ghoul : Enemy {

	void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 0.833f;
		attackAnimationDelays [2] = 0.833f;
		attackAnimationDelays [3] = 0.833f;
	}

	private void fillAttributes(){
		acceptableDistanceForHitCristal = 2f;
		maxAttack = 3;
		roarAnimationDelay = 2.167f;

		Life = (int)(InitialValues.GHOUL_LIFE*lifeMultiplier);
		level = InitialValues.GHOUL_LEVEL;

		delayBetweenAttacks = InitialValues.GHOUL_ATTACK_DELAY;
		bounty = (int)(InitialValues.GHOUL_BOUNTY*bountyMultiplier);

		characterName = InitialValues.GHOUL_NAME;
	}

	new void Awake(){
		base.Awake ();
		fillAttackAnimationDelays ();
		fillAttributes ();
	}

}
