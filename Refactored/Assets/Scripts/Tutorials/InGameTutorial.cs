﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InGameTutorial : Tutorial {
	
	void fillTutorialItems(){
		tutorialItems = new ItemTutorialTuple[14];

		Dictionary<string, Rect> hudRectangles = hud.getRetangles ();

		Rect heroRect = hudRectangles["armyPos"];
		Rect itemRect = hudRectangles["itemPos"];
		Rect fighterRect = hudRectangles["Fighter"];
		Rect upgradeForceRect = hudRectangles["UpgradeForce"];
		Rect blackHoleRect = hudRectangles["AttackBlackHole"];
		Rect teletransportRect = hudRectangles["Teletransport"];

		Rect endOfTutorialRect = new Rect (-1f, -1f, -1f, -1f);

		tutorialItems [0] = new ItemTutorialTuple (STR.get("tut0"), heroRect, "armyMenu");
		tutorialItems [1] = new ItemTutorialTuple (STR.get("tut1"), fighterRect, "FighterButt");
		tutorialItems [2] = new ItemTutorialTuple (STR.get("tut2"), itemRect, "confirm");
		tutorialItems [3] = new ItemTutorialTuple (STR.get("tut3"), itemRect, "itemMenu");
		tutorialItems [4] = new ItemTutorialTuple (STR.get("tut4"), teletransportRect, "TeletransportButt");
		tutorialItems [5] = new ItemTutorialTuple (STR.get("tut5"), itemRect, "confirm");
		tutorialItems [6] = new ItemTutorialTuple (STR.get("tut6"), itemRect, "confirm");
		tutorialItems [7] = new ItemTutorialTuple (STR.get("tut7"), itemRect, "itemMenu");
		tutorialItems [8] = new ItemTutorialTuple (STR.get("tut8"), upgradeForceRect, "UpgradeForceButt");
		tutorialItems [9] = new ItemTutorialTuple (STR.get("tut9"), itemRect, "confirm");
		tutorialItems [10] = new ItemTutorialTuple (STR.get("tut10"), itemRect, "itemMenu");
		tutorialItems [11] = new ItemTutorialTuple (STR.get("tut11"), blackHoleRect, "AttackBlackHoleButt");
		tutorialItems [12] = new ItemTutorialTuple (STR.get("tut12"), itemRect, "confirm");
		tutorialItems [13] = new ItemTutorialTuple (STR.get("tut13"), endOfTutorialRect, "");

	}

	void Start(){
		hud = FindObjectOfType<HUD_refactored> ();
		hud.runningTutorial = true;
		fillTutorialItems ();
		hud.pointExclusivelyTo = tutorialItems [currentItem = 0].buttonTextureName;
		tutorialItem.newTextAndPosition( tutorialItems[currentItem=0].test, tutorialItems[currentItem=0].rect );
	}

	void OnEnable(){
		HUD_refactored hud = FindObjectOfType<HUD_refactored> ();
		hud.onMouseAction += mouseClick;
	}

	void OnDisable(){
		HUD_refactored hud = FindObjectOfType<HUD_refactored> ();
		hud.onMouseAction -= mouseClick;
	}

}
