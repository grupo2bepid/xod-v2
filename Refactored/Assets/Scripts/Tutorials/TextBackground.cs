﻿using UnityEngine;
using System.Collections;

public class TextBackground : MonoBehaviour {
	private Texture background;
	public Rect r;

	void Awake(){
		background = Util.LoadImageResource ("Roll") as Texture;
		r = new Rect ();
		r.width = Screen.width * 0.69f;
		r.height = Screen.height * 0.25f;
		r.x = (Screen.width - r.width) * 0.5f;
		r.y = Screen.height * 0.12f;
	}

	void OnGUI(){
		if (GameController.State != GameController.GameStateEnum.Paused) {
			GUI.depth = 5;
			GUI.DrawTexture (r, background);
		}
	}
}
