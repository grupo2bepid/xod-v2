﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemTutorialTuple{
	public string test;
	public Rect rect;
	public string buttonTextureName;
	public ItemTutorialTuple( string t, Rect r, string btName ){
		test = t;
		rect = r;
		buttonTextureName = btName;
	}
}

public abstract class Tutorial : MonoBehaviour {

	protected TutorialItem tutorialItem;
	protected ItemTutorialTuple[] tutorialItems;
	protected int currentItem;
	protected Phase phaseReference;
	protected HUD_refactored hud;

	private bool waitingForUpClick = false;
	private Rect skipTutorialRect;
	private Texture skipPressed;
	private Texture skipUnpressed;
	private Texture currentSkipButtonTexture;
	private float timePassed = 0f;
	private float threshold = 0.09f;
	private bool clickedSkip = false;

	protected void Awake(){
		GameObject tutorialItemGameObject = Util.LoadPrefabResource ("TutorialItem") as GameObject;
		tutorialItemGameObject = Instantiate (tutorialItemGameObject) as GameObject;
		tutorialItem = tutorialItemGameObject.GetComponent (typeof(TutorialItem)) as TutorialItem;
		skipPressed = Util.LoadImageResource ( STR.TranslatedResources.SKIP_PRESSED ) as Texture;
		currentSkipButtonTexture = skipUnpressed = Util.LoadImageResource ( STR.TranslatedResources.SKIP ) as Texture;

		float skipTutorialWidth = currentSkipButtonTexture.width * Screen.width / 2048;
		skipTutorialWidth *= 0.76f;
		float skipTutorialHeight = currentSkipButtonTexture.height * Screen.height / 1536;
		skipTutorialHeight *= 0.76f;
		skipTutorialRect = new Rect (Screen.width / 2 - (skipTutorialWidth) / 2, 0.95f*Screen.height - ( skipTutorialHeight ), skipTutorialWidth, skipTutorialHeight );
	}
	
	protected void mouseClick(Vector2 mpos){
		if (tutorialItems [currentItem].rect.Contains (mpos)) {
				++currentItem;
				if (currentItem == tutorialItems.Length - 1) {
						hud.pointExclusivelyTo = "";
						hud.runningTutorial = false;
						Invoke ("playPhaseAndDestroyMyself", 2f);
				}

				hud.pointExclusivelyTo = tutorialItems [currentItem].buttonTextureName;
				tutorialItem.newTextAndPosition (tutorialItems [currentItem].test, tutorialItems [currentItem].rect);
			}
	}

	private void playPhaseAndDestroyMyself(){
		Database.setAlreadyViewedTutorial (true);
		phaseReference.playPhase ();
		Destroy (tutorialItem.gameObject);
		Destroy (gameObject);
	}

	protected void OnGUI(){
		timePassed += Time.deltaTime;
		if( GameController.State != GameController.GameStateEnum.Paused && currentItem < tutorialItems.Length - 1)	drawSkipButton ();
	}

	public void setPhaseReference(Phase p){
		phaseReference = p;
	}

	private void drawSkipButton(){
		if(GUI.RepeatButton (skipTutorialRect, "")){
			//Vai para estado pressionado
			currentSkipButtonTexture	= skipPressed;
			clickedSkip 	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(skipTutorialRect, currentSkipButtonTexture);
			
		}else if(clickedSkip && timePassed > threshold){

			hud.pointExclusivelyTo = "";
			hud.runningTutorial = false;
			playPhaseAndDestroyMyself();

			currentSkipButtonTexture = skipUnpressed;
			clickedSkip 	= false;
			GUI.DrawTexture(skipTutorialRect, currentSkipButtonTexture);
			
		}else
			GUI.DrawTexture(skipTutorialRect, currentSkipButtonTexture);
		
	}
}
