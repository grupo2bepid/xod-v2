﻿using UnityEngine;
using System.Collections;

public class HowToPlay : MonoBehaviour {

	private int step = 0;
	public int stepMax = 5;
	private Texture leftButton;
	private Texture rightButton;
	private Rect leftRect;
	private Rect rightRect;
	private bool left = false;
	private bool right = false;
	private bool exit = false;

	private Texture exitButton;
	private Rect exitRect;

	private bool download = false;
	
	private Texture downloadButton;
	private Rect downloadRect;

	float timePassed = 0; 
	const float threshold = 0.05F;

	GUIStyle style = new GUIStyle();

	public GameObject musicBox;
	public AudioSource effects;

	// Use this for initialization
	void Start () {
	
	}

	void Awake(){

		leftButton = (Texture)Util.LoadImageResource (STR.Resources.BACK_MENU_HERO);
		rightButton = (Texture)Util.LoadImageResource (STR.Resources.BACK_MENU_ITEM);
		exitButton = (Texture)Util.LoadImageResource (STR.Resources.EXIT_BUTTON);
		downloadButton = (Texture)Util.LoadImageResource (STR.Resources.DOWNLOAD);

		leftRect 	= new Rect (0, 0.5f*(Screen.height - (leftButton.height * Screen.height / 1536)), leftButton.width * Screen.width / 2048, leftButton.height * Screen.height / 1536);
		rightRect 	= new Rect (Screen.width - (rightButton.width * Screen.width / 2048) , 0.5f*(Screen.height - (rightButton.height * Screen.height / 1536)), rightButton.width * Screen.width / 2048, rightButton.height * Screen.height / 1536);
		exitRect 	= new Rect (0, 0, exitButton.width * Screen.width / 2048, exitButton.height * Screen.height / 1536);
		downloadRect = new Rect (0.5f*(Screen.width - 0.25f*Screen.width), 0.75f*(Screen.height - 0.25f*Screen.height), 0.25f*Screen.width, 0.25f*Screen.height);

		musicBox = new GameObject ("MusicBox");
		musicBox.transform.position = gameObject.transform.position;

		effects = musicBox.AddComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

		timePassed += Time.deltaTime;
	
	}

	void OnGUI(){
		GUI.backgroundColor = Color.clear; 

		GUI.DrawTexture(new Rect(0,0,2048,1536), (Texture)Util.LoadImageResource (STR.Resources.HOW_TO_PLAY_BACKGROUND));
		
		if (step > 0)
			drawLeftButton ();

		if (step < stepMax)
			drawRightButton ();

		drawExitButton ();

		loadStep ();


		
	}

	void drawExitButton(){
		
		if(GUI.RepeatButton (exitRect, "")){
			//Vai para estado pressionado
			exitButton		= (Texture)Util.LoadImageResource (STR.Resources.EXIT_BUTTON_PRESSED);
			exit 	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(exitRect, exitButton);
			
		}else if(exit && timePassed > threshold){
			
			
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);

			//Volta para estado despressionado
			exitButton		= (Texture)Util.LoadImageResource (STR.Resources.EXIT_BUTTON);
			exit 	= false;
			
			GUI.DrawTexture(exitRect, exitButton);

			//Faz operacao
			Application.LoadLevel ("BeginMenu");
			
		}else
			GUI.DrawTexture(exitRect, exitButton);
		
		
	}

	void drawLeftButton(){

		if(GUI.RepeatButton (leftRect, "")){
			//Vai para estado pressionado
			leftButton		= (Texture)Util.LoadImageResource (STR.Resources.BACK_MENU_HERO_PRESSED);
			left 	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(leftRect, leftButton);
			
		}else if(left && timePassed > threshold){
			//Faz operacao
			
			step --;

			
			//Volta para estado despressionado
			leftButton		= (Texture)Util.LoadImageResource (STR.Resources.BACK_MENU_HERO);
			left 	= false;

			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);

			GUI.DrawTexture(leftRect, leftButton);
			
		}else
			GUI.DrawTexture(leftRect, leftButton);


	}

	void drawRightButton(){
		
		if(GUI.RepeatButton (rightRect, "")){
			//Vai para estado pressionado
			rightButton		= (Texture)Util.LoadImageResource (STR.Resources.BACK_MENU_ITEM_PRESSED);
			right 	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(rightRect, rightButton);
			
		}else if(right && timePassed > threshold){
			//Faz operacao
			
			step ++;

			//Volta para estado despressionado
			rightButton		= (Texture)Util.LoadImageResource (STR.Resources.BACK_MENU_ITEM);
			right 	= false;

			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);
			
			GUI.DrawTexture(rightRect, rightButton);
			
		}else
			GUI.DrawTexture(rightRect, rightButton);
		
		
	}

	void downloadTarget(){
		Application.OpenURL("https://dl.dropboxusercontent.com/u/65033980/target.jpg");
	}

	void drawDownloadButton(){
		
		if(GUI.RepeatButton (downloadRect, "")){
			//Vai para estado pressionado
			downloadButton		= (Texture)Util.LoadImageResource (STR.Resources.DOWNLOAD_PRESSED);
			download 	= true;
			
			timePassed = 0;
			
			GUI.DrawTexture(downloadRect, downloadButton, ScaleMode.ScaleToFit);
			
		}else if(download && timePassed > threshold){
			//Faz operacao
			
			downloadTarget();
			
			//Volta para estado despressionado
			downloadButton		= (Texture)Util.LoadImageResource (STR.Resources.DOWNLOAD);
			download 	= false;
			
			SoundController.playSoundEffect (effects, Resources.Load ("Sounds/menuButtonEffect")as AudioClip);
			
			GUI.DrawTexture(downloadRect, downloadButton, ScaleMode.ScaleToFit);
			
		}else
			GUI.DrawTexture(downloadRect, downloadButton, ScaleMode.ScaleToFit);
		
		
	}

	void loadStep(){

		loadText ();
		loadImage ();

	}


	void loadImage(){

		Texture t;
		Rect tRect;

		switch (step) {

		case 0: 
			t = (Texture)Util.LoadImageResource (STR.Resources.TARGET);
			tRect = new Rect (0.5f*(Screen.width - 0.55f*Screen.height*t.width/t.height), 0.35f*Screen.height, 0.55f*Screen.height*t.width/t.height, 0.55f*(Screen.height));
			GUI.DrawTexture(tRect,t);
			break;
		case 1: //Foto do Target
			t = (Texture)Util.LoadImageResource (STR.Resources.TARGET_CINZA);
			tRect = new Rect (0.5f*(Screen.width - 0.55f*Screen.height*t.width/t.height), 0.35f*Screen.height, 0.55f*Screen.height*t.width/t.height, 0.55f*(Screen.height));
			GUI.DrawTexture(tRect,t);
			break;
		case 2: //Target na mesa
			t = (Texture)Util.LoadImageResource (STR.Resources.TABLE_TARGET);
			tRect = new Rect (leftRect.x + leftRect.width*1.2f, 0.45f*Screen.height, Screen.width - 1.2f*(rightRect.width + leftRect.width), 0.55f*(Screen.height));
			GUI.DrawTexture(tRect,t);
			break;
		case 3: //Ipad com target
			t = (Texture)Util.LoadImageResource (STR.Resources.IPAD_HOLD_TARGET);
			tRect = new Rect (0, 0.6f*Screen.height, Screen.width, 0.40f*(Screen.height));
			GUI.DrawTexture(tRect,t);
			break;
		case 4: //Ipad com target + anao
			t = (Texture)Util.LoadImageResource (STR.Resources.IPAD_HOLD_TARGET_AR);
			tRect = new Rect (0, 0.6f*Screen.height, Screen.width, 0.40f*(Screen.height));
			GUI.DrawTexture(tRect,t);
			break;
		case 5: //carregar botoes
			drawDownloadButton();
			break;
		default: break;
		
		}

	}

	int findNearestSpace(string text){
		
		int lenght = Mathf.FloorToInt( text.Length * 3.5f / 6);
		
		while (text[lenght] != ' ') {
			lenght --;
		}
		
		return lenght;
		
	}
	
	void loadText(){
		
		style.fontSize = (int)(0.07f*Screen.height);
		style.fontStyle = FontStyle.Normal;
		style.font =(Font)Resources.Load(InitialValues.DEFAULT_FONT_NAME);
		style.normal.textColor = Color.blue;
		Color color = new Color(4.0f/255,16.0f/255,125.0f/255);
		style.normal.textColor = color;
		style.alignment = TextAnchor.MiddleCenter;
		
		string text = STR.get("HtP"+step);
		
		if (text.Length >= 40) {
			
			Rect SubtitleRect1 = new Rect(Screen.width*0.05f,  Screen.height*0.15f, 0.90f* Screen.width , 0.10f*Screen.height);
			Rect SubtitleRect2 = new Rect(Screen.width*0.05f,  Screen.height*0.23f, 0.90f* Screen.width , 0.10f*Screen.height);
			
			int nearestSpace = findNearestSpace(text);
			
			GUI.Label(SubtitleRect1, text.Substring(0,nearestSpace), style);
			GUI.Label(SubtitleRect2, text.Substring(nearestSpace), style);
			
		}
		else{
			
			Rect SubtitleRect = new Rect(Screen.width*0.05f,  Screen.height*0.17f, 0.90f* Screen.width , 0.10f*Screen.height);
			
			GUI.Label(SubtitleRect, text, style);
			
		}
		
	}
}
