﻿using UnityEngine;
using System.Collections;

public class PointerArrow : MonoBehaviour {
	private Vector2 arrowSize;
	private Texture arrowCurretTexture;
	private char arrowDirection = '*';
	private Texture[] arrowsTexture;
	private Rect arrowRectangle;
	private float offset;
	private float sizeFractionOfScreen = 0.15f;
	private float offsetFractionOfScreen = 0.2f;
	private float currentOscillation = 0f;
	private Vector2[] oscillationPerDirection;
	private float maxOscillation;
	private float oscillationFrequence = 1.3f;
	private bool oscillationIsPositive = true;
	private Rect currentRectangle;

	void Awake(){
		arrowsTexture = new Texture[30];
		arrowsTexture ['l' - 'a'] = Util.LoadImageResource ("LeftTutorialArrow") as Texture;
		arrowsTexture ['r' - 'a'] = Util.LoadImageResource ("RightTutorialArrow") as Texture;
		arrowsTexture ['d' - 'a'] = Util.LoadImageResource ("DownTutorialArrow") as Texture;
		arrowsTexture ['u' - 'a'] = Util.LoadImageResource ("UpTutorialArrow") as Texture;

		oscillationPerDirection = new Vector2[30];
		oscillationPerDirection ['l' - 'a'] = new Vector2 (1f,0f);
		oscillationPerDirection ['r' - 'a'] = new Vector2 (-1f,0f);
		oscillationPerDirection ['d' - 'a'] = new Vector2 (0f,-1f);
		oscillationPerDirection ['u' - 'a'] = new Vector2 (0f,1f);

		arrowRectangle = new Rect ();
	}



	void Update(){
		if (arrowDirection != '*' ) {
			currentRectangle = arrowRectangle;
			currentRectangle.x += oscillationPerDirection[arrowDirection - 'a'].x * (currentOscillation ) ;
			currentRectangle.y += oscillationPerDirection[arrowDirection - 'a'].y * (currentOscillation );

			currentOscillation += (Time.deltaTime*(oscillationFrequence*2f)) * maxOscillation * ( oscillationIsPositive ? 1f : -1f );

			if( currentOscillation < 0f ){
				oscillationIsPositive = true;
			}
			else if( currentOscillation > maxOscillation ){
				oscillationIsPositive = false;
			}

		}
	}
	
	void OnGUI(){
		if (arrowDirection != '*' && GameController.State != GameController.GameStateEnum.Paused) {
			GUI.depth = 0;
			GUI.DrawTexture(currentRectangle, arrowCurretTexture, ScaleMode.ScaleToFit);
		}


	}

	private float fabs(float x){
		if (x < 0f)
			return -x;
		return x;
	}

	private char directionForRect( Rect r ){
		Vector2 p = r.center;

		if( p.x < Screen.width*0.333f ) return 'l';
		else if( p.x > Screen.width*0.666f ) return 'r';
		else if ( p.y > Screen.height*0.5f ) return 'd';
		else return 'u';
	} 

	public void pointToNothing(){
		arrowDirection = '*';
	}

	public void pointTo( Rect r ){
		char newDirection = directionForRect (r);

		if (newDirection != arrowDirection) {
			arrowDirection = newDirection;
			arrowCurretTexture = arrowsTexture [ arrowDirection - 'a'] ;
		}

		Vector2 p = r.center;

		if (arrowDirection == 'l') {
			p.x += r.width*0.5f;
			arrowRectangle.width = Screen.width*sizeFractionOfScreen;
			arrowRectangle.height = Screen.height*sizeFractionOfScreen;
			offset = arrowRectangle.width*offsetFractionOfScreen;
			arrowRectangle.x = p.x + offset;
			arrowRectangle.y = p.y - arrowRectangle.height*0.5f;
		}
		else if (arrowDirection == 'r') {
			p.x -= r.width*0.5f;
			arrowRectangle.width = Screen.width*sizeFractionOfScreen;
			arrowRectangle.height = Screen.height*sizeFractionOfScreen;
			offset = arrowRectangle.width*offsetFractionOfScreen;
			arrowRectangle.x = p.x - arrowRectangle.width - offset;
			arrowRectangle.y = p.y - arrowRectangle.height*0.5f;
		}
		else if (arrowDirection == 'd') {
			p.y -= r.height*0.5f;
			arrowRectangle.width = Screen.height*sizeFractionOfScreen;
			arrowRectangle.height = Screen.width*sizeFractionOfScreen;
			offset = arrowRectangle.height*offsetFractionOfScreen;
			arrowRectangle.x = p.x - arrowRectangle.width*0.5f;
			arrowRectangle.y = p.y - arrowRectangle.height - offset;
		}
		else if (arrowDirection == 'u') {
			p.y += r.height*0.5f;
			arrowRectangle.width = Screen.height*sizeFractionOfScreen;
			arrowRectangle.height = Screen.width*sizeFractionOfScreen;
			offset = arrowRectangle.height*offsetFractionOfScreen;
			arrowRectangle.x = p.x - arrowRectangle.width*0.5f;
			arrowRectangle.y = p.y + offset;
		}

		currentOscillation = 0f;
		oscillationIsPositive = true;
		maxOscillation = offset * 1f;
	}
}
