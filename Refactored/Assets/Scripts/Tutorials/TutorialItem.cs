﻿using UnityEngine;
using System.Collections;

public class TutorialItem : MonoBehaviour {
	private PointerArrow pointerArrow;
	private Text text;

	void Awake(){
		GameObject pointerArrowPrefab = Util.LoadPrefabResource ("PointerArrow") as GameObject;
		GameObject pointerArrowGameObject = Instantiate (pointerArrowPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		pointerArrow = pointerArrowGameObject.GetComponent (typeof(PointerArrow)) as PointerArrow;

		text = GetComponent<Text> ();
	}

	public void newTextAndPosition(string t, Rect r){
		text.setNewText (t);

		if (r.width == -1)
			pointerArrow.pointToNothing ();
		else
			pointerArrow.pointTo(r);
	}

	void OnDestroy(){
		Destroy ( pointerArrow );
	}
}
