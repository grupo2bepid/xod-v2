﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Text : MonoBehaviour {

	private string txt = "";
	private TextBackground txtBack;
	public Rect r;
	private GUIStyle gstyle;

	void Awake(){
		gstyle = new GUIStyle ();
		gstyle.normal.textColor = Color.white;
		gstyle.fontSize = (int)(Screen.width * 0.04f);
		gstyle.alignment = TextAnchor.MiddleCenter;
		gstyle.font = Resources.Load (InitialValues.DEFAULT_FONT_NAME) as Font;
		txtBack = GetComponent<TextBackground> ();
	}

	string breakString(string txt, int p){
		string ans = "";
		for (int i = 0; i < txt.Length; ++i) {
			if( i != p ) ans += txt[i];
			else ans+= "\n";
		}
		return ans;
	}

	List<string> getWords(string s){
		List<string> ans = new List<string> ();
		string aux = "";

		foreach (char c in s) {
			if( c == ' ' ){
				ans.Add(aux);
				aux = "";
			}
			else aux+= c;
		}

		if (aux.Length > 0)
			ans.Add (aux);

		return ans;
	}

	string getStringWithBrokenLines(string txt){
		List<string> l = getWords (txt);
		string newtxt = "";
		string currentLine = "";
		bool first_line = true;
		foreach (string s in l) {
			if( gstyle.CalcSize(new GUIContent( currentLine + " " + s )).x/r.width < 0.85f ){
				if( currentLine.Length > 0 ) currentLine += " ";
				currentLine += s;
			}
			else{
				if( !first_line ) newtxt += "\n";
				else first_line = false;

				newtxt += currentLine;

				currentLine = s;
			}
		}

		if(!first_line) newtxt += "\n";
		newtxt += currentLine;
		return newtxt;
	}
	
	void OnGUI(){
		if( GameController.State != GameController.GameStateEnum.Paused ){
			GUI.depth = 2;
			GUI.Label (r, txt, gstyle);
		}
	}

	public void setNewText(string s){
		r = txtBack.r;
		txt = getStringWithBrokenLines (s);
	}
}
