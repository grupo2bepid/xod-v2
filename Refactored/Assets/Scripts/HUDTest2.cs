﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
////public struct pair<T1, T2>{
////	public T1 first;
////	public T2 second;
////}
//
//public class HUDTest2 : MonoBehaviour {
//
//	private pair<string, int>[] itensCount;
//	private pair<string, int>[] heroesCount;
//
//	private int totalNumberOfItens = 8;
//	private int totalNumberOfHeroes = 4;
//	private int numItens;
//	private int numHeroes;
//	private GameObject instantiationSystemPrefab;
//
//	void Awake(){
//
//		itensCount = new pair<string, int>[ totalNumberOfItens + 5];
//		for (int i = 0; i < totalNumberOfItens; ++i) {
//			Barracks.ItemClass cls = (Barracks.ItemClass)(Barracks.ItemClass.UpgradeDefense + i);
//			itensCount[i].first = Barracks.getNameOfClassItem(cls);
//			itensCount[i].second = Barracks.getItemCount(cls);
//		}
//
//		heroesCount = new pair<string, int>[ totalNumberOfHeroes + 5];
//		for (int i = 0; i < totalNumberOfHeroes; ++i) {
//			Barracks.HeroClass cls = (Barracks.HeroClass)(Barracks.HeroClass.Fighter + i);
//			heroesCount[i].first = Barracks.getNameOfClass(cls);
//			heroesCount[i].second = Barracks.getHeroCount(cls);
//		}
//
//		instantiationSystemPrefab = Util.LoadPrefabResource ("InstantiationSystem") as GameObject;
//
//	}
//
//	int currentNumberOfItens(){
//		int ans = 0;
//		for (int i = 0; i < totalNumberOfItens; ++i)
//			if (itensCount [i].second > 0)
//					++ans;
//		return ans;
//	}
//
//	int currentNumberOfHeroes(){
//		int ans = 0;
//		for (int i = 0; i < totalNumberOfHeroes; ++i)
//			if (heroesCount [i].second > 0)
//				++ans;
//		return ans;
//	}
//
//	void drawHeroesButtons(){
//		if (numHeroes > 0) {
//			float buttonSize = Screen.height / (numHeroes * 2f);
//			int aux = -1;
//			for (int i = 0; i < totalNumberOfHeroes; ++i) {
//				if (heroesCount [i].second > 0) {
//					++aux;
//					Rect r = new Rect (Screen.width * 0.8f, buttonSize * aux + (Screen.height / (numHeroes * 3)) * (aux + 1f), buttonSize, buttonSize);
//					if (GUI.Button (r, heroesCount[i].first)) {
//						buy( true, i );
//					}
//				}
//			}
//		}
//	}
//
//	void drawItensButtons(){
//		if (numItens > 0) {
//			float buttonSize = Screen.height / (numItens * 2f);
//			int aux = -1;
//			for (int i = 0; i < totalNumberOfItens; ++i) {
//				if (itensCount [i].second > 0) {
//					++aux;
//					Rect r = new Rect (Screen.width / 10f, buttonSize * aux + (Screen.height / (numItens * 3)) * (aux + 1f), buttonSize, buttonSize);
//					if (GUI.Button (r, itensCount[i].first )) {
//						buy(  false, i );
//					}
//				}
//			}
//		}
//	}
//
//	void buy( bool isHero, int p ){
//
//		if (isHero) {
//			if ( --heroesCount[p].second == 0 ){
//				--numHeroes;
//			}
//
//
//
//		} else {
//			if ( --itensCount[p].second == 0 ){
//				--numItens;
//			}
//		}
//
////		GameObject instGameObj = Instantiate (instantiationSystemPrefab, Vector3.zero, Quaternion.identity) as GameObject;
////		InstantiationSystem instSystem = instGameObj.GetComponent (typeof(InstantiationSystem)) as InstantiationSystem;
////		instSystem.startInstantiationSystem (objectName);
//	}
//
//	void Start(){
//		numItens = currentNumberOfItens ();
//		numHeroes = currentNumberOfHeroes ();
//	}
//
//	void OnGUI(){
//		drawHeroesButtons ();
//		drawItensButtons ();
//	}
//
//}
