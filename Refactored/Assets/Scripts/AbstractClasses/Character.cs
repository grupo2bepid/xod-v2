﻿using UnityEngine;
using System.Collections;

public class Character : AugmentedRealityObject {

//	Nome da classe
	private Barracks.HeroClass _class;

	protected float[] attackAnimationDelays;
	protected HeroBase heroBase;
	protected Vector3 lastPosition;
	protected AIPath aipath;
	protected Animator animator;
	protected int maxAttack = 3;
	protected float delayBetweenAttacks = 1f;
	protected float currentSpeed = 0f;
	protected int bounty;
	protected bool superAgility=false;
	protected bool superForce=false;
	protected bool superDefense=false;

	public Transform lookToTarget = null;

	[HideInInspector] public string characterName;	//nao muito feliz em fazer isto
	[HideInInspector] public bool Dying = false;
	[HideInInspector] public int initialLife;

	private bool _fighting = true;
	private int _life = 10;
	private float turningSpeed = 2.2f;
	private CharacterController characterController;
	private float blockedTime = 0f;

	private IEnumerator turnOffCharacterColliderTemporarily(){
		if (characterController != null) {

			characterController.enabled = false;

			yield return new WaitForSeconds(1.5f);

			characterController.enabled = true;
		}
	}

	private void checkIfCharacterIsBlocked(){
		if (characterController != null && characterController.enabled ) {
			if( !Fighting && (aipath.target != null  && sqrDistance2D(transform.position, aipath.target.position) > 0.3f ) && currentSpeed < 0.2f ){
				blockedTime += Time.deltaTime;
			}
			else blockedTime = 0f;

			if( blockedTime > 4.2f ){
				StartCoroutine( "turnOffCharacterColliderTemporarily" );
				blockedTime = 0f;
			}
		}
	}

	protected void Awake(){
		aipath = GetComponent (typeof(AIPath)) as AIPath;
		animator = GetComponent (typeof(Animator)) as Animator;
		characterController = GetComponent<CharacterController> ();
	}

	protected void Start(){
		lastPosition = transform.position;
	}

	protected void Update(){
		updateSpeed ();
		animator.SetFloat ( HashIDs.speedFloat, currentSpeed );
		if ( currentSpeed < 0.2f && lookToTarget != null) {

			lookAt2dSmoothly( lookToTarget.position );
		}
		checkIfCharacterIsBlocked ();
	}
	
	protected void updateSpeed(){
		currentSpeed = distance2D(lastPosition, transform.position) / Time.deltaTime;
		lastPosition = transform.position;
	}

	protected void destroyMyself(){
		if (this is Hero) {
			Destroy ( heroBase.gameObject );
		}
		Destroy ( gameObject );
	}

	protected void lookAt2dSmoothly (Vector3 point) {
		Vector3 dir = point - transform.position;

		if (dir == Vector3.zero) return;

		Quaternion rot = transform.rotation;
		Quaternion toTarget = Quaternion.LookRotation (dir);
		
		rot = Quaternion.Slerp (rot,toTarget,turningSpeed*Time.deltaTime);
		Vector3 euler = rot.eulerAngles;
		euler.z = 0;
		euler.x = 0;
		rot = Quaternion.Euler (euler);
		
		transform.rotation = rot;
	}

	public bool Fighting{
		get{
			return _fighting; 
		}
		set{
			if( _fighting != value ){
				_fighting = value;
				if( _fighting ){
					aipath.stopWalk();
					StartCoroutine( "attackSelectionRoutine" );
				}
				else{
					StopCoroutine( "attackSelectionRoutine" );
					aipath.startWalk();
				}
			}
		}
	}

	protected float fabs(float f){
		if (f < 0)
			return -f;
		return f;
	}

	protected IEnumerator attackSelectionRoutine(){
		while (true) {
			int nextAttackNumber = Random.Range( 1, maxAttack + 1 );
			int nextAttackID = HashIDs.attacksIDs[ nextAttackNumber ];
			animator.SetTrigger(nextAttackID);

			if( superAgility ) yield return new WaitForSeconds( attackAnimationDelays[nextAttackNumber] );
			else yield return new WaitForSeconds( attackAnimationDelays[nextAttackNumber] + delayBetweenAttacks );
		}
	}

	public int Life{
		get{
			return _life;
		}
		set{
			if (_life != value) {
					_life = value;
					if (_life <= 0 && !Dying) {
							Dying = true;
							aipath.stopWalk ();
							StopAllCoroutines ();
							if (this is Boss) {
									GameController.bountyEarnedOnPhase += bounty;
									GameController.endPhase (true);
							} else if (this is Enemy) {
									GameController.registerEnemyDeath (bounty);
							} else {
									GameController.registerHeroDeath (ClassType);
							}
							gameObject.AddComponent ("BlinkEffect");
							animator.SetTrigger (HashIDs.dieTrigger);

					}
					if (this is Hero && heroBase != null && !Dying)
						heroBase.LifeFraction = ((float)Life) / initialLife;
			}
		}
	}
	
	public void sufferDamage(int damage){
		if( !Dying && !superDefense )
			Life -= damage;
	}

	public void dieForced(){
		Life = 0;
	}

	public Barracks.HeroClass ClassType{
		get{
			return _class;
		}
		set{
			_class = value;
		}
	}
}