﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hero : Character {


	protected float shotInterval = 2.0f;
	protected bool alreadyCalledShotMethod = false;
	protected Antihero antiheroFightPartner = null;
	protected float shootingHatchetAnimationDelay;
	protected bool playingShootingHatchetAnimation = false;

	private float acceptableDistanceFromBase = 0.5f;
	private GameObject heroBaseGameObject;
	private GameObject heroesGivingDamageEffectPrefab;
	private float appropriateDistanceForCurrentFight ;

	[HideInInspector] public float range = 3.0f;

	public bool isInsideFractionOfHeroRange( Vector3 position, float f ){
		return ( sqrDistance2D( position, heroBase.gameObject.transform.position ) <= range*range*f*f );
	}

	public void cancelOnFightRoutine(){
		StopCoroutine ("onFightRoutine");
		StopCoroutine ("lookForAntiheroes");
	}
	
	public void wakeUpFromTeletransport(float t){
		Invoke ("wakeUpAux", t);
	}

	private void wakeUpAux(){
		gameObject.SetActive (true);
		updadeHeroBasePosition ();
		if( heroBaseGameObject != null ) heroBaseGameObject.SetActive (true);
		Dying = false;
		StartCoroutine ( "lookForAntiheroes" );
	}

	public void disappearAndReapper(float t){
		disappear ();
		Invoke ("appear", t);
	}

	public void waitToDisappear(float t){
		Invoke ("disappear", t);
	}
	public void waitToReappear(float t){
		Invoke ("appear", t);
	}

	public void updadeHeroBasePosition(){
		heroBaseGameObject.GetComponent<HeroBase> ().setTargetAndRadius ( transform.position, range );
	}

	private void appear(){
		gameObject.SetActive (true);
		if( heroBaseGameObject != null ) heroBaseGameObject.SetActive (true);
	}

	private void disappear(){
		gameObject.SetActive (false);
		if( heroBaseGameObject != null ) heroBaseGameObject.SetActive (false);
	}

	new protected void Awake(){
		base.Awake ();
		heroesGivingDamageEffectPrefab = Util.LoadPrefabResource ("HeroesGivingDamageEffect") as GameObject;
		Fighting = false;
	}

	new protected void Start(){
		base.Start ();

		GameObject heroBasePrefab = Util.LoadPrefabResource ("HeroBase") as GameObject;
		heroBaseGameObject = Instantiate (heroBasePrefab, transform.position, Quaternion.identity) as GameObject;
		heroBase = heroBaseGameObject.GetComponent ( typeof(HeroBase) ) as HeroBase;
		heroBase.setTargetAndRadius (transform.position, range );
		StartCoroutine ( "lookForAntiheroes" );
	}

	private List<Antihero> getOnFloorAntiheroes(){
		Antihero[] antiheroes = FindObjectsOfType (typeof(Antihero)) as Antihero[];
		List<Antihero> ans = new List<Antihero> ();

		for (int i = 0; i < antiheroes.Length; ++i) {
			if( yDifferenceTo( antiheroes[i] ) < 0.5f ){
				ans.Add( antiheroes[i] );
			}
		}
		return ans;
	}

	protected Antihero findClosestAntihero(){
		Antihero closest = null;
		Vector3 pos = transform.position;
		List<Antihero> antiheroes = getOnFloorAntiheroes ();

		if (antiheroes.Count > 0) {
			closest = antiheroes[0];
			for(int i = 1; i < antiheroes.Count; ++i){
				if( !antiheroes[i].Dying && sqrDistance2D(pos, antiheroes[i].transform.position) < sqrDistance2D(pos, closest.transform.position)){
					closest = antiheroes[i];
				}
			}
		}
		return closest;
	}

	protected void applyDamage(int attackBaseStrength){
		if (antiheroFightPartner != null && !antiheroFightPartner.Dying) {
			Vector3 pos = antiheroFightPartner.transform.position;
			pos.y += 1f;
			Instantiate ( heroesGivingDamageEffectPrefab, pos, antiheroFightPartner.transform.rotation);

			if( superForce ){
				if( antiheroFightPartner is Boss )antiheroFightPartner.sufferDamage( 3*attackBaseStrength );
				else antiheroFightPartner.sufferDamage( 1000 );
			}
			else antiheroFightPartner.sufferDamage (attackBaseStrength);
		}

	}
	
	protected IEnumerator lookForAntiheroes(){
		while (true) {
			if( !Fighting ){

				Antihero antihero = findClosestAntihero();
				
				if( antihero != null && !antihero.Dying && distance2D( antihero.transform.position, heroBase.gameObject.transform.position ) <= range && !playingShootingHatchetAnimation){
					antiheroFightPartner = antihero;
					appropriateDistanceForCurrentFight = AppropriateDistancesForFight.getDistance( characterName, antiheroFightPartner.characterName );
					aipath.target = antiheroFightPartner.transform;
					StartCoroutine( "onFightRoutine" );
					yield break;
				}
				else{
					if( distance2D(transform.position, heroBase.gameObject.transform.position) > acceptableDistanceFromBase )
						aipath.target = heroBase.gameObject.transform;
					else if( aipath.target != gameObject.transform )
						aipath.target = gameObject.transform;

					if(  !alreadyCalledShotMethod && ( this is HatchetThrower ) ){

						if( superAgility ) Invoke("tryShotHatchet", shootingHatchetAnimationDelay);
						else Invoke("tryShotHatchet", shotInterval);

						alreadyCalledShotMethod = true;
					}
				}
			}

			yield return new WaitForSeconds(0.3f);
		}
	}

	void callLookForAntiheroes(){
		StartCoroutine( "lookForAntiheroes" );
	}

	private float yDifferenceTo(Character c){
		float ans = c.transform.position.y - transform.position.y;
		return fabs(ans);
	}

	private IEnumerator onFightRoutine(){
		while (true) {

//			print ("ydif = " + yDifferenceTo(antiheroFightPartner) );

			bool allowFight = true;
			if( antiheroFightPartner != null && antiheroFightPartner is Demon ){
				Demon auxDemon = antiheroFightPartner as Demon;
				allowFight = !auxDemon.isDemonFlying();
			}

			if (antiheroFightPartner != null && !Fighting && distance2D (transform.position, antiheroFightPartner.gameObject.transform.position) <= appropriateDistanceForCurrentFight && yDifferenceTo(antiheroFightPartner) < 0.45f && allowFight) {
				lookToTarget = antiheroFightPartner.gameObject.transform;
				aipath.target = transform;
				antiheroFightPartner.fightWithMe (this);
				Fighting = true;
			}
			else if( Fighting && ( antiheroFightPartner == null || antiheroFightPartner.Dying ) ){
				Fighting = false;
				if(!Dying) animator.SetTrigger( HashIDs.forceIdleTrigger );
				Invoke("callLookForAntiheroes", 0.2f);
				yield break;
			}

			yield return new WaitForSeconds( 0.3f );
		}
	}

	public void heal(){
		if( !Dying ) Life = initialLife;
	}

	public void giveSuperForce(){
		if (!Dying) {
			superForce = true;
			GameObject effectGameObject = Util.LoadPrefabResource ("UpgradeForceEffect") as GameObject;
			Vector3 offset = new Vector3 (0f, 1f, 0f);
			effectGameObject = Instantiate (effectGameObject, transform.position + offset, transform.rotation) as GameObject;
			ItemEffect effectScript = effectGameObject.GetComponent (typeof(ItemEffect)) as ItemEffect;
			effectScript.follow (transform, offset);
			Invoke ("removeSuperDefense", InitialValues.UPGRADE_ITENS_DURATION);
			Invoke ("removeSuperForce", InitialValues.UPGRADE_ITENS_DURATION);
		}
	}

	private void removeSuperForce(){
		superForce = false;
	}

	public void giveSuperDefense(){
		if (!Dying) {
			superDefense = true;
			GameObject effectGameObject = Util.LoadPrefabResource ("UpgradeDefenseEffect") as GameObject;
			Vector3 offset = new Vector3 (0f, 1f, 0f);
			effectGameObject = Instantiate (effectGameObject, transform.position + offset, transform.rotation) as GameObject;
			ItemEffect effectScript = effectGameObject.GetComponent (typeof(ItemEffect)) as ItemEffect;
			effectScript.follow (transform, offset);
			Invoke ("removeSuperDefense", InitialValues.UPGRADE_ITENS_DURATION);
		}
	}

	private void removeSuperDefense(){
		superDefense = false;
	}

	public void giveSuperAgility(){
		if (!Dying) {
			superAgility = true;
			GameObject effectGameObject = Util.LoadPrefabResource ("UpgradeAgilityEffect") as GameObject;
			Instantiate (effectGameObject, transform.position, transform.rotation);
			Invoke ("removeSuperAgility", InitialValues.UPGRADE_ITENS_DURATION);
		}
	}

	public void removeSuperAgility(){
		superAgility = false;
	}


}
