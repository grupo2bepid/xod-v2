﻿using UnityEngine;
using System.Collections;

public class Boss : Antihero {

	protected float takingOffAnimationLength;
	protected float landingAnimationLength;

	private Hero heroToGetCloser = null;
	private bool closeEnoughFromTargetHero;
	private bool alreadyCalledCheckingDistance = true;
	private float idleTime = 0f;

	private void stopProcastination(){

		Hero newHeroTarget = null;
		Hero[] heroes = (Hero[])FindObjectsOfType (typeof(Hero));

		if (heroes != null && heroes.Length > 0) {
			for(int i = 1; i < heroes.Length; ++i){
				if(  heroes[i] != null && heroes[i] != heroToGetCloser && !heroes[i].Dying ){
					newHeroTarget = heroes[i];
					break;
				}
			}
		}

		if (newHeroTarget != null) {
			heroToGetCloser  = newHeroTarget;
			closeEnoughFromTargetHero = false;
		}

		idleTime = 0f;

	}

	new protected void Awake(){
		base.Awake ();
	}

	new protected void Start(){
		base.Start ();
		setTargetAndGoAfterTheCristal ();
		StartCoroutine ("lookingForHeroesRoutine");
	}

	new protected void Update(){
		base.Update ();
		if (idleTime > 4f)
			stopProcastination ();
	}

	protected Hero findClosestHero(){
		Hero closest = null;
		Vector3 pos = transform.position;
		Hero[] heroes = (Hero[])FindObjectsOfType (typeof(Hero));

		if (heroes != null && heroes.Length > 0) {
			closest = heroes[0];
			for(int i = 1; i < heroes.Length; ++i){
				if(  heroes[i] != null && !heroes[i].Dying && Vector3.Distance(pos, heroes[i].transform.position) < Vector3.Distance(pos, closest.transform.position)){
					closest = heroes[i];
				}
			}
		}
		return closest;
	}

	private void setTargetAndGoAfterTheCristal(){
		aipath.target = cristal.transform;
		aipath.startWalk ();
		StartCoroutine ( "checkingDistanceFromCristalRoutine" );
	}

	private void stopLookingCristal(){
		if (lookToTarget == cristal.transform)
			lookToTarget = null;
	}

	private IEnumerator lookingForHeroesRoutine(){
		while (true) {

			if( (!Fighting || hittingCristal) && heroToGetCloser == null ){
				idleTime = 0f;
				Hero hero = findClosestHero();
				if( hero != null && !hero.Dying && heroToGetCloser == null && hero.gameObject.activeSelf){
					StopCoroutine("checkingDistanceFromCristalRoutine");
					stopLookingCristal();

					if(!Dying) animator.SetTrigger( HashIDs.forceIdleTrigger );
					yield return new WaitForSeconds(0.2f);

					hittingCristal = false;
					Fighting = false;
					heroToGetCloser = hero;
					closeEnoughFromTargetHero = false;


					alreadyCalledCheckingDistance = false;
					aipath.target = heroToGetCloser.gameObject.transform;
				}
				else if( hero == null && !alreadyCalledCheckingDistance) {

					setTargetAndGoAfterTheCristal();
					alreadyCalledCheckingDistance = true;

				}
			}
			else if( heroToGetCloser != null && (heroToGetCloser.Dying || !heroToGetCloser.gameObject.activeSelf)){	//POTENTIAL BUG
				idleTime = 0f;
				heroToGetCloser = null;
			}
			else if( heroToGetCloser != null && !closeEnoughFromTargetHero ){
				idleTime = 0f;
				if(  aboveGround() && heroToGetCloser.isInsideFractionOfHeroRange( transform.position, 0.5f ) ){
					aipath.target = transform;	//avoid step after kill a hero
					aipath.stopWalk();
					closeEnoughFromTargetHero = true;
				}
			}
			else if( heroToGetCloser != null && closeEnoughFromTargetHero  ){
				idleTime += 0.3f;
			}

			yield return new WaitForSeconds(0.3f);
		}
	}

	protected bool aboveGround(){
		RaycastHit hit;
		if (Physics.Raycast (transform.position, -Vector3.up, out hit) && hit.collider.gameObject.tag == "Ground" && fabs (hit.point.y - GameController.phaseHeight) < 0.2f)
			return true;
		return false;
	}

}
