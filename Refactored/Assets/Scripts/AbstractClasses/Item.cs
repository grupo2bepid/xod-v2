﻿using UnityEngine;
using System.Collections;

public abstract class Item : AugmentedRealityObject {

	protected Hero findClosestHero(){
		Hero closest = null;
		Vector3 pos = transform.position;
		Hero[] heroes = (Hero[])FindObjectsOfType (typeof(Hero));
		
		if (heroes != null && heroes.Length > 0) {
			closest = heroes[0];
			for(int i = 1; i < heroes.Length; ++i){
				if( !heroes[i].Dying && Vector3.Distance(pos, heroes[i].transform.position) < Vector3.Distance(pos, closest.transform.position)){
					closest = heroes[i];
				}
			}
		}
		return closest;
	}

}
