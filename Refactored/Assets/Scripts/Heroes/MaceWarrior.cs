﻿using UnityEngine;
using System.Collections;

public class MaceWarrior : HatchetThrower {
	
	private void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 0.833f;
		attackAnimationDelays [2] = 1.333f;
		attackAnimationDelays [3] = 1.833f;

		shootingHatchetAnimationDelay = 1.833f;
	}

	private void fillAttributes(){
		delayBetweenAttacks = Barracks.getValueOfAttribute (Barracks.Attributes.AttackDelay, Barracks.HeroClass.MaceWarrior);
		range = Barracks.getValueOfAttribute (Barracks.Attributes.Range, Barracks.HeroClass.MaceWarrior);
		maxAttack = (int)Barracks.getValueOfAttribute (Barracks.Attributes.MaxAttack, Barracks.HeroClass.MaceWarrior);
		Life = initialLife = (int)Barracks.getValueOfAttribute (Barracks.Attributes.Life, Barracks.HeroClass.MaceWarrior);
		shotInterval = shootingHatchetAnimationDelay + delayBetweenAttacks;
		ClassType = Barracks.HeroClass.MaceWarrior;
		characterName = InitialValues.MACEWARRIOR_NAME;
	}
	
	new void Awake () {
		base.Awake ();
		fillAttackAnimationDelays ();
		fillAttributes ();
	}

}
