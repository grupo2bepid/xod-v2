﻿using UnityEngine;
using System.Collections;

public class WarhammerWarrior : HatchetThrower {

	private void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 0.833f;
		attackAnimationDelays [2] = 1.500f;
		attackAnimationDelays [3] = 1.833f;

		shootingHatchetAnimationDelay = 1.833f;
	}

	private void fillAttributes(){
		delayBetweenAttacks = Barracks.getValueOfAttribute (Barracks.Attributes.AttackDelay, Barracks.HeroClass.WarhammerWarrior);
		range = Barracks.getValueOfAttribute (Barracks.Attributes.Range, Barracks.HeroClass.WarhammerWarrior);
		maxAttack = (int)Barracks.getValueOfAttribute (Barracks.Attributes.MaxAttack, Barracks.HeroClass.WarhammerWarrior);
		Life = initialLife = (int)Barracks.getValueOfAttribute (Barracks.Attributes.Life, Barracks.HeroClass.WarhammerWarrior);
		shotInterval = shootingHatchetAnimationDelay + delayBetweenAttacks;
		ClassType = Barracks.HeroClass.WarhammerWarrior;
		characterName = InitialValues.WARHAMMERWARRIOR_NAME;
	}

	new void Awake () {
		base.Awake ();
		fillAttackAnimationDelays ();
		fillAttributes ();
	}
}