﻿using UnityEngine;
using System.Collections;

public class Fighter : Hero {
	
	private void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 0.667f;
		attackAnimationDelays [2] = 1.333f;
		attackAnimationDelays [3] = 1.667f;
	}

	private void fillAttributes(){
		delayBetweenAttacks = Barracks.getValueOfAttribute (Barracks.Attributes.AttackDelay, Barracks.HeroClass.Fighter);
		range = Barracks.getValueOfAttribute (Barracks.Attributes.Range, Barracks.HeroClass.Fighter);
		maxAttack = (int)Barracks.getValueOfAttribute (Barracks.Attributes.MaxAttack, Barracks.HeroClass.Fighter);
		Life = initialLife = (int)Barracks.getValueOfAttribute (Barracks.Attributes.Life, Barracks.HeroClass.Fighter);
		ClassType = Barracks.HeroClass.Fighter;
		characterName = InitialValues.FIGHTER_NAME;
	}

	new void Awake(){
		base.Awake ();
		fillAttackAnimationDelays ();
		fillAttributes ();
	}

}
