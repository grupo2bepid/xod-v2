﻿using UnityEngine;
using System.Collections;

public class AxeWarrior : Hero {

	private void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 1.000f;
		attackAnimationDelays [2] = 1.333f;
		attackAnimationDelays [3] = 1.833f;
	}

	private void fillAttributes(){
		delayBetweenAttacks = Barracks.getValueOfAttribute (Barracks.Attributes.AttackDelay, Barracks.HeroClass.AxeWarrior);
		range = Barracks.getValueOfAttribute (Barracks.Attributes.Range, Barracks.HeroClass.AxeWarrior);
		maxAttack = (int)Barracks.getValueOfAttribute (Barracks.Attributes.MaxAttack, Barracks.HeroClass.AxeWarrior);
		Life = initialLife = (int)Barracks.getValueOfAttribute (Barracks.Attributes.Life, Barracks.HeroClass.AxeWarrior);
		ClassType = Barracks.HeroClass.AxeWarrior;
		characterName = InitialValues.AXEWARRIOR_NAME;
	}
	
	new void Awake(){
		base.Awake ();
		fillAttackAnimationDelays ();
		fillAttributes ();		
	}

}
