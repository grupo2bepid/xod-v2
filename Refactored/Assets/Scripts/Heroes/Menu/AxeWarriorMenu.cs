﻿using UnityEngine;
using System.Collections;

public class AxeWarriorMenu : HeroMenu {

	private void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 1.000f;
		attackAnimationDelays [2] = 1.333f;
		attackAnimationDelays [3] = 1.833f;
	}

	void Awake(){
		heroClass = Barracks.HeroClass.AxeWarrior ;
		fillAttackAnimationDelays ();
	}
}
