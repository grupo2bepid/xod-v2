﻿using UnityEngine;
using System.Collections;

public class FighterMenu : HeroMenu {
	
	private void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 0.667f;
		attackAnimationDelays [2] = 1.333f;
		attackAnimationDelays [3] = 1.667f;
	}

	void Awake(){
		heroClass = Barracks.HeroClass.Fighter ;
		fillAttackAnimationDelays ();
	}
}
