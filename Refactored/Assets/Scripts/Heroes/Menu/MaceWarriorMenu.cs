﻿using UnityEngine;
using System.Collections;

public class MaceWarriorMenu : HeroMenu {

	private void fillAttackAnimationDelays(){
		attackAnimationDelays = new float[5];
		attackAnimationDelays [1] = 0.833f;
		attackAnimationDelays [2] = 1.333f;
		attackAnimationDelays [3] = 1.833f;
	}

	void Awake(){
		heroClass = Barracks.HeroClass.MaceWarrior ;
		fillAttackAnimationDelays ();
	}
}
